<?php
if(isset($_GET['id'])){
require_once 'header_link.php';
require_once 'number_to_word.php';
$r = $db_handle->getPaymentDetails(base64_decode($_GET['iid']));
if(count($r)>0){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<head>
<title> Payment Receipt</title>
<script src="../vendors/jquery/dist/jquery.min.js"></script>
</head>
<body style="font-size:18px !important;">
<div style="width:970px;height:840px;margin:0 auto;padding:0;padding:10px;"> <br/>
  <div align="center" style="font-size:30px;font-weight:bold;color:black;"><?php echo invoiceCompanyTitle(); ?></div>
  <div align="center"><?php echo invoiceCompanyAddress(); ?></div>
  <div align="center"><?php echo invoiceCompanyPhone(); ?></div>
  <div align="center"><?php echo invoiceCompanyEmail() ?></div>
  <!-- <div align="center" style="font-size:21px;"><?php echo $slogan; ?></div> -->
  <div align="center">
  <br>
    <div style="font-weight:bold;border-radius:6px;background:#eef; width:250px;border:solid 2px #000;padding:5px;"> Payment Receipt <br >
    </div>
  </div>
  <br/>

  <br/>
  <br/>
  <div class="row">
  <center>
    <div  align="right" style="width:750px !important;border-radius:6px;font-size:16px;">
      <table class="cute_table" border="1px" style="border-collapse: collapse;padding:5px;">
       <?php 
         
       $id=(base64_decode($_GET['iid']));
      $r = $db_handle->getPaymentDetails($id);
      foreach($r as $PaymentDetails) {
        $supId = $PaymentDetails['supId'];
          $supName = $PaymentDetails['supName'];
          $supPhone = $PaymentDetails['supPhone'];
          $supAddress = $PaymentDetails['supAddress'];
          $paymentDate = $PaymentDetails['paymentDate'];
          $paymentAmount = $PaymentDetails['debit'];
          $paymentTime = $PaymentDetails['paymentTime'];
          $description = $PaymentDetails['pdescription'];
      }

      $results = $db_handle->getSupplierAllPayableAmountById($supId);
      $tcredit=0;
      $payable = 0;
      $tdebit=0;
      $trow=count($results);
      if($trow>0){
      foreach($results as $supPay) 
      {
        $tcredit += $supPay["credit"];
        $tdebit += $supPay["debit"];
      }
      $payable = ($tcredit - $tdebit);

           ?>
        <tr>
          <td  width="28%"><b>Supplier Name </b></td>
          <td><b><?php echo $supName; ?></b></td>
        </tr>
        <tr>
          <td><b>Suplier Phone: </b></td>
          <td><b><?php echo $supPhone; ?></b></td>
        </tr>
        <tr>
          <td><b>Supplier Address : </b></td>
          <td><b><?php echo  $supAddress; ?></td>
        </tr>
        <tr>
          <td><b>Payment Amount :  </b></td>
          <td style="font-size:15px;"><b><?php echo  $paymentAmount; ?> Taka</b></td>
        </tr>
        <tr>
          <td><b>Net Payable Amount : </b></td>
          <td  style="font-size:15px;"><b><?php echo $payable;  ?> Taka</b></td>
        </tr>
        <tr>
          <td><b>Payment Date: </b></td>
          <td><b><?php echo  date("d-m-Y", strtotime($paymentDate));  ?></b></td>
        </tr>
        <tr>
          <td><b>Payment Time: </b></td>
          <td><b><?php echo  $paymentTime;  ?></b></td>
        </tr>
        <tr>
          <td><b>Description : </b></td>
          <td><b><?php echo  $description;  ?></b></td>
        </tr>
    
      </table>
    </div>
    <div style="padding-top:150px;">
      <table>
        <tr>
          <td><center>-------------------------------</center></td>
          <td width="50%"><center></center></td>
          <td><center>-------------------------------</center></td>
        </tr>
        <tr>
          <td><center>Paid By</center></td>
          <td width="50%"><center></center></td>
          <td><center>Received By</center></td>
        </tr>
      </table>
    </div>
    </center>
  </div>
    <br/>
    <br/>
</div>

  <br/>
<style type="text/css">
#child tr td{
	text-align:center !important;
}
#bottom_calculation tr td{
	text-align:right;
}
.cute_table {
	margin:0px;padding:0px;
	border:1px solid #000000;
	
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
	
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
	
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
	
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}
.cute_table table{
  border-collapse: collapse;
  border-spacing: 0;
	width:100%;
	height:100%;
	margin:0px;padding:0px;
}.cute_table tr:last-child td:last-child {
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
}
.cute_table table tr:first-child td:first-child {
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}
.cute_table table tr:first-child td:last-child {
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
}.cute_table tr:last-child td:first-child{
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
}.cute_table tr:hover td{
		

}
.cute_table td{
	vertical-align:middle;

	border:1px solid #000000;
	border-width:0px 1px 1px 0px;
	text-align:left;
	padding:7px;
	color:#000000;
}.cute_table tr:last-child td{
	border-width:0px 1px 0px 0px;
}.cute_table tr td:last-child{
	border-width:0px 0px 1px 0px;
}.cute_table tr:last-child td:last-child{
	border-width:0px 0px 0px 0px;
}
.cute_table tr:first-child td{
	border:0px solid #000000;
	border-width:0px 0px 1px 1px;
	color:#000;
}
.cute_table tr:first-child:hover td{
}
.cute_table tr:first-child td:first-child{
	border-width:0px 0px 1px 0px;
}
.cute_table tr:first-child td:last-child{
	border-width:0px 0px 1px 1px;
}
.last_child{
	border-top:dashed 1px #666;
	border-bottom:dashed 1px #666;
}
</style>

 <script type="text/javascript">
 $(document).ready(function() {
  window.print();
 });
 </script>
</body>
</html>

<?php
}else{
  echo "<span style='color:red;font-size:15px;font-weight:bold;'><center>Invalid Purchase ID.</center></span>";
}
}else{
  echo "<h2 style='color:red;text-align:center;padding-top:250px;'>No Purchase Details Was Found.</h2>";
}
}
ob_end_flush();
?>