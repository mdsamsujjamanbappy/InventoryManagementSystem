<?php 
require_once 'header_link.php'; 
if (isset($_POST['savePayment'])) {
  if(isset($_POST['_MSBtoken'])){
	    date_default_timezone_set('Asia/Dhaka');

	    $paymentDate = date("Y-m-d");
	    $paymentTime = date("h:i:sa");
		$supplierId = ($_POST['supplierId']);
		$tenderedAmount = ($_POST['tenderedAmount']);
		$description = ($_POST['description']);
		
		$results = $db_handle->getNewSupplierPaymentNumber();

		foreach($results as $row0) {
		$SupplierPaymentsId = $row0['maximum'];
		}
		$SupplierPaymentsId+=1;

		$r = $db_handle->saveSupplierPayments($SupplierPaymentsId,$supplierId,$tenderedAmount,$paymentDate,$paymentTime,$description);

		if($r==1){
			echo "<script> document.location.href='payments_conf.php?st=paid&&p=".base64_encode($SupplierPaymentsId)."';</script>";

		}else{
			echo "Error";
		}
	}
}
