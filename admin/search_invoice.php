<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

    <style type="text/css">

        iframe {
        width: 100%;
        max-width: 100%;
        min-width: 100%;
        min-height: 500px;
        }

    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Search Invoice</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <center>
                    <div>
                     <form action="" method="POST">
                        <b>Invoice Number: </b>
                        <div class="form-group">
                          <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                            <input name="invoice_id" required autofocus style="text-align:center;margin-top:16px;" class="form-control" type="text"  placeholder="Insert Invoice Number" >
                            <br >
                            <button name="search" class="btn btn-primary" type="submit" ><i class="fa fa-search"></i> Search Invoice</button>
                            </div>

                          </div>
                     </form>
                    </div>
                    </center>
                  </div>
                  <?php
                  if(isset($_POST['search'])){ ?>
                  <div class="x_content">
                  <hr>
                     <?php
                      $invoice_number=($_POST['invoice_id']);
                      $r = $db_handle->getInvoiceDetails($invoice_number);
                      if(count($r)){
                     ?> 
                     <div>
                       
                      <div  style="float:left;">
                         <h2 style="color:green;align:center;"><strong>Invoice found.</strong></h2>
                      </div>   
                      <div style="float:right;">
                        <a class="btn btn-success btn-sm" target="_NEW" href="save_invoice_as_pdf.php?invoice_id=<?php echo base64_encode($invoice_number); ?>"><i class="fa fa-print"></i> Print Invoice</a>
                      </div>

                     </div>
                       <div class="clearfix"></div>
                       <hr>
                      <div style="padding-top:25px">
                        <iframe  src="save_invoice_as_pdf.php?invoice_id=<?php echo base64_encode($invoice_number); ?>"></iframe>
                      </div>
                    <?php }else{ ?>
                      <h2 style="color:red;text-align:center;"><strong>Sorry, Invoice not found.</strong></h2>

                     <?php } ?>
                  </div>
                  <?php } ?>



                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>