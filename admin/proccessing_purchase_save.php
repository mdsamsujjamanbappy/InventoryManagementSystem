<?php 
require_once 'header_link.php';
date_default_timezone_set('Asia/Dhaka');
?>
<script type="text/javascript">
    
    function getSupplierPayableAmount(val) {
        $.ajax({
        type: "POST",
        url: "get_supplier_payable.php",
        data:'supplier_id='+val,
        success: function(data){
            $("#supplier-payable").html(data);
        }
        });
        pay();
    }
</script>
<div class="row-fluid" style="color:black;">
    <div class="span12">
        <form action="save_purchase.php" method="POST">
        <table class="table table-striped table-bordered">
            <h3 align="center"><b>Supplier's Information</b></h3>
            <br />
              
                    <tr>
                    <td class="span2">Select Supplier</td>
                    <td  class="span4">
                    <select name="supplierId" onChange="getSupplierPayableAmount(this.value);"  required class="form-control">
                        <option value="">Select Supplier</option>
                    
                        <?php
                          $results = $db_handle->getSupplier();
                          $trow=count($results);
                          if($trow>0){
                           foreach($results as $supplier) { ?>
                            <option value="<?php echo $supplier['id'];?>"><?php echo $supplier['supName']."(".$supplier['supPhone'].")";?></option>
                           <?php }
                       }
                        
                        ?>
                        </select>
                    </td>
                    </tr>

                    <tr>
                    <td class="span2">Current Voucher Amount</td>
                    <td  class="span4"><input value="<?php echo $_REQUEST['tot'];?>" id="voucherAmount"  onkeyup="cal();"  readonly type="text"  class="form-control" name="total_amount"></td>
                    </tr>

                    <tr>
                    <td class="span2">Previous Payable Amount</td>
                    <td  class="span4"><div id="supplier-payable">
                        <input type="text" id="prePayableAmount"  required disabled name="payableAmount" value='0' class="form-control">
                    </div></td>
                    </tr>

                   <!--  
                    <tr>
                    <td class="span2">Total Payable Amount</td>
                    <td  class="span4"><input id="totalPayable"  onkeyup="cal();"  disabled type="text"  class="form-control" name="payableAmount"></td>
                    </tr> -->

                    <tr>
                        <td class="span2">Tendered Amount</td>
                        <td class="span4"><input required placeholder="Tendered Amount" type="number"   id="txt1"  onkeyup="pay();"  class="form-control" name="tendered_amount"></td>
                    </tr>

                    <tr>
                        <td class="span2">Total Due Payable Amount</td>
                        <td class="span4"><input required  type="number"   id="txt2"  onkeyup="cal();" disabled value="0" class="form-control" name=""></td>
                    </tr>

                    <tr>
                        <td class="span2">Purchase Date</td>
                        <td class="span4"><input value="<?php echo date('Y-m-d')?>" class="form-control" name="purchaseDate"></td>
                    </tr>

                    <tr>
                        <td class="span2">Purchase Time</td>
                        <td class="span4"><input value="<?php echo date('h:i:sa')?>" class="form-control" name="purchaseTime"></td>
                    </tr>

    				<tr>
                    <td class="span2"></td>
                    <td  class="span4">
                        <input type="submit" name="purchase_save_print" value="Save" class="btn btn-primary ">
                    </td>
                    </tr>
        </table>
        </form>
    </div>
</div>

<script>

    function pay() {
           var txtFirstNumberValue = document.getElementById('prePayableAmount').value;
         var txtSecondNumberValue = document.getElementById('voucherAmount').value;
        var tendered_amount = document.getElementById('txt1').value;

        var result = (parseInt(txtFirstNumberValue) + parseInt(txtSecondNumberValue))-parseInt(tendered_amount);
        if (!isNaN(result)) {
            document.getElementById('txt2').value = result;
        }
    }

</script>
