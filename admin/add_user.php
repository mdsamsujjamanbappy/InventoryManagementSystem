<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>New User Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                     <center>
                      <?php 
                      if (isset($_POST['save'])) {
                          if(isset($_POST['_MSBtoken'])){

                        $userfname = ($_POST['userfname']);
                        $usertype = ($_POST['usertype']);
                        $uemail = ($_POST['uemail']);
                        $uphone = ($_POST['uphone']);
                        $ujoiningDate = ($_POST['ujoiningDate']);
                        $uaddress = ($_POST['uaddress']);
                        $username = ($_POST['username']);
                        $upassword = ($_POST['upassword']);
                        $image = "default.jpg";
                        
                        if(isset($_FILES['photo']['name'])){
                          $a = explode(".", $_FILES['photo']['name']);
                          if(count($a) > 1)
                          {
                              $ext = strtolower($a[count($a) - 1]);
                              
                              if($ext=="jpg" || $ext=="png" || $ext=="jpeg")
                              {
                                  if(($_FILES['photo']['size']) <= (3000*1024))
                                  {
                                         $picture_tmp = $_FILES['photo']['tmp_name'];
                                          $picture_name = $_FILES['photo']['name'];
                                          $picture_type = $_FILES['photo']['type'];
                                          $arr1 = explode(".", $picture_tmp);
                                            $extension1 = strtolower(array_pop($arr1));  
                                            
                                            $arr = explode(".", $picture_name);
                                            $extension = strtolower(array_pop($arr));

                                          $image="user"."_".$usertype."_".$uphone .".".$extension;
                                          $newfilename1="user"."_".$usertype."_".$uphone .".".$extension1;
                                          $path = '../user_image/'.$image; 
                                          move_uploaded_file($picture_tmp, $path);
                                  }
                              }
                            }
                          }

                        $r = $db_handle->insertNewUserInfo($userfname,$usertype,$uemail,$uphone,$ujoiningDate,$uaddress,$username,$upassword,$image);

                        if($r==1){
                            echo "<h2 style='color:green;'>User Information has been Successfully Inserted</h2>";

                            echo "<br />";

                            echo "<a href='add_user.php' style='margin-bottom:8px; margin-top:-5px;' class='btn btn-sm btn-primary'>Add Another User</a> &nbsp;&nbsp;";
                            echo "<a href='users_list.php' style='margin-bottom:8px; margin-top:-5px;' class='btn btn-sm btn-primary'>Manage Users</a>";
                        }else{
                            echo "<h2 style='color:red;'>User Insertion Failed</h2>";

                            echo "<br />";
                            echo "<br />";
                            echo "<a href='users_list.php' style='margin-bottom:8px; margin-top:-5px;' class='btn btn-sm btn-primary'>Manage User</a>";
                          }

                        }else{
                            echo "Invalid Token";
                        }

                       }else{ ?>

                         
                        <table class="table table-bordered" width="800px;">

                            <form action="" method="POST" enctype="multipart/form-data"  >
                             
                              <tr><td width="25%">Fullname:</td><td>
                              <input style="margin-bottom:-0px;" class="form-control" required name="userfname" type="text" value="">
                              <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>">
                              </td></tr>
                              
                              <tr><td>Type:</td><td>
                              <select name="usertype" required class="form-control"  onChange="getSubCat(this.value);" >
                              <option value=""> Select User Type</option>
                                <?php
                                  $results = $db_handle->getUserType("","100");
                               foreach($results as $usertype) {
                                ?>
                                   <option value="<?php echo $usertype['userTypeId']; ?>"><?php echo $usertype['userType']; ?></option>
                                <?php } ?>
                                </select></td></tr>

                              <tr><td>Email :</td><td><input name="uemail" style="margin-bottom:-2px;"  class="form-control" type="email" value=""></td></tr>
                              
                              <tr><td>Phone:</td><td><input  class="form-control" name="uphone" style="margin-bottom:-2px;" id='txt1' required  type="text" value=""></td></tr>
                              
                              <tr><td>Joining Date:</td><td><input  id="single_cal4" class="form-control" name="ujoiningDate" style="margin-bottom:-2px;" required  type="text" value=""></td></tr>
                              
                              <tr><td>Address:</td><td><textarea  class="form-control" required name="uaddress" style="margin-bottom:-2px;"></textarea></td></tr>
                              
                              <tr><td>Username:</td><td><input  class="form-control" name="username" style="margin-bottom:-2px;" required type="text" value=""></td></tr>
                              
                              <tr><td>Password:</td><td><input name="upassword"  pattern=".{8,8}" title="Password should be exactly 8 characters" maxlength="8" class="form-control" type="text" style="margin-bottom:-2px;" required value=""></td></tr>
                              
                                                                         
                              <tr><td>Image:</td><td><input type="file" style="margin-bottom:-2px;"  class="form-control"  name="photo"></td></tr>
                              
                        
                              
                              <tr><td></td><td><input name="save" type="submit" class="btn btn-success" style="margin-bottom:-2px;"  value="Add User Information"></td></tr>

                           </form>
                       </table>

                            <?php } ?>
                        </center>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
     <!-- bootstrap-datetimepicker -->    
    <script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>


  </body>
</html>