<?php
require_once 'header_link.php';
$results = $db_handle->getNewVoucherNumber();

foreach($results as $row0) {
$voucherNumber = $row0['maximum'];
}

$voucherNumber+=1;

date_default_timezone_set('Asia/Dhaka');

if(isset($_POST['purchase_save_print'])){

$tmpId = $_SESSION['tmpPuchaseId'];
$supplierId = $_POST['supplierId'];
$purchaseDate = $_POST['purchaseDate'];
$purchaseTime = $_POST['purchaseTime'];
$tenderedAmount = $_POST['tendered_amount'];
$total_amount = $_POST['total_amount'];

$results = $db_handle->getTmpPurchaseProducts($tmpId);
foreach($results as $product) {
$purTmpId = $product["purTmpId"];
$productId = $product["productId"];
$purchaseRate = $product["purchaseRate"];
$purchaseQtys = $product["purchaseQtys"];

$r = $db_handle->tmpToPurchase($voucherNumber,$productId,$purchaseRate,$purchaseQtys);

$db_handle->updateIncreaseProductQuantity($productId,$purchaseQtys);

$db_handle->deleteProductFromPurchaseList($purTmpId);
}

if($r){
$db_handle->insertPurchaseInfo($voucherNumber,$supplierId,$purchaseDate,$purchaseTime);
$db_handle->insertPaymentInfo($supplierId,$tenderedAmount,$total_amount,$purchaseDate,$purchaseTime,$voucherNumber);
    unset($_SESSION['tmpPuchaseId']);
}

echo "<script> document.location.href='purchased.php?iid=".base64_encode($voucherNumber)."&&st=pur';</script>";

}

?>