<?php 
require_once 'header_link.php'; 
$id = base64_decode($_REQUEST['id']);
$results = $db_handle->getProductDetails($id);
foreach($results as $product) {
  $pname = $product['pname'];
  $quantity = $product['quantity'];
  $pcode = $product['code'];
  }
?>
<div>
  <div class="row animated  " style="color:black;">
    <div class="col-md-10 col-md-offset-1">
      <form action="update_stock_info.php" method="POST">
        <h3><center><b>Update Stock</b></center></h3><br>
        <table class="table table-striped table-bordered responsive-utilities">
          <tr>
              <td  class="">Product Code</td>
              <td class="">
              <div ><b><?php echo $pcode;?></b></div>
              </td>
          </tr>  
          <tr>
              <td  class="">Product Name</td>
              <td class="">
              <div ><b><?php echo $pname;?></b></div>
              </td>
          </tr> 

          <tr>
              <td class="">Present Stock</td>
              <td class="">
              <div>
                <input hidden value="<?php echo md5("MSBCSE");?>" name="_MSBtoken" >
                <input hidden value="<?php echo $id;?>" name="productId" >
                <input type="number" step="any" required class="form-control" min="0" autofocus value="<?php echo $quantity;?>"  name="quantity" >
              </div>
              </td>
          </tr>

          <tr>
              <td class=" "></td>
              <td class=" ">
                  <input type="submit" name="update_product" value="Update Stock" class="btn btn-success ">
                  <input type="reset" name="" value="Reset" class="btn btn-danger ">
              </td>
          </tr>
        </table>
      </form>
     </div>
  </div>
</div>
