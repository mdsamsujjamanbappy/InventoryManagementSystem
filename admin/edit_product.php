<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Edit Product</title>

    <?php include("css.php");?>

    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/select2/dist/css/select2.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <span style="color:black !important;font-size: 18px;"><b>Edit Product Information</b></span>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                   <center>
                     <?php 
                     if (isset($_POST['update'])) {
                        if(isset($_POST['_MSBtoken'])){

                      $pid = ($_POST['pid']);
                      $pname = ($_POST['pname']);
                      $pcode = ($_POST['pcode']);
                      $subcat_id = ($_POST['subcat_id']);
                      $quantity = ($_POST['quantity']);
                      $description = ($_POST['description']);
                      $sdescription = ($_POST['sdescription']);
                      $originalPrice = ($_POST['originalPrice']);
                      $sellingPrice = ($_POST['sellingPrice']);
                      $discount = ($_POST['discount']);
                      $vat = ($_POST['vat']);

                      $r = $db_handle->updateProductInfo($pid,$pname,$pcode,$subcat_id,$quantity,$description,$sdescription,$originalPrice,$sellingPrice,$vat,$discount);

                      if($r==1){
                          echo "<h2 style='color:green;'>Product Information has been Successfully Updated</h2>";

                          echo "<br />";

                          echo "<a href='all_products_list.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-xs btn-primary'>All Products List</a>";
                      }else{
                          echo "<h2 style='color:red;'>Information Update Failed</h2>";

                          echo "<br />";
                          echo "<br />";
                          echo "<a href='all_products_list.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-xs btn-primary'>All Products List</a>";
                        }
                        }else{
                          echo "Invalid Token";
                         }
                     }else{ 
                          
                          $id = base64_decode($_REQUEST['id']);
                          $results = $db_handle->getProductDetails($id);
                          
                        foreach($results as $product) {
                        ?>
                            <table class="table table-bordered" width="800px;">
                            <script>
                            function getSubCat(val) {
                            $.ajax({
                            type: "POST",
                            url: "get_sub_category.php",
                            data:'category_id='+val,
                            success: function(data){
                            $("#subcat_list").html(data);
                            }
                            });
                            } 
                          </script>


                          <form action="" method="POST" >
                            <input hidden name="pid" value="<?php echo $id;?>">
                            <tr><td width="25%">Product Code:</td><td><input class="form-control"  style="margin-bottom:-0px;" name="pcode" type="text" value="<?php echo htmlentities($product["code"]); ?>"><input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>"></td></tr>
                            
                            <tr><td>Product Name:</td><td><input name="pname" class="form-control"  type="text" value="<?php echo htmlentities($product["pname"]); ?>">  </td></tr>
                            
                            <tr><td>Category:</td><td>
                            <select name="cat_id" required class="form-control select2me"  onChange="getSubCat(this.value);" >
                            <option value=""> Select Category</option>
                              <?php
                                $results = $db_handle->getCategory();
                             foreach($results as $category) {
                              ?>
                                 <option value="<?php echo $category['cid']; ?>"><?php echo $category['cname']; ?></option>
                              <?php } ?>
                              </select> <br />Previous Category: <b><?php echo htmlentities($product["cname"]); ?></b></td></tr>

                              <tr><td>Sub-Category:</td><td>
                               <select id="subcat_list"  class="form-control select2me"  name="subcat_id" required > 
                                  <option value="">Select </option>
                                </select> <br >Previous Sub-Category: <b><?php echo htmlentities($product["sub_cat_name"]); ?></b>
                            </td></tr>
                            
                            <tr><td>Long Description:</td><td><textarea rows="5" name="description" class="form-control"  style="margin-bottom:-2px;"><?php echo htmlentities($product["description"]); ?></textarea></td></tr>
                            <tr><td>Short Description:</td><td><textarea rows="4" name="sdescription" class="form-control"  style="margin-bottom:-2px;"><?php echo htmlentities($product["sdescription"]); ?></textarea></td></tr>
                            <tr><td>Quantity:</td><td><input name="quantity" class="form-control"  style="margin-bottom:-2px;" type="text" value="<?php echo htmlentities($product["quantity"]); ?>"></td></tr>
                            <tr><td>Original Price:</td><td><input name="originalPrice" class="form-control"  style="margin-bottom:-2px;"  type="text" value="<?php echo htmlentities($product["originalPrice"]); ?>"></td></tr>
                            <tr><td>Selling Price:</td><td><input name="sellingPrice" class="form-control"  style="margin-bottom:-2px;"  type="text" value="<?php echo htmlentities($product["sellingPrice"]); ?>"></td></tr>
                            <tr><td>Vat (%): </td><td><input name="vat"   class="form-control"  style="margin-bottom:-2px;" required  type="number" step="any"  style="margin-bottom:-2px;"  value="<?php echo htmlentities($product["vat"]); ?>"></td></tr>
                            <tr><td>Discount:</td><td><input name="discount" class="form-control"  type="text" style="margin-bottom:-2px;"  value="<?php echo htmlentities($product["discount"]); ?>"></td></tr>
                            <tr><td></td><td><input name="update" type="submit" class="btn btn-success" style="margin-bottom:-2px;"  value="Update Information"></td></tr>
                         </form>
                          </table>

                          <?php } } ?>
                      </center>

                  </div>
                </div>
              </div>
              </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/select2/dist/js/select2.js"></script>
    <script>
      $(document).ready(function() {
          $('.select2me').select2();
      });
    </script>
  </body>
</html>