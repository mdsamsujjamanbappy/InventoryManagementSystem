<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2>Puchase Product</h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <a href="purchase_product.php" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i> Refresh</a>
                        <a href="purchase_product.php?new=new"  class="btn btn-default btn-sm"><i class="fa fa-plus-square-o"></i> New Purchase Information</a>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                  <div class="x_content">
                        <center>
                         <a rel="facebox" href="add_product_by_category.php" class="btn btn-primary btn-sm"> <i class="fa fa-plus-square"></i> Add Product By Category</a>
                         <hr>
                        <br>
                        <?php
                            if(isset($_GET['new'])){
                              $tmpId = md5($_SESSION['tmpPuchaseId']);

                                $r = $db_handle->getTotalRowNumber("tbpuchaseproducts_tmp","tmpPuchaseId",$tmpId);
                                if($r<=0){
                                      unset($_SESSION['tmpPuchaseId']);
                                }else{

                                $results = $db_handle->getTmpPurchaseProducts($tmpId);
                                if(count($results)>0){
                                 foreach($results as $product) {
                                    $tmpid = $product["tmpid"];
                                    $db_handle->deleteProductFromPurchaseList($tmpid);
                                 }
                               }else{}
                                }
                                 unset($_SESSION['tmpPuchaseId']);
                              }


                            if(!isset($_SESSION['tmpPuchaseId']))
                            {
                             $tmp_id1 = rand(1000000,9999999);
                             $_SESSION['tmpPuchaseId']=md5($tmp_id1);
                            }

                            $tmp_id = $_SESSION['tmpPuchaseId'];
                        ?>
                    
                   <?php
                         $tmp_id = $_SESSION['tmpPuchaseId'];
                        $results = $db_handle->getTmpPurchaseProducts($tmp_id);
                   ?>
                   <table id="datatable" class="table table-bordered table-stripted table-hover">
                        <thead style="font-size:12px;color:black;">
                              <th width="3%">SN</th>
                              <th width="35%">Product Name <br></th>
                              <th width="4%">Quantity <br></th>
                              <th width="12%">Purchase Rate</th>
                              <th  width="12%">Sub-Total <br></th>
                              <th width="4%">Action <br></th>
                         </thead>
                         <tbody>
                          <?php
                          $i=0;
                          $gt =0;
                          $trow=count($results);
                          if($trow>0){

                           foreach($results as $product) {
                          ?>
                         <tr>
                              <td><?php echo ++$i; ?></td>
                              <td><b><?php echo ($product["pname"]); ?></b><br>
                              <span style="font-size:11px;"><?php echo ($product["sdescription"]); ?></span>
                              </td>
                              <td><?php echo ($product["purchaseQtys"])." ".($product["unitName"]); ?> </td>
                              <td><?php echo (($product["purchaseRate"])); ?> Taka</td>
                              <td><?php echo (($product["purchaseRate"])*($product["purchaseQtys"])); ?> Taka</td>
                              <td>
                                  <center><a onClick="return confirm('Do you want to remove this Product from List?');"  href="remove_product_from_purchase_list.php?id=<?php echo base64_encode($product['purTmpId']); ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a></center>
                              </td>
                         </tr>

                         <?php
                          $gt = $gt + (($product["purchaseRate"])*($product["purchaseQtys"]));

                          }
                         }
                          ?>
                          </tbody>
                          <tfoot>
                              <th colspan="4" >Total </th>
                              <th  colspan="" style="font-size:15px;"><?php echo $gt;?> Taka</th>
                              <th  colspan=""></th>
                         </tfoot>
                       </table>
                                         <a  style="margin-left:10px;" rel="facebox" href="proccessing_purchase_save.php?tot=<?php echo $gt;?>" class="btn btn-success"><i class="fa fa-save"></i>  Save Invoice</a>

          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>