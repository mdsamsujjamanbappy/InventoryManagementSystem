<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add New Sub-Category</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <?php 
                     if (isset($_POST['save'])) {
                        if(isset($_POST['_MSBtoken'])){

                      $name = ($_POST['sname']);
                      $cat_id = ($_POST['cat_id']);
                      $r = $db_handle->saveSubCategory($name,$cat_id);

                      if($r==1){
                          echo "<h2 style='color:green;'>Sub Categotry has been Successfully Inserted</h2>";

                          echo "<br />";

                          echo "<a href='add_subcategory.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-sm btn-success'>Add Another Sub-Category</a> &nbsp;&nbsp;";

                          echo "<a href='subcategory_list.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-sm btn-primary'>Manage Sub Category</a>";
                      }else{
                          echo "<h2 style='color:red;'>Insertion Failed</h2>";

                          echo "<br />";
                          echo "<br />";

                          echo "<a href='add_subcategory.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-sm btn-primary'>Try Again</a> &nbsp;&nbsp;";
                        }
                      }else{
                          echo "Invalid Token";
                      }
                     }else{ ?>
                         <form action="" method="POST"  class="form-horizontal form-label-left">
                                     
                          <div class="form-group">
                            <label style='margin-top:0px;' class="control-label col-md-3 col-sm-3 col-xs-12" for="testPrice">Category Name:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                               <select required name="cat_id" class="form-control" >
                                <option value="">Select Category </option>}
                                <?php
                                  $results = $db_handle->getCategory("","50000");
                               foreach($results as $category) {
                                ?>
                                   <option value="<?php echo $category['cid']; ?>"><?php echo $category['cname']; ?></option>
                                   <?php } ?>
                                </select>
                                 <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>">
                            </div>
                          </div>  
                          <div class="clearfix"></div>
                          <div class="form-group">
                            <label style='margin-top:10px;' class="control-label col-md-3 col-sm-3 col-xs-12" for="testPrice">Sub Category Name:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <input name="sname" class="form-control"  style=" margin-top:10px;"  required  type="text" placeholder="Insert Sub Category name">
                            </div>
                          </div>
                          <div class="clearfix"></div>

                          <div class="form-group">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                              <input style="margin-top:25px;" class="btn btn-success"  name="save" type="submit" value="Save Sub-category" >
                              <a href="subcategory_list.php" style="margin-top:25px;" class="btn btn-primary"><i class="fa fa-refresh"></i> Go Back</a>
                            </div>
                          </div>

                          </form>
                          <?php } ?>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>