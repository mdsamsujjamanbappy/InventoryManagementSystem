<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-user"></i> Edit User Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <center>
                     <?php 
                     if (isset($_POST['update'])) {
                      if(isset($_POST['_MSBtoken'])){

                        $userfname = ($_POST['userfname']);
                        $usertype = ($_POST['usertype']);
                        $uemail = ($_POST['uemail']);
                        $uphone = ($_POST['uphone']);
                        $ujoiningDate = ($_POST['ujoiningDate']);
                        $uaddress = ($_POST['uaddress']);
                        $username = ($_POST['username']);
                        $image = ($_POST['photo1']);
                        
                        if(isset($_FILES['photo']['name'])){
                          $a = explode(".", $_FILES['photo']['name']);
                          if(count($a) > 1)
                          {
                              $ext = strtolower($a[count($a) - 1]);
                              
                              if($ext=="jpg" || $ext=="png" || $ext=="jpeg")
                              {
                                  if(($_FILES['photo']['size']) <= (3000*1024))
                                  {
                                          unlink("user_image/".$image);
                                          $picture_tmp = $_FILES['photo']['tmp_name'];
                                          $picture_name = $_FILES['photo']['name'];
                                          $picture_type = $_FILES['photo']['type'];
                                          $extension1=end(explode(".", $picture_tmp));
                                          $extension=end(explode(".", $picture_name));
                                          $image="user"."_".$usertype."_".$uphone .".".$extension;
                                          $newfilename1="user"."_".$usertype."_".$uphone .".".$extension1;
                                          $path = 'user_image/'.$image; 
                                          move_uploaded_file($picture_tmp, $path);
                                  }
                              }
                            }
                          }


                            $id = ($_POST['id']);
                            $r = $db_handle->updateUserInfo($id,$userfname,$usertype,$uemail,$uphone,$ujoiningDate,$uaddress,$username,$image);

                        if($r==1){
                            echo "<h2 style='color:green;'>User Information has been Successfully Updated</h2>";

                            echo "<br />";

                            echo "<a href='users_list.php' class='btn btn-sm btn-primary'><i class='fa fa-users'></i> Manage Users</a>";
                        }else{
                            echo "<h2 style='color:red;'>User Updating Failed</h2>";

                            echo "<br />";
                            echo "<br />";
                            echo "<a href='users_list.php' class='btn btn-sm btn-primary'> <i class='fa fa-users'></i> Manage User</a>";
                          }

                        }else{
                          echo "Invalid Token";
                         }
                       }else{
                         
                            $id = base64_decode($_REQUEST['id']);
                            $results = $db_handle->getUserDetails($id);
                            
                          foreach($results as $user) {
                          ?>
                        <table class="table table-bordered" width="800px;">

                            <form action="" method="POST" enctype="multipart/form-data"  >
                             
                              <tr><td width="25%">Fullname:</td><td>
                              <input style="margin-bottom:-0px;" value="<?php echo htmlentities($user["userFullName"]); ?>" class="form-control" required name="userfname" type="text" value="">
                              <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>">
                              <input name="id" hidden value="<?php echo $id;?>">

                              </td></tr>
                              
                              <tr><td>Type:</td><td>
                              <select name="usertype" required class="form-control"  onChange="getSubCat(this.value);" >
                              <option value=""> Select Type</option>
                                <?php
                                  $results = $db_handle->getUserType();
                               foreach($results as $usertype) {
                                ?>
                                   <option <?php if($usertype['userTypeId']==$user["userTypeId"]){ echo "selected";}?> value="<?php echo $usertype['userTypeId']; ?>"><?php echo $usertype['userType']; ?></option>
                                <?php  } ?>
                                </select> <br />&nbsp;Previous Type: <b><?php echo htmlentities($user["userType"]); ?></b></td></tr>

                              <tr><td>Email :</td><td><input value="<?php echo htmlentities($user["userEmail"]); ?>" name="uemail" style="margin-bottom:-2px;"  class="form-control" type="email" </td></tr>
                              
                              <tr><td>Phone:</td><td><input  class="form-control"  value="<?php echo htmlentities($user["userPhone"]); ?>" name="uphone" style="margin-bottom:-2px;" required  type="text"></td></tr>
                              
                              <tr><td>Joining Date:</td><td><input  class="form-control"  value="<?php echo htmlentities($user["userJoiningDate"]); ?>" name="ujoiningDate" style="margin-bottom:-2px;"required  type="text" ></td></tr>
                              
                              <tr><td>Address:</td><td><textarea  class="form-control" required name="uaddress" style="margin-bottom:-2px;"> <?php echo htmlentities($user["userAddress"]); ?></textarea></td></tr>
                              
                              <tr><td>Username:</td><td><input  value="<?php echo htmlentities($user["userName"]); ?>"  class="form-control" name="username" style="margin-bottom:-2px;" required type="text"></td></tr>
                              
                              <tr><td>Password:</td><td><a  onClick="return confirm('Do you want to reset this User password?');" href="reset_user_password.php?user=<?php echo base64_encode($id);?>">Reset password to default</a> 
                              </td></tr>
                              
                                                                          
                              <tr><td>Image:</td><td><input name="photo1"  value="<?php echo htmlentities($user["userImage"]); ?>" hidden ><input type="file" style="margin-bottom:-2px;" class="form-control"  name="photo"><br /> <a style="text-decoration:none; color:red;">NB: If You don't want to change photo just skip it</a></td></tr>
                              
                        
                              
                              <tr><td></td><td><input name="update" type="submit" class="btn btn-success" style="margin-bottom:2px;margin-top:8px;"  value="Update User Information"></td></tr>

                           </form>
                       </table>

                            <?php } } ?>
                        </center>
          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>