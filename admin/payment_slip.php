<?php
ob_start();
if(isset($_GET['id'])){
require_once 'header_link.php';
require_once 'number_to_word.php';
$r = $db_handle->getPaymentDetails(base64_decode($_GET['iid']));
if(count($r)>0){
require('../fpdf/fpdf.php');
class PDF extends FPDF
{
	function Header()
	{
		$this ->SetFont('Times','B',18);;
		$this->Cell(200,10,invoiceCompanyTitle(),0,0,'C');
		$this->Ln(6);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyAddress(),0,0,'C');
		$this->Ln(5);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyPhone(),0,0,'C');
		$this->Ln(5);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyEmail(),0,0,'C');
		$this->Ln(9);
	}

	function Footer()
	{
	    $this->SetY(-18);
	    $this->SetFont('Times','',10);
	    $this->Cell(0,10,"...................................................................",0,0,'R');

	    $this->SetY(-15);
	    $this->SetFont('Times','I',10);
	    $this->Cell(0,10,"Signature & Date                    ",0,0,'R');

	     $this->SetY(-15);
	    $this->SetFont('Times','I',8);
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'L');
	}
}

 $id=(base64_decode($_GET['iid']));
$r = $db_handle->getPaymentDetails($id);
foreach($r as $PaymentDetails) {
	$supId = $PaymentDetails['supId'];
    $supName = $PaymentDetails['supName'];
    $supPhone = $PaymentDetails['supPhone'];
    $supAddress = $PaymentDetails['supAddress'];
    $paymentDate = $PaymentDetails['paymentDate'];
    $paymentTime = $PaymentDetails['paymentTime'];
}

$results = $db_handle->getSupplierAllPayableAmountById($supId);
$tcredit=0;
$payable = 0;
$tdebit=0;
$trow=count($results);
if($trow>0){
foreach($results as $supPay) 
{
  $tcredit += $supPay["credit"];
  $tdebit += $supPay["debit"];
}
$payable = ($tcredit - $tdebit);


$pdf = new PDF();
$pdf->AddPage();
$pdf->AliasNbPages();

$pdf->Ln(5);
$pdf->SetFont('Times','B',12);
$pdf->Cell(70,10,"",0,0,'C');
$pdf->Cell(50,10,"Payment Receive",1,0,'C');
$pdf->Cell(70,10,"",0,0,'C');
$pdf->Ln(15);


$pdf->SetFont('Times','B',10);
$pdf->Cell(20,10,"",0);
$pdf->Cell(35,10,"Supplier Name",1);
$pdf->Cell(115,10,$supName,1,0,'L');
$pdf->Ln();

$pdf->SetFont('Times','B',10);
$pdf->Cell(20,10,"",0);
$pdf->Cell(35,10,"Supplier Phone",1);
$pdf->Cell(115,10,$supPhone,1,0,'L');
$pdf->Ln();

$pdf->SetFont('Times','B',10);
$pdf->Cell(20,10,"",0);
$pdf->Cell(35,10,"Supplier Address",1);
$pdf->SetFont('Times','',10);
$pdf->Cell(115,10,$supAddress,1,0,'L');
$pdf->Ln();


$filename = "Payment-slip-".$id.".pdf";

$pdf->Output("",$filename,"false");
}else{
	echo "<span style='color:red;font-size:15px;font-weight:bold;'><center>Invalid Purchase ID.</center></span>";
}
}else{
	echo "<h2 style='color:red;text-align:center;padding-top:250px;'>No Purchase Details Was Found.</h2>";
}
}
ob_end_flush();
?>
