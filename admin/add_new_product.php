<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>
        <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/select2/dist/css/select2.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add New Product</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <center>
                   <?php 
                   if (isset($_POST['save'])) {
                    if(isset($_POST['_MSBtoken'])){

                    $pname = ($_POST['pname']);
                    $pcode = ($_POST['pcode']);
                    $subcat_id = ($_POST['subcat_id']);
                    $quantity = ($_POST['quantity']);
                    $discription = ($_POST['discription']);
                    $sdiscription = ($_POST['sdiscription']);
                    $originalPrice = ($_POST['originalPrice']);
                    $sellingPrice = ($_POST['sellingPrice']);
                    $vat = ($_POST['vat']);
                    $discount = ($_POST['discount']);
                    $unitid = ($_POST['unitid']);
                    $image = "default.jpg";

                    if(isset($_FILES['photo']['name'])){
                      $a = explode(".", $_FILES['photo']['name']);
                      if(count($a) > 1)
                      {
                          $ext = strtolower($a[count($a) - 1]);

                          if($ext=="jpg" || $ext=="png" || $ext=="jpeg" ||  $ext=="gif"||  $ext=="bmp")
                          {
                              if(($_FILES['photo']['size']) <= (3000*1024))
                              {

                                $maxrow= $db_handle->getTotalRowNumber("tbproducts");

                                     $picture_tmp = $_FILES['photo']['tmp_name'];
                                      $picture_name = $_FILES['photo']['name'];
                                      $picture_type = $_FILES['photo']['type'];
                                     
                                      $arr1 = explode(".", $picture_tmp);
                                      $extension1 = strtolower(array_pop($arr1));  
                                      
                                      $arr = explode(".", $picture_name);
                                      $extension = strtolower(array_pop($arr));
        
                                      $image="p".rand(0,199)."_".$maxrow."_".$pcode .".".$extension;
                                      $newfilename1="p"."_".$maxrow."_".$pcode .".".$extension1;
                                      $path = '../product_image/'.$image;

                                      move_uploaded_file($picture_tmp, $path);

                              }
                          }
                        }
                      }

                    $r = $db_handle->insertNewProductInfo($pname,$pcode,$subcat_id,$quantity,$discription,$sdiscription,$unitid,$originalPrice,$sellingPrice,$vat,$discount,$image);

                    if($r==1){
                        echo "<h2 style='color:green;'>Product Information has been Successfully Inserted</h2>";

                        echo "<br />";

                        echo "<a href='add_new_product.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-sm btn-primary'>Add Another Product</a> &nbsp;&nbsp;";
                        echo "<a href='all_products_list.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-sm btn-primary'>All Products List</a>";
                    }else{
                        echo "<h2 style='color:red;'>Product Insertion Failed</h2>";

                        echo "<br />";
                        echo "<br />";
                        echo "<a href='all_products_list.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-sm btn-primary'>All Products List</a>";
                      }

                    }else{
                        echo "Invalid Token";
                    }

                   }else{ ?>


                          <table class="table table-bordered">
                          <script>
                          function getSubCat(val) {
                          $.ajax({
                          type: "POST",
                          url: "get_sub_category.php",
                          data:'category_id='+val,
                          success: function(data){
                          $("#subcat_list").html(data);
                          }
                          });
                          }
                        </script>


                        <form action="" method="POST" enctype="multipart/form-data"  >
                          <tr><td width="25%">Product Code:</td><td><input autofocus style="margin-bottom:-0px;" class="form-control" required name="pcode" type="text" value=""> <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>"></td></tr>

                          <tr><td>Product Name:</td><td><input  class="form-control" required name="pname" type="text" value="">  </td></tr>

                          <tr><td>Category:</td><td>
                          <select name="cat_id" required class="form-control select2me"  onChange="getSubCat(this.value);" >
                          <option value=""> Select Category</option>
                            <?php
                              $results = $db_handle->getCategory("","50000");
                           foreach($results as $category) {
                            ?>
                               <option value="<?php echo $category['cid']; ?>"><?php echo $category['cname']; ?></option>
                            <?php } ?>
                            </select></td></tr>

                            <tr><td>Sub-Category:</td><td>
                             <select id="subcat_list" name="subcat_id" class="form-control select2me"  required >
                                <option value="">Select </option>
                              </select>
                          </td></tr>

                          <tr><td>Quantity:</td><td><input required name="quantity" style="margin-bottom:-2px;"  class="form-control" type="number" step="any" value=""></td></tr>

                          <tr><td>Long Description:</td><td><textarea  class="form-control" required name="discription" style="margin-bottom:-2px;"></textarea></td></tr>
                          
                          <tr><td>Short Description (For Invoice)<br > Max: 70 Characters</td><td><textarea  onkeypress="if (this.value.length > 69) { return false; }" class="form-control" required name="sdiscription" style="margin-bottom:-2px;"></textarea></td></tr>

                          <tr><td>Unit:</td><td>
                            <select name="unitid"  class="form-control select2me"  required >
                                <option value=""> Select Unit</option>
                                <?php
                                $results = $db_handle->getUnit();
                                foreach($results as $unit) {
                                ?>
                                 <option value="<?php echo $unit['id']; ?>"><?php echo $unit['unitName']; ?></option>
                              <?php } ?>
                            </select>

                          </td></tr>

                          <tr><td>Original Price:</td><td><input  class="form-control" name="originalPrice" style="margin-bottom:-2px;" id='txt1' required  type="number" step="any" value=""></td></tr>

                          <tr><td>Selling Price :</td><td><input  class="form-control" name="sellingPrice" style="margin-bottom:-2px;" id='txt2' required type="number" step="any" value=""></td></tr>

                          <tr><td>Vat (%) :</td><td><input  class="form-control"  name="vat" style="margin-bottom:-2px;" id='txtVat' required type="number"  onkeyup="total();" step="any" value=""></td></tr>

                          <tr><td>Discount:</td><td><input name="discount"  class="form-control" type="number" step="any" style="margin-bottom:-2px;" onkeyup="total();" id='txt3' required value=""></td></tr>


                           <!--      <tr><td>Total Price:</td><td><input id="tot" hidden class="form-control" type="text" style="margin-bottom:-2px;"  disabled value=""></td></tr> -->


                          <tr><td>Image (Max:3MB):</td><td><input type="file" style="margin-bottom:-2px;"  class="form-control"  name="photo"></td></tr>



                          <tr><td></td><td><button name="save" type="submit" class="btn btn-success" style="margin-bottom:-2px;" ><i class="fa fa-save"></i> Save Product Information</button></td></tr>

                       </form>
                        </table>

                        <?php } ?>
                    </center>
          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/select2/dist/js/select2.js"></script>
    <script>
      $(document).ready(function() {
          $('.select2me').select2();
      });
    </script>

  </body>
</html>