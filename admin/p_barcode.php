<?php
require "../assets/msblibrary/barcode/Barcode39.php";

// set Barcode39 object
$p_code = base64_decode($_GET['code']);
$bc = new Barcode39($p_code);

// display new barcode
$bc->draw();
?>