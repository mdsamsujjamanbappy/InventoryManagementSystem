<?php 
require_once 'header_link.php';
$autofocus='autofocus';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sales</title>

    <?php include("css.php");?>
   <script src="../assets/js/jquery.min.js"></script>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Executive Panel</span></a>
            </div>

            <div class="clearfix"></div>
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->executiveMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->exuserImage();?>" alt=""><?php $my_tools->exfullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Sales</h3>
              </div>
              <div align="right">
                <a href="sales.php" class="btn btn-info"><i class="fa fa-refresh"></i> Refresh</a>
                <a href="sales.php?new_invoice=new"  class="btn btn-default"><i class="fa fa-plus-square"></i> Create New Invoice</a>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    
                    <div class="row">
                      <div class="col-md-12">
                        <center>
                          <form style="margin-top:15px;" action="" method="POST">
                             <a style=""  rel="facebox" href="add_product_by_category.php" class="btn btn-primary">Add Product By Category</a>
                             &nbsp;&nbsp;&nbsp;
                              <b>OR,</b>
                             &nbsp;&nbsp;&nbsp;
                               <input  <?php if(!isset($_POST['barcode_product'])){echo $autofocus;}?>  name="productCode" type="text" class="" style="padding:5px;width:300px" placeholder="Click here and scan product's barcode"  required >
                              <input type="submit" name="barcode_product" value="Continue" style="" class="btn btn-success">
                            </form>
                          </center>
                      </div>
                    </div>

              <?php if(isset($_POST['barcode_product'])){ ?>
                  <div class="row"  align="center">
                        <div class="col-md-12">
                        <hr  style="" >
                            <?php
                                $code = $_POST['productCode'];
                               $r = $db_handle->getTotalRowNumber("tbproducts","code","$code");
                               if ($r>0) {
                                    $results = $db_handle->getProductDetailsbyCode($code);
                                if(count($results)){
                                   foreach($results as $product) {
                            ?>

                                  <form method="POST" action="save_to_tbtmp.php" onsubmit="return cal()" >
                                    <p style="font-size:14px;color:black;margin-top:-5px;margin-bottom:25px;">
                                        Category: <b><?php echo ($product["cname"]); ?></b>
                                        &nbsp;>> &nbsp;
                                        Sub Category: <b><?php echo ($product["sub_cat_name"]); ?></b>
                                        &nbsp;>> &nbsp;
                                        Product Name: <b><?php echo ($product["pname"]); ?></b>
                                        &nbsp;>> &nbsp;
                                        Available Quantity: <span style="font-size:16px;"><b><?php echo ($product["quantity"]); ?> </b></span>
                                    </p>
                                    <input name="product_id" hidden value="<?php echo ($product["pid"]); ?>">
                                    <input name="avail_quan" id="avilable" onkeyup="cal();"  value="<?php echo ($product["quantity"]); ?>" hidden >
                                    <?php
                                      $vatamount = ($product["sellingPrice"])*($product['vat']/100);
                                    ?>
                                     <strong>Price</strong> (including Discount and VAT) <input type="text" style="padding:5px;width:150px;" name="productPrice" value="<?php echo ($product["sellingPrice"]-$product["discount"]+$vatamount); ?>"> | 
                                   
                                    <input name="quantity" onkeyup="cal();"  id="quantity" required autofocus placeholder="Quantity" type="text" style="padding:5px;width:300px;" ><span id="txt3"></span>
                                    <br ><br >
                                    <input id="add_product" name="add" type="Submit" value="Add Product to List" class="btn btn-info">
                                    <br >
                                    <br >
                                  </form>

                          <?php
                               }
                             }else{ echo "<span style='color:red;font-size:15px;font-weight:bold;'>Product is not Found!</span><br ><br >";}
                             }else{
                                  echo "<h4 style='color:red;'>Invalid Product's barcode </h4>";
                               }
                          ?>
                            <hr   style="margin-top:-5px;" >
                        </div>
                    </div>
                    <?php } ?>

                    <div class="clearfix"></div>
                    <div class="row">
                      <div class="col-md-12">
                      <hr >
                        <center>
                         <div class="row">
                        <br>
                        <?php
                            if(isset($_GET['new_invoice'])){
                              $tmpId = $_SESSION['tmpSalesNumber'];

                                $r = $db_handle->getTotalRowNumber("tbproductsales_tmp","tmpSalesNumber",$tmpId);
                                if($r<=0){
                                      unset($_SESSION['tmpSalesNumber']);
                                }else{

                                $results = $db_handle->getTmpProducts($tmpId);
                                if(count($results)>0){
                                 foreach($results as $product) {
                                    $tmpid = $product["tmpid"];
                                    $db_handle->deleteProductFromChart($tmpid);
                                    unset($_SESSION['tmpSalesNumber']);
                                 }
                               }else{}
                                }
                              }


                            if(!isset($_SESSION['tmpSalesNumber']))
                            {
                             $tmp_id1 = rand(1000000,9999999);
                             $_SESSION['tmpSalesNumber']=md5($tmp_id1);
                            }

                            $tmp_id = $_SESSION['tmpSalesNumber'];
                        ?>

                            <div class="col-md-12">
                               <?php
                                    $tmp_id = $_SESSION['tmpSalesNumber'];
                                    $results = $db_handle->getTmpProducts($tmp_id);

                                    
                               ?>
                               <table style="margin-top:-30px"; class="table table-bordered table-stripted table-hover">
                                      <thead style="font-size:15px;color:black;">
                                            <th width="3%">SN</th>
                                            <th width="35%">Product Name <br></th>
                                            <th width="4%">Quantity <br></th>
                                            <th width="12%">Rate <span style="font-size:11px;">(inc. Vat & Discount)</span></th>
                                            <th  width="8%">Sub-Total <br></th>
                                            <th width="4%">Action <br></th>
                                       </thead>
                                        <?php
                                        $i=0;
                                        $gt =0;
                                        $trow=count($results);
                                        if($trow>0){

                                         foreach($results as $product) {
                                        ?>
                                       <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td><b><?php echo ($product["pname"]); ?></b><br>
                                            <span style="font-size:11px;"><?php echo ($product["sdescription"]); ?></span>

                                            </td>
                                            <td><?php echo ($product["productQtys"])." ".($product["unitName"]); ?> </td>
                                            <td><?php echo (($product["productPriceRate"])); ?> Taka</td>
                                             <td><?php echo (($product["productPriceRate"])*($product["productQtys"])); ?> Taka</td>
                                            <td>
                                                <a style="text-decoration:none;color:red;" onClick="return confirm('Do you want to remove this Product from Chart?');"  href="remove_product.php?id=<?php echo base64_encode($product['tmpid']); ?>" >Remove</a>
                                       </tr>

                                       <?php
                                        $gt = $gt + (($product["productPriceRate"])*($product["productQtys"]));

                                        }
                                       }else{
                                          echo "<tr><td colspan='7' > <center>No Products are in Chart</center></td></tr>";
                                        }
                                        ?>
                                        <tfoot>
                                            <th colspan="4" >Total </th>
                                            <th  colspan="" style="font-size:15px;"><?php echo $gt;?> Taka</th>
                                            <th  colspan=""></th>
                                       </tfoot>
                                   </table>
                                   <div class="row">
                                    <div class="col-md-12" align="center">
                                         <a  style="margin-left:10px;" rel="facebox" href="poccessing_save_invoice.php?tot=<?php echo $gt;?>" class="btn btn-success"><i class="fa fa-save"></i>  Save Invoice</a>
                                    </div>
                                  </div>

                            </div>
                        </div>
                        </center>
                      </div>
                    </div>

                    <!-- Row -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
  </body>
</html>