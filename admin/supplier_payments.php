<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>
    <link href="../vendors/select2/dist/css/select2.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Supplier Payments</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <div class="row">
                      <div class="col-md-8 col-md-offset-2">
                         <?php 
                         if(isset($_POST['details'])){ 
                         $results = $db_handle->getSupplierAllPayableAmountById($_POST['supplierId']);

                          $tcredit=0;
                          $payable = 0;
                          $tdebit=0;
                          $trow=count($results);
                          if($trow>0){
                           foreach($results as $supPay) {
                              $supName =  $supPay["supName"];
                              $supPhone =  $supPay["supPhone"];
                              $supAddress =  $supPay["supAddress"];
                              $tcredit += $supPay["credit"];
                              $tdebit += $supPay["debit"];
                            }
                          $payable = ($tcredit - $tdebit);
                          ?>  
                          <form action="save_payment.php" method="POST">
                              <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>">
                              <input name="supplierId" hidden value="<?php echo $_POST['supplierId'];?>">
                            
                              <table class="table table-striped table-bordered table-hover">
                                <tr>
                                  <td width="30%"><strong>Supplier Name</strong></td>
                                  <td><strong><?php echo $supName;?></strong></td>
                                </tr>
                                <tr>
                                  <td>Supplier Phone</td>
                                  <td><?php echo $supPhone;?></td>
                                </tr>
                                <tr>
                                  <td>Supplier Address</td>
                                  <td><?php echo $supAddress;?></td>
                                </tr>
                                <tr>
                                  <td>Payable Amount</td>
                                  <td><strong><?php echo $payable;?> TK</strong></td>
                                </tr>
                                <tr>
                                  <td>Tendered Amount</td>
                                  <td><input autofocus required  type="number" class="form-control" step="any" name="tenderedAmount" ></td>
                                </tr>
                                <tr>
                                  <td>Description</td>
                                  <td><textarea class="form-control" name="description" ></textarea></td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td><input  type="submit" class="btn btn-primary" name="savePayment" value="Save Payment" ></td>
                                </tr>
                              </table>
                          </form>

                         <?php }else{ echo "<h3 style='color:red'>No payment details was found for selected supplier.";} }else{ ?>
                         <form action="" method="POST" accept-charset="utf-8">
                            <center>

                            <div class="form-group">
                              <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                              <strong>Select Supplier:</strong><br><br>
                                <select name="supplierId" required class="form-control select2me">
                                  <option value="">Select Supplier</option>
                                  <?php
                                    $results = $db_handle->getSupplier();
                                    $trow=count($results);
                                    if($trow>0){
                                     foreach($results as $supplier) { ?>
                                      <option value="<?php echo $supplier['id'];?>"><?php echo $supplier['supName']."(".$supplier['supPhone'].")";?></option>
                                     <?php }
                                  }
                                  
                                  ?>
                               </select>
                               <br>
                               <br>
                               <br>
                                <input type="submit" name="details" class="btn btn-primary" value="Procced To Payment">
                              </div>
                            </div>

                            </center>
                         </form>
                         <?php } ?>
                      </div>
                    </div>


          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
    <script src="../vendors/select2/dist/js/select2.js"></script>
    <script>
      $(document).ready(function() {
          $('.select2me').select2();
      });
    </script>
  </body>
</html>