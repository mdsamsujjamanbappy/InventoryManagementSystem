<script src="../vendors/jquery/dist/jquery.min.js"></script>
<?php
require_once 'header_link.php';  
$output = "";
$output .= $my_tools->invoiceHeaderExport();
$output.="
    <center><h3 style='margin-top:-10px;'> Sales report from: (".date("d-m-Y", strtotime(base64_decode($_GET['fromdate']))).") to: (".date("d-m-Y", strtotime(base64_decode($_GET['todate']))).")
    <br /></h3>
    
    <table border='1px' style='border-collapse: collapse;'>
      <thead style='color:black;''>
            <th><center>Invoice no. </center></th>
            <th>Customer Name</th>
            <th><center>Date</center></th>
            <th><center>Amount </center></th>
            <th><center>VAT </center></th>
            <th><center>Discount </center></th>
            <th><center>Profit </center></th>
            <th><center>Sub-Total </center></th>
       </thead>
       <tbody>";
    
    $extot=0;
    $gt =0;
    $ex_total_profit=0;
    $ex_tot = 0;
    $ex_mvat=0;
    $ex_special=0;
    $fromdate = base64_decode($_GET['fromdate']);
    $todate = base64_decode($_GET['todate']);
    $results = $db_handle->getDateWiseReport($fromdate,$todate);

    $trow=count($results);
    if($trow>0){
    foreach($results as $invoice) {
      $vat = $invoice["tvat"];

      $output.="<tr>";
      $output.="<td>Invoice-".$invoice["invoice_Number"]."</td>";
      $output.="<td>".$invoice["customerName"]."</td>";
      $output.="<td>".date('d-m-Y', strtotime($invoice["invoiceDate"]))."</td>";
    $mvat=0;
   
      $total = 0;
      $total_profit=0;
      $results1 = $db_handle->getProductSalesListByInvoiceNumber($invoice["invoice_Number"]);
      if($results1){
      foreach($results1 as $invoiceTotalAmount) {
        $tt = ($invoiceTotalAmount["productPriceRate"])*$invoiceTotalAmount["productQtys"];
        
        $total += $tt;
        $total_profit += $invoiceTotalAmount["profitAmount"]*$invoiceTotalAmount["productQtys"];
      }
    }

    $total_profit = $total_profit-$invoice["specialDiscount"];
    $mvat = $tt*($vat/100);
    $total = ($total);
      $ex_tot+=$total;
      $ex_total_profit+=$total_profit;
       $ex_special += $invoice["specialDiscount"]; 
       $ex_mvat+=$mvat;
       $totms=(($total+$mvat)-$invoice["specialDiscount"]);
      
      $output.="<td>".$total." TK</td>";
      $output.="<td>".$mvat." TK</td>";
      $output.="<td>".$invoice["specialDiscount"]." TK</td>";
      $output.="<td>".$total_profit." TK</td>";
      $output.="<td>".$totms." TK</td>";
      $output.="</tr>";
    $extot+=$totms;
  }}

$output.="
</tbody>
<tfoot>
   <th></th>
    <th></th>
    <th><center>Total</center></th>
    <th><center>".$ex_tot." TK </center></th>
    <th><center>".$ex_mvat." TK</center></th>
    <th><center>".$ex_special." TK</center></th>
    <th><center>".$ex_total_profit." TK</center></th>
    <th><center>".$extot." TK</center></th>
</tfoot>";

$output .="</table></center>";

echo $output;      
?>

 <script type="text/javascript">
 $(document).ready(function() {
  window.print();
 });
 </script>