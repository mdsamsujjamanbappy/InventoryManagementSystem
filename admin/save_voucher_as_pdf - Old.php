<?php
ob_start();
if(isset($_GET['voucher_id'])){
require_once 'header_link.php';
require_once 'number_to_word.php';
$r = $db_handle->getVoucherDetails(base64_decode($_GET['voucher_id']));
if(count($r)>0){
require('../fpdf/fpdf.php');
class PDF extends FPDF
{
	function Header()
	{
		$this ->SetFont('Times','B',18);;
		$this->Cell(200,10,invoiceCompanyTitle(),0,0,'C');
		$this->Ln(6);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyAddress(),0,0,'C');
		$this->Ln(5);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyPhone(),0,0,'C');
		$this->Ln(5);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyEmail(),0,0,'C');
		$this->Ln(9);
	}

	function Footer()
	{
	    $this->SetY(-18);
	    $this->SetFont('Times','',10);
	    $this->Cell(0,10,"...................................................................",0,0,'R');

	    $this->SetY(-15);
	    $this->SetFont('Times','',10);
	    $this->Cell(0,10,"Signature & Date                    ",0,0,'R');

	     $this->SetY(-15);
	    $this->SetFont('Times','I',8);
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'L');
	}
}

$voucher_id=base64_decode($_GET['voucher_id']);
$r = $db_handle->getVoucherDetails($voucher_id);
foreach($r as $voucherDetails) {
    $supName = $voucherDetails['supName'];
    $supPhone = $voucherDetails['supPhone'];
    $supAddress = $voucherDetails['supAddress'];
    $purchaseDate = $voucherDetails['purchaseDate'];
    $purchaseTime = $voucherDetails['purchaseTime'];
}

$r1 = $db_handle->getVoucherPaymentDetails($voucher_id);
foreach($r1 as $VoucherPaymentDetails) {
    $debit = $VoucherPaymentDetails['debit'];
    $credit = $VoucherPaymentDetails['credit'];
}

$pdf = new PDF();
$pdf->AddPage();
$pdf->AliasNbPages();

$pdf->SetFont('Times','B',12);
$pdf->Cell(190,8,"Purchase Number: ".$voucher_id,0,0,'C');
$pdf->Ln(5);

$pdf->SetFont('Times','',11);
$pdf->Cell(27,10,"Supplier Name: ",0,0);
$pdf->SetFont('Times','B',12);
$pdf->Cell(53,10,$supName,0,0);
$pdf->Cell(35,10,"",0,0);
$pdf->SetFont('Times','',11);
$pdf->Cell(28,10,"Supplier Phone: ",0,0);
$pdf->SetFont('Times','B',11);
$pdf->Cell(55,10,$supPhone,0,0);
$pdf->Ln(6);

$pdf->SetFont('Times','',11);
$pdf->Cell(31,10,"Supplier Address: ",0,0);
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,10,$supAddress,0,0);
$pdf->Cell(34,10,"",0,0);
$pdf->SetFont('Times','',11);
$pdf->Cell(22,10,"Date & Time: ",0,0);
$pdf->SetFont('Times','B',11);
$pdf->Cell(50,10,date("d-M-Y", strtotime($purchaseDate))."   ".$purchaseTime,0,0);
$pdf->Ln(12);

$pdf->SetFont('Times','B',12);
$pdf->Cell(13,10,"Serial",1);
$pdf->Cell(101,10,"Product Name",1);
$pdf->Cell(21,10,"Quantity",1,0,'C');
$pdf->Cell(30,10,"Purchase Rate",1,0,'C');
$pdf->Cell(30,10,"Sub-Total",1,0,'C');
$pdf->Ln();

$results = $db_handle->getPurchaseProducts($voucher_id);
$i=0;
$gt =0;
$trow=count($results);
if($trow>0){
	foreach($results as $product){
			$rate =$product["purchaseRate"];
			$pdf->SetFont('Times','',10);
			$pdf->Cell(13,7,++$i,'LT',0,'C');
			$pdf->SetFont('Times','B',10);
			$pdf->Cell(101,7,$product["pname"],'LT',0,'L');
			$pdf->SetFont('Times','',10);
			$pdf->Cell(21,7,$product["purchaseQtys"]." ".$product["unitName"],'LT',0,'C');
			$pdf->Cell(30,7,$rate." TK",'LT',0,'C');
			$pdf->Cell(30,7,$product["purchaseQtys"]*($product["purchaseRate"])." TK",'LTR',0,'C');
			$pdf->Ln(5);

			$pdf->SetFont('Times','',8);
			$pdf->Cell(13,5,"",'LB',0,'L');
			$pdf->Cell(101,5,$product["sdescription"],'LB',0);
			$pdf->Cell(21,5,"",'LB',0);
			$pdf->Cell(30,5,"",'LB',0);
			$pdf->Cell(30,5,"",'LBR',0);
			$pdf->Ln();
			$gt+=$product["purchaseQtys"]*($product["purchaseRate"]);

		if ($i==14) {
			$pdf->Ln(10);
		}

		$pdf->SetFont('Times','I',9);
		$pdf->Cell(135,7,"",'L',0,'C');
		$pdf->SetFont('Times','B',12);
		$pdf->Cell(30,7,"Total",1,0);
		$pdf->SetFont('Times','B',11);
		$pdf->Cell(30,7,$gt." TK",1,0,'C');
		$pdf->Ln();

		$pdf->SetFont('Times','I',8);
		$pdf->Cell(135,7,strtoupper(convert_number_to_words($gt)." Taka Only"),'L',0,'C');
		$pdf->SetFont('Times','B',11);
		$pdf->Cell(30,7,"Paid Amount",1,0);
		$pdf->SetFont('Times','B',11);
		$pdf->Cell(30,7,$debit." TK",1,0,'C');
		$pdf->Ln();

		$pdf->SetFont('Times','I',9);
		$pdf->Cell(135,7,"",'LB',0,'C');
		$pdf->SetFont('Times','B',11);
		$pdf->Cell(30,7,"Due",1,0);
		$pdf->SetFont('Times','B',11);
		$pdf->Cell(30,7,($credit-$debit)." TK",1,0,'C');
		$pdf->Ln();

	}
$filename = "Purchase-".$voucher_id.".pdf";

$pdf->Output("",$filename,"false");
}else{
	echo "<span style='color:red;font-size:15px;font-weight:bold;'><center>Invalid Purchase ID.</center></span>";
}
}else{
	echo "<h2 style='color:red;text-align:center;padding-top:250px;'>No Purchase Details Was Found.</h2>";
}
}
ob_end_flush();
?>
