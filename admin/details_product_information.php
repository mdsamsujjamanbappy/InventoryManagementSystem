<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Product Details</h2>
                    <ul class="nav navbar-right panel_toolbox">
                       <li><a href="all_products_list.php"><button class="btn btn-sm btn-primary"><i class="fa fa-th-list"></i> All Product List</button></a>
                       </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                      <?php
                      $id = base64_decode($_REQUEST['id']);
                      $results = $db_handle->getProductDetails($id);
                      foreach($results as $product) {
                       $code = htmlentities($product["code"]);
                      ?>
                         <table class="table table-bordered"  style="color:black;">
                          <tr><td width="25%">Product Code:</td><td  style="color:black;font-weight:800;" ><b>
                          <?php echo htmlentities($product["code"]); ?></b> &nbsp; &nbsp; &nbsp;
                          <a target="_BLANK" href="save_barcode39.php?text=<?php echo  base64_encode(htmlentities($product["code"])); ?>" class="btn btn-primary btn-xs"><i class="fa fa-download"></i> Download Barcode</a>
                          <a target="_BLANK" href="generate_qrcode.php?code=<?php echo  base64_encode(htmlentities($product["code"])); ?>"  class="btn btn-success btn-xs"><i class="fa fa-qrcode"></i> Generate QR Code</a>
                          <a data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-danger btn-xs"><i class="fa fa-barcode"></i> Bulk Barcode Download</a>
                          </td></tr>
                          <tr><td>Product Name:</td><td  style="color:black;font-weight:800;" ><b><?php echo htmlentities($product["pname"]); ?></b></td></tr>
                          <tr><td>Sub-Category:</td><td><?php echo htmlentities($product["sub_cat_name"]); ?></td></tr>
                          <tr><td>Category:</td><td><?php echo htmlentities($product["cname"]); ?></td></tr>
                          <tr><td>Description:</td><td><?php echo ($product["description"]); ?></td></tr>
                          <tr><td>Short Description:</td><td><?php echo ($product["sdescription"]); ?></td></tr>
                          <tr><td>Quantity:</td><td><b><?php echo htmlentities($product["quantity"])." ".htmlentities($product["unitName"]); ?></b></td></tr>
                          <tr><td>Original Price:</td><td><?php echo htmlentities($product["originalPrice"])." Taka"; ?></td></tr>
                          <tr><td>Selling Price:</td><td><?php echo htmlentities($product["sellingPrice"])." Taka"; ?></td></tr>
                          <tr><td>Vat:</td><td><?php echo htmlentities($product["vat"])." %"; ?></td></tr>
                          <tr><td>Discount:</td><td><?php echo htmlentities($product["discount"])." Taka"; ?></td></tr>
                          <tr><td>Image:</td><td><img width="150px" height="150px" src="../product_image/<?php echo htmlentities($product["image"]); ?>" ></td></tr>
                        </table>
                        <?php }   ?>
          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->



          <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Barcode</h4>
                </div>
                <div class="modal-body">
                  <form action="barcode_pdf.php" method="POST" target="_NEW">
                      <div class="form-group">
                        <label >Column Number</label>
                        <input name="b" value="<?php echo $code; ?>" hidden >
                        <input type="number" class="form-control" name="m" value="4" placeholder="Number of column(ex: 4)">
                      </div> 
                      <div class="form-group">
                        <label >Row Number</label>
                        <input type="number" class="form-control" name="n" value="5" placeholder="Number of row(ex: 5)">
                      </div> 
                      <div class="form-group">
                        <label >Distance between two barcode</label>
                        <input type="number" class="form-control" name="w" value="45" placeholder="Distance between two barcode(ex: 45)">
                      </div> 
                      <div class="form-group">
                        <label >Height between two barcode</label>
                        <input type="number" class="form-control" name="h" value="28" placeholder="Height between two barcode(ex: 28)">
                      </div> 

                      <div class="form-group">
                        <hr>
                        <button type="submit" class="btn btn-primary" > <i class="fa fa-refresh"></i> Generate Barcode</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
                      </div>
                  </form>
                </div>

              </div>
            </div>
          </div>
        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>