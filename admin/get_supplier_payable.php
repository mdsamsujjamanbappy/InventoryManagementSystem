<?php
require_once 'header_link.php';
if(!empty($_POST["supplier_id"])) {
	$tdebit = 0;
	$tcredit = 0;
	$tpayable=0;
	$results = $db_handle->getSupplierPayableAmountById($_POST["supplier_id"]);
	if(count($results)){
	foreach($results as $supplier) {
		$tdebit = $supplier['debit'];
		$tcredit += $supplier['credit'];
	}
	$tpayable = $tcredit - $tdebit;
	}

?>
<input type="text" id="prePayableAmount"  onchange="pay()" required disabled name="payableAmount" value='<?php echo $tpayable; ?>' class="form-control">
<?php
}
?>
