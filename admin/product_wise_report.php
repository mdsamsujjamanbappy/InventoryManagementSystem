<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>
    <link href="../vendors/select2/dist/css/select2.css" rel="stylesheet">

   <script>                              
      function getSubCat(val) {
        $.ajax({
        type: "POST",
        url: "get_sub_category1.php",
        data:'category_id='+val,
        success: function(data){
        $("#subcat_list").html(data);
        }
        });
      } 
               
      function getProductList(val) {
          $.ajax({
          type: "POST",
          url: "get_product_list.php",
          data:'sub_cat_id='+val,
          success: function(data){
              $("#product-list").html(data);
          }
          });
      }

  </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Product Wise Report</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <div class="form-group">
                     <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                      
                         <form action="" method="POST">
                         
                            <select class="form-control select2me" name="cat_id" required autofocus onChange="getSubCat(this.value);" >
                              <option value=""> Select Category</option>
                                <?php
                                  $results = $db_handle->getCategory();
                               foreach($results as $category) {
                                ?>
                                   <option value="<?php echo $category['cid']; ?>"><?php echo $category['cname']; ?></option>
                                <?php } ?>
                            </select>
                            <br ><br >
                            <select id="subcat_list"  class="form-control select2me" onChange="getProductList(this.value);"  required name="subcat_id" required ></select>
                            <br ><br >
                            <select id="product-list"  class="form-control select2me" name="productId"  required ></select>
                            <br ><br >
                           <button type="submit" name="search" class="btn btn-primary"><i class="fa fa-eye"></i> View Report</button>

                         </form> 
                         <br>
                         <br>
                      </div>
                    </div>
                  </div>

                  <?php if(isset($_POST['search'])){ ?>
                  <div class="x_content">
                  <hr>
                   <?php
                       $search = $_POST['productId'];
                       $results = $db_handle->getProductsSaleDetails($search);
                    if(count($results)){
                         echo "<script> document.location.href='view_product_wise_report.php?pid=".base64_encode($_POST['productId'])."';</script>";

                    }else{ ?>
                    <h2 style="color:red;text-align:center;"><strong>Sorry, This product is not sold yet.</strong></h2>

                   <?php } ?>
                  </div>
                  <?php } ?>

                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
    <script src="../vendors/select2/dist/js/select2.js"></script>
    <script>
      $(document).ready(function() {
          $('.select2me').select2();
      });
    </script>
  </body>
</html>