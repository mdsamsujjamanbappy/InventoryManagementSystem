<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>
   <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Date Wise Report (From:<?php echo date('d-m-Y', strtotime(($_POST['fromdate'])))?> ~ To:<?php echo date('d-m-Y', strtotime(($_POST['todate'])))?>)</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <center>
                              
                  <?php if(isset($_POST['show_report'])){  ?>
                  <br>
                    <table id="datatable" class="table table-bordered table-stripted table-hover">
                          <thead style="font-size:15px;color:black;">
                                <th width="13%"><center>Invoice no. </center></th>
                                <th width="25%">Customer Name</th>
                                <th width="12%"><center>Date</center></th>
                                <th  width="9%"><center>Amount </center></th>
                                <th width="9%"><center>VAT </center></th>
                                <th width="10%"><center>Discount </center></th>
                                <th width="10%"><center>Profit </center></th>
                                <th width="12%"><center>Sub-Total </center></th>
                           </thead>
                        <?php
                            $extot=0;
                            $gt =0;
                            $ex_total_profit=0;
                            $ex_tot = 0;
                            $ex_mvat=0;
                            $ex_special=0;
                            $fromdate = $_POST['fromdate'];
                            $todate = $_POST['todate'];
                            $results = $db_handle->getDateWiseReport($fromdate,$todate);

                            $trow=count($results);
                            if($trow>0){
                            foreach($results as $invoice) {
                              $vat = $invoice["tvat"];
                            ?>
                           <tr>
                                <td align="center">
                                <a target="_BLANK" style='font-weight:bold;' class="btn btn-link btn-xs" href="save_invoice_as_pdf.php?invoice_id=<?php echo base64_encode($invoice["invoice_Number"]);?>"><?php echo htmlentities("Invoice-".$invoice["invoice_Number"]); ?></a></td>

                                <td><?php echo htmlentities($invoice["customerName"]); ?></td>
                                <td align="center"><?php echo date('d-m-Y', strtotime(($invoice["invoiceDate"]))); ?></td>
                                <td align="center"><?php
                                $mvat=0;
                                $tt=0;
                                  $total = 0;
                                  $total_profit=0;
                                  $results1 = $db_handle->getProductSalesListByInvoiceNumber($invoice["invoice_Number"]);
                                  if($results1){
                                  foreach($results1 as $invoiceTotalAmount) {
                                    $tt = ($invoiceTotalAmount["productPriceRate"])*$invoiceTotalAmount["productQtys"];
                                    
                                    $total += $tt;
                                    $total_profit += $invoiceTotalAmount["profitAmount"]*$invoiceTotalAmount["productQtys"];
                                  }
                                }

                                $total_profit = $total_profit-$invoice["specialDiscount"];
                                $mvat = $tt*($vat/100);
                                $total = ($total);
                                  $ex_tot+=$total;
                                  $ex_total_profit+=$total_profit;
                                   $ex_special += $invoice["specialDiscount"]; 
                                   $ex_mvat+=$mvat;
                                   $totms=(($total+$mvat)-$invoice["specialDiscount"]);
                                echo htmlentities($total); ?></td>
                                
                                <td align="center">
                                   <?php echo htmlentities($mvat);  ?>
                                </td>
                                <td align="center">
                                   <?php echo htmlentities($invoice["specialDiscount"]);?>
                                </td>
                                <td align="center">
                                   <?php echo htmlentities($total_profit);  ?>
                                </td>
                                <td align="center">
                                   <?php echo htmlentities($totms); $extot+=$totms;  ?>
                                </td>
                               
                              </tr>

                           <?php

                            } } ?>

                            <tfoot>
                               <th colspan="2"></th>
                               <th colspan=""><center>Total</center></th>
                               <th colspan=""><center><b><?php echo $ex_tot;?></b></center></th>
                               <th colspan=""><center><b><?php echo $ex_mvat;?></b></center></th>
                               <th colspan=""><center><b><?php echo $ex_special;?></b></center></th>
                               <th colspan=""><center><b><?php echo $ex_total_profit;?></b></center></th>
                               <th colspan=""><center><b><?php echo $extot;?></b></center></th>
                            </tfoot>
                    </table>

                 <?php }else{  echo "<h2 style='color:red;'>Invalid Request!!!</h2>"; } ?>
                  </center>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
     <!-- bootstrap-datetimepicker -->    
    <script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    

  </body>
</html>