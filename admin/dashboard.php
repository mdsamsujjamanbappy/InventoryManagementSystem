<?php require_once 'header_link.php';
  function getLastNDays($days, $format = 'd/m'){
  date_default_timezone_set('Asia/Dhaka');

      $m = date("m"); $de= date("d"); $y= date("Y");
      $dateArray = array();
      for($i=0; $i<=$days-1; $i++){
          $dateArray[] = date($format, mktime(0,0,0,$m,($de-$i),$y)) ; 
      }
      return array_reverse($dateArray);
  }

 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard</title>
    <style type="text/css">
      @media only screen and (max-width: 992px) {
         .chart_hidden{
          display: none !important;
         }
      }
    </style>
    <?php include("css.php");    ?>
<script>
window.onload = function () {
 
var chart = new CanvasJS.Chart("chartContainer", {
  animationEnabled: true,
  theme: "light1",
  title: {
    text: "Todays Sale Statistics"
  },
  axisY: {
    suffix: "%",
    scaleBreaks: {
      autoCalculate: true
    }
  },
  data: [{
    type: "pie",
    yValueFormatString: "#,##0\"%\"",
    indexLabel: "{y}",
    indexLabelPlacement: "inside",
    indexLabelFontColor: "white",
    markerColor: "red",
    dataPoints:[ 
    <?php 

  date_default_timezone_set('Asia/Dhaka');
  $date = date('Y-m-d');
  $results = $db_handle->getProductsSaleHistory($date);
  $trow=count($results);
  if($trow>0){
    $dataPoints=array();
  foreach($results as $invoice) {

  $pid=$invoice["productId"];
  $totp_quantity=0;
    $results1 = $db_handle->getProductsSaleHistoryTotalQuantity($pid);
     foreach($results1 as $tot) {
        $pname=$tot["pname"];
          $totp_quantity +=$tot["productQtys"];
     }
     echo '{ y: '.$totp_quantity.', label: "'.$pname.'" },';
  } 
  }
   ?>
    ]
  }]
});
chart.render();
 

var chart = new CanvasJS.Chart("chartContainer1", {
  animationEnabled: true,
  theme: "dark1",
  title: {
    text: "Todays Sale Products"
  },
  axisY: {
    suffix: "%",
    scaleBreaks: {
      autoCalculate: true
    }
  },
  data: [{
    type: "column",
    yValueFormatString: "#,##0\"\"",
    indexLabel: "{y}",
    indexLabelPlacement: "inside",
    indexLabelFontColor: "white",
    markerColor: "red",
    dataPoints:[ 
           
    <?php 

  date_default_timezone_set('Asia/Dhaka');
  $date = date('Y-m-d');
  $results = $db_handle->getProductsSaleHistory($date);
  $trow=count($results);
  if($trow>0){
    $dataPoints=array();
  foreach($results as $invoice) {

  $pid=$invoice["productId"];


  $totp_quantity=0;
    $results1 = $db_handle->getProductsSaleHistoryTotalQuantity($pid);
     foreach($results1 as $tot) {
        $pname=$tot["pname"];
          $totp_quantity +=$tot["productQtys"];
     }

     echo '{ y: '.$totp_quantity.', label: "'.$pname.'" },';

  } 
  }
   ?>
    ]
  }]
});
chart.render();
 
 
var chart = new CanvasJS.Chart("chartContainer2", {
  animationEnabled: true,
  theme: "light2",
  title:{
    text: "Sell Statistics Of Last 10 Days"
  },
  axisX: {
    valueFormatString: "DD MMM"
  },
  axisY: {
    title: ""
  },
  data: [{
    color: "green",
    markerBorderColor: "red",
    type: "splineArea", 
    markerColor: "#221155",
    lineColor: "black",
    // xValueType: "dateTime",
    xValueFormat:"YYYY-MM-DD",
    yValueFormatString: "Sell Amount #,##0 Taka",
    dataPoints:[
      <?php
        $r1 = getLastNDays(10, 'Y-m-d');
        foreach ($r1 as $date) {
          $results = $db_handle->getProductsSaleAmountHistory($date);
          $trow=count($results);
          $totamount=0;
          if($trow>0){
            foreach($results as $dataArr) {
                $tot = $dataArr['dueAmount'];
                $totamount+=$tot;
            } 
          }
          // $totamount=$totamount/100;
          $date=date(strtotime(($date)))*1000;
          echo '{ x: new Date('.$date.'), y: '.$totamount.' },';
          // echo '{ x: new Date('.$date.'), y: '.$totamount.' },';
        }
        ?>

    ]
  }]
});
 
chart.render();

}


</script>
           

    <script src="canvasjs.min.js"></script>
    <style type="text/css" media="screen">
      .canvasjs-chart-canvas{
        /*position: inherit !important;*/
      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Dashboard</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12 ">
                          <h1 class="well" style='font-weight:bold;color:#3c763d;'><marquee>Welcome to Admin Panel.</marquee></h1>
                         <hr>
                      </div>
                    </div>
                    <div class="row chart_hidden">
                      <div class="col-md-6">
                         <?php if($_SESSION['ActLeft']>3 && $_SESSION['ActLeft']<7){ ?>
                          <h4 style='color:blue;font-weight:bold;'> <?php echo $_SESSION['ActLeft'];?> days left. Please contact with developers to renew.</h4>
                          <hr >
                         <?php } ?>
                         <?php if($_SESSION['ActLeft']>=0 && $_SESSION['ActLeft']<=3){ ?>
                          <h4  class="well label-danger"  style='color:red;font-weight:bold;'>Your trial period is near to expire. <?php echo $_SESSION['ActLeft'];?> days left.<br> <br> Please contact developers to renew.</h4>
                          <hr >
                         <?php } ?>
                         
                         <?php
                            $results = $db_handle->getCategory();
                            $trow=count($results);
                         ?>
                          <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                              <div class="icon"><i class="fa fa-calendar"></i></div>
                              <div class="count"><?php echo $trow; ?></div>
                              <h3 style="color:#555555">Category</h3>
                              <p></p>
                            </div>
                          </div>

                         <?php
                            $results = $db_handle->getSubCategory();
                            $trow=count($results);
                         ?>
                          <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                              <div class="icon"><i class="fa fa-clone"></i></div>
                              <div class="count"><?php echo $trow; ?></div>
                              <h3 style="color:#555555">Sub-category</h3>
                              <p></p>
                            </div>
                          </div>

                         <?php
                            $results = $db_handle->getAllProducts();
                            $trow=count($results);
                         ?>
                          <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                              <div class="icon"><i class="fa fa-chrome"></i></div>
                              <div class="count"><?php echo $trow; ?></div>
                              <h3 style="color:#555555">Products</h3>
                              <p></p>
                            </div>
                          </div>

                         <?php
                            $results = $db_handle->getUsers();
                            $trow=count($results);
                         ?>
                          <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                              <div class="icon"><i class="fa fa-users"></i></div>
                              <div class="count"><?php echo $trow; ?></div>
                              <h3 style="color:#555555">Users</h3>
                              <p></p>
                            </div>
                          </div>

                         <?php
                            $results = $db_handle->getSupplier();
                            $trow=count($results);
                         ?>
                          <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                              <div class="icon"><i class="fa fa-users"></i></div>
                              <div class="count"><?php echo $trow; ?></div>
                              <h3 style="color:#555555">Suppliers</h3>
                              <p></p>
                            </div>
                          </div>

                         <?php
                            $results = $db_handle->getInvoiceList();
                            $trow=count($results);
                         ?>
                          <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                              <div class="icon"><i class="fa fa-file"></i></div>
                              <div class="count"><?php echo $trow; ?></div>
                              <h3 style="color:#555555">Invoices</h3>
                              <p></p>
                            </div>
                          </div>

                        
                      </div>
                      <div class="col-md-6  well">

                        <div id="chartContainer" class="chart_hidden" style="height: 370px; width: 100%;border-radius:5px;"></div>
                       
                      </div>
                    </div>
                      <hr>
                    <div class="row chart_hidden">
                      <div class="col-md-12 well">
                        <div id="chartContainer1" class="chart_hidden" style="height: 370px; width: 100%;"></div>
                      </div>
                    </div>
                    <hr>
                    <div class="row chart_hidden">
                      <div class="col-md-12 well">
                        <div id="chartContainer2" class="chart_hidden" style="height: 380px; width: 100%;"></div>
                      
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
  </body>
</html>