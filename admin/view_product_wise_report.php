<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Product Wise Report</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <center>
                     <table id="datatable" class="table table-bordered table-stripted table-hover" style="color:black;">
                     <thead style="font-weight:bold;">
                       <td><center>Serial</center></td>
                       <td>Product Name</td>
                       <td><center>Product Code</center></td>
                       <td><center>Selling Quantity</center></td>
                       <td><center>Selling Date</center></td>
                       <td><center>Invoice Number</center></td>
                     </thead>
                     <tbody>
                       <?php
                       $i=0;
                       $gt=0;
                        $pid= base64_decode($_GET['pid']);
                        $results = $db_handle->getProductsSaleDetails($pid);
                        $trow=count($results);
                          if($trow>0){
                             foreach($results as $product) {
                       ?>
                       <tr>
                         <td><center><?php echo ++$i;?></center></td>
                         <td><?php echo htmlentities($product['pname']);?></td>
                         <td><center><?php echo htmlentities($product['code']);?></center></td>
                         <td><center><?php echo htmlentities($product['productQtys'])." ".htmlentities($product['unitName']);?></center></td>
                         <td><center><?php echo date('d-m-Y', strtotime((htmlentities($product['invoiceDate']))));?></center></td>
                         <td><center>
                            <a class="btn btn-primary btn-xs" target="_BLANK" href="save_invoice_as_pdf.php?invoice_id=<?php echo base64_encode($product["invoiceNumber"]);?>"><i class="fa fa-file-pdf-o"></i> Invoice-<?php echo htmlentities($product['invoiceNumber']);?></a></center>
                         </td>
                        </tr>
                       <?php
                       $unit = htmlentities($product['unitName']);
                        $gt+=$product['productQtys'];
                        } ?>
                      </tbody>
                      <tfoot style="font-weight:bold;">
                       <td></td>
                       <td></td>
                       <td></td>
                       <td><center>Total: <?php echo $gt." ".$unit;?></center></td>
                       <td></td>
                       <td></td>
                      </tfoot>
                    <?php
                      }else{ echo "<tr><td colspan='7'><span style='color:red;font-weight:bold;'><center>No data found.</center></span></td></tr>";}
                        ?>
                      
                       </table>
                      </center>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>