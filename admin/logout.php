<?php
require_once 'header_link.php';

date_default_timezone_set('Asia/Dhaka');
$date = date('Y-m-d');
$time = date("h:i:sa");
$results = $db_handle->insertLogoutActivityLog($_SESSION['adId'],"Log out", $date, $time);

unset($_SESSION['adAccess']);
unset($_SESSION['adId']);
unset($_SESSION['fname']);
echo "<script> document.location.href='../index.php';</script>";
?>