<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-user"></i> User Details  </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="users_list.php"><button class="btn btn-sm btn-primary"><i class="fa fa-th-list"></i> Users List</button></a>
                       </li>
                       <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                  <?php
                  if(isset($_GET['rsp'])){ ?>

                  <div class="alert alert-success">
                    <strong>Success!</strong> Password has been successfully reset.
                  </div>              
                    <?php
                  }
                    $id = base64_decode($_REQUEST['id']);
                      $results = $db_handle->getUserDetails($id);
                    foreach($results as $user) {
                    ?>
                      <table id="datatable" class="table table-bordered" >
                        <tr><td width="25%">FullName:</td><td><b><?php echo htmlentities($user["userFullName"]); ?></b></td></tr>
                        <tr><td>User Type:</td><td><b><?php echo htmlentities($user["userType"]); ?></b></td></tr>
                        <tr><td>Email:</td><td><?php echo htmlentities($user["userEmail"]); ?></td></tr>
                        <tr><td>Phone:</td><td><?php echo htmlentities($user["userPhone"]); ?></td></tr>
                        <tr><td>Address:</td><td><?php echo htmlentities($user["userAddress"]); ?></td></tr>
                        <tr><td>Joining Date:</td><td><b><?php echo htmlentities($user["userJoiningDate"]); ?></b></td></tr>
                        <tr><td>Username:</td><td><?php echo htmlentities($user["userName"]); ?></td></tr>
                        <tr><td>Image:</td><td><img  width="230px" class="img-thumbnail" src="../user_image/<?php echo htmlentities($user["userImage"]); ?>" ></td></tr>
                     
                      </table>
                      <?php }   ?>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>