<?php require_once 'header_link.php';
$cphone =base64_decode($_GET['cphone']); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Customer payment details(<?php echo $cphone;?>)</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                       <thead>
                          <th width="5%"><center>Serial</center></th>
                          <th width="10%"><center>Date</center></th>
                          <th width="10%"><center>Time</center></th>
                          <th width="15%"><center>Due Amount</center></th>
                          <th width="15%"><center>Paid Amount</center></th>
                          <th width="30%"><center>Payment Description</center></th>
                          <th width="15%"><center>Recieved By</center></th>
                       </thead>
                       <tbody>
                        <?php
                        $results = $db_handle->getCustomerPaymentByPhone($cphone);
                        $tdueAmount=0;
                        $tpaidAmount=0;
                        $i=0;
                        $trow=count($results);
                        if($trow>0){
                         foreach($results as $dataArr) {
                        ?>
                       <tr>
                            <td align="center"><?php echo ++$i; ?></td>
                            <td align="center"><?php echo date("d-m-Y", strtotime($dataArr["pDate"])); ?></td>
                            <td align="center"><?php echo htmlentities($dataArr["pTime"]); ?></td>
                            <td align="center"><?php echo htmlentities($dataArr["dueAmount"])." Tk"; ?></td>
                            <td align="center"><?php echo htmlentities($dataArr["paidAmount"])." Tk"; ?></td>
                            <td align="center"><?php

                             if(!empty($dataArr["invoiceNumber"])){
                              echo "<a style='color:darkblue;' target='_BLANK' href='save_invoice_as_pdf.php?invoice_id=".base64_encode($dataArr["invoiceNumber"])."'><strong>Invoice - ".$dataArr["invoiceNumber"]."</strong></a>";

                             }else{
                              echo $dataArr['payDescription'];
                             }
                              $tdueAmount+=$dataArr["dueAmount"];
                              $tpaidAmount+=$dataArr["paidAmount"];
                             ?></td>
                            <td align="center"><?php echo htmlentities($dataArr["userFullName"]); ?></td>
                       </tr>
                       <?php 
                         
                        } 
                       }else{
                          echo "<tr><td colspan='3' > <center>No data are matching.</center></td></tr>";
                        } 
                        ?>
                        </tbody>
                        <tfoot>
                          <th colspan="3"></th>
                          <th><center><?php echo $tdueAmount." Tk"; ?></center></th>
                          <th><center><?php echo $tpaidAmount." Tk"; ?></center></th>
                          <th style="color:red;"><center>Total Due: <strong><?php echo ($tdueAmount-$tpaidAmount)." Tk"; ?></strong></center></th>
                          <th></th>
                        </tfoot>
                    </table>
          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>