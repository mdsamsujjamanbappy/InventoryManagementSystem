<?php 
require_once 'header_link.php'; 
?>
<div>
    <div class="row animated  " style="color:black;">
        <div class="col-md-12">
            <form action="save_to_tbtmp.php" method="POST" onsubmit="return cal()" >
            <table class="table table-striped table-bordered responsive-utilities">
            <br /><br />
                <script>                              
                    function getSubCat(val) {
                      $.ajax({
                      type: "POST",
                      url: "get_sub_category1.php",
                      data:'category_id='+val,
                      success: function(data){
                      $("#subcat_list").html(data);
                      }
                      });
                    } 
                             
                    function getProductList(val) {
                        $.ajax({
                        type: "POST",
                        url: "get_product_list.php",
                        data:'sub_cat_id='+val,
                        success: function(data){
                            $("#product-list").html(data);
                        }
                        });
                    }

                </script>
                          <tr>
                              <td width="35%">Select Category:</td>
                              <td>
                                  <select class="form-control" name="cat_id" required autofocus onChange="getSubCat(this.value);" >
                                      <option value=""> Select Category</option>
                                        <?php
                                          $results = $db_handle->getCategory();
                                       foreach($results as $category) {
                                        ?>
                                           <option value="<?php echo $category['cid']; ?>"><?php echo $category['cname']; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td class="">Select Sub-Category:</td>
                                <td >
                                 <select id="subcat_list"  class="form-control" onChange="getProductList(this.value);"  required name="subcat_id" required >
                                  </select>
                              </td>
                          </tr>
                        <tr>
                            <td  class="">Select Product<br ><label for="chk" style="color:red;font-size:12px;font-weight:500;"> Is it new Product? &nbsp;</label><input type="checkbox" id="chk" name="c1" onclick="NewProductF(this)" style="padding-top:10px;" > </td>
                            <td class=" ">
                              <select id="product-list"  class="form-control" name="product_id"  onclick="getProductQuantity(this.value);" onChange="getProductPrice(this.value);"  required  class="select2me form-control"></select>

                              <input required  id="newProductInfo"  style="display: none" class="form-control" placeholder="Insert product code here"  name="pcode" >

                              <input required  id="newProductInfo1"  style="display: none;margin-top:10px;" class="form-control" placeholder="Insert product name Here"  name="pname" >

                             <select name="unitid"  id="newProductInfo2"  style="display: none;margin-top:10px;"  class="form-control"  required >
                              <option value=""> Select Product Unit</option>
                              <?php
                              $results = $db_handle->getUnit();
                              foreach($results as $unit) {
                              ?>
                               <option value="<?php echo $unit['id']; ?>"><?php echo $unit['unitName']; ?></option>
                              <?php } ?>
                            </select>

                              <input required  id="newProductInfo3"  style="display: none;margin-top:10px;" class="form-control" placeholder="Insert selling price here " type="number" step="any" name="psellp" >

                              <input required  id="newProductInfo4"  style="display: none;margin-top:10px;" class="form-control" placeholder="Insert VAT(%) amount here" type="number" step="any" name="pvat" >

                              <input   id="newProductInfo5"  style="display: none;margin-top:10px;" class="form-control" placeholder="Insert Discount amount here"  type="number" step="any"  name="pdiscount" >

                              <textarea id="newProductInfo6"  style="display: none;margin-top:10px;" class="form-control" placeholder="Insert product short description (MAX:70 character) here"   name="sdescription" ></textarea>

                            </td>
                        </tr>
                        <tr>
                             <td  class="">Purchase Price
                             <br ><span style="font-size:11px;">(including Discount and VAT)</span></td>
                            <td class="">
                            <div ><input required class="form-control"  name="purchaseRate" ></div>
                            </td>
                        </tr>

                        <tr>
                            <td  class="">Purchase Quantity</td>
                            <td>
                            <input required class="form-control"  name="purchaseQtys" >
                                
                            </td>
                        </tr>
						   
                        <tr>
                            <td class=" "></td>
                            <td class=" ">
                                <input type="submit" name="add_product" value="Add To List" class="btn btn-success ">
                                <input type="reset" name="" value="Reset" class="btn btn-danger ">
                            </td>
                        </tr>
            </table>
                 </form>
                        <span  class=""  type="text" disabled  > </span>
         </div>
    </div>
</div>
<script type="text/javascript">
function NewProductF (NewProduct) {

    if(document.getElementById("chk").checked){
        document.getElementById("product-list").style.display = "none";
        document.getElementById("newProductInfo").style.display = "block";
        document.getElementById("newProductInfo1").style.display = "block";
        document.getElementById("newProductInfo2").style.display = "block";
        document.getElementById("newProductInfo3").style.display = "block";
        document.getElementById("newProductInfo4").style.display = "block";
        document.getElementById("newProductInfo5").style.display = "block";
        document.getElementById("newProductInfo6").style.display = "block";

        document.getElementById("product-list").required = false;

      }else{

        document.getElementById("product-list").style.display = "block";
        document.getElementById("newProductInfo").style.display = "none";
        document.getElementById("newProductInfo1").style.display = "none";
        document.getElementById("newProductInfo2").style.display = "none";
        document.getElementById("newProductInfo3").style.display = "none";
        document.getElementById("newProductInfo4").style.display = "none";
        document.getElementById("newProductInfo5").style.display = "none";
        document.getElementById("newProductInfo6").style.display = "none";

        document.getElementById("newProductInfo").required = false;
        document.getElementById("newProductInfo1").required = false;
        document.getElementById("newProductInfo2").required = false;
        document.getElementById("newProductInfo3").required = false;
        document.getElementById("newProductInfo4").required = false;
     
      }

}

        document.getElementById("newProductInfo").required = false;
        document.getElementById("newProductInfo1").required = false;
        document.getElementById("newProductInfo2").required = false;
        document.getElementById("newProductInfo3").required = false;
        document.getElementById("newProductInfo4").required = false;
</script>
