-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2018 at 07:57 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbmsb_pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbactivity_logs`
--

CREATE TABLE `tbactivity_logs` (
  `acl_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activity` varchar(20) NOT NULL,
  `adate` date NOT NULL,
  `time` varchar(20) NOT NULL,
  `loginIp` varchar(60) NOT NULL,
  `createdDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbactivity_logs`
--

INSERT INTO `tbactivity_logs` (`acl_id`, `user_id`, `activity`, `adate`, `time`, `loginIp`, `createdDateTime`) VALUES
(37, 13103042, 'Log out', '2018-02-19', '03:19:25pm', '::1', '2018-02-19 15:19:25'),
(38, 1, 'Login', '2018-02-19', '03:19:31pm', '::1', '2018-02-19 15:19:31'),
(39, 1, 'Login', '2018-02-19', '03:26:38pm', '::1', '2018-02-19 15:26:38'),
(40, 1, 'Login', '2018-02-19', '03:34:04pm', '::1', '2018-02-19 15:34:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbcategory`
--

CREATE TABLE `tbcategory` (
  `cid` int(11) NOT NULL,
  `cname` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbcustomerpayments`
--

CREATE TABLE `tbcustomerpayments` (
  `id` int(11) NOT NULL,
  `customerPhone` varchar(20) NOT NULL,
  `dueAmount` varchar(20) NOT NULL DEFAULT '0',
  `paidAmount` varchar(20) NOT NULL DEFAULT '0',
  `pDate` date NOT NULL,
  `pTime` text NOT NULL,
  `userId` varchar(20) NOT NULL,
  `payDescription` text NOT NULL,
  `invoiceNumber` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbdestroyproduct`
--

CREATE TABLE `tbdestroyproduct` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `destroyQty` int(11) NOT NULL,
  `DateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbinvoice`
--

CREATE TABLE `tbinvoice` (
  `inid` int(20) NOT NULL,
  `invoice_Number` int(20) NOT NULL,
  `customerName` varchar(200) NOT NULL,
  `customerPhone` varchar(50) NOT NULL,
  `customerAddress` varchar(200) NOT NULL,
  `userId` int(20) NOT NULL,
  `invoiceDate` date NOT NULL,
  `invoiceTime` varchar(20) NOT NULL,
  `paymentMethodId` int(20) NOT NULL,
  `tenderedAmount` varchar(50) NOT NULL,
  `specialDiscount` varchar(50) NOT NULL DEFAULT '0',
  `tvat` varchar(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbmsb_inventory`
--

CREATE TABLE `tbmsb_inventory` (
  `id` int(11) NOT NULL,
  `truckId` varchar(250) NOT NULL,
  `sdate` varchar(250) NOT NULL,
  `act` varchar(200) NOT NULL,
  `days` varchar(250) NOT NULL,
  `edays` varchar(250) NOT NULL,
  `appId` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbmsb_inventory`
--

INSERT INTO `tbmsb_inventory` (`id`, `truckId`, `sdate`, `act`, `days`, `edays`, `appId`) VALUES
(1, 'RmVpdHNBY3RL', 'MjAxOC0wMi0xOA==', 'OTA=', 'OTA=', 'MjAxOC0wNS0xOQ==', 'c2hvcDE='),
(3, 'MjAxNjA5', '2018-01-01', 'bad8cdbc2b7501e653ab96bacd679b4e', '644fba8ac63e23a73fa075b6d811a3ec', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbproducts`
--

CREATE TABLE `tbproducts` (
  `pid` int(11) NOT NULL,
  `pname` text NOT NULL,
  `code` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `sdescription` varchar(70) NOT NULL,
  `unitid` int(11) NOT NULL,
  `originalPrice` varchar(50) NOT NULL,
  `sellingPrice` varchar(50) NOT NULL,
  `vat` varchar(20) NOT NULL DEFAULT '0',
  `discount` varchar(20) NOT NULL DEFAULT '0',
  `subCategoryId` int(11) NOT NULL,
  `quantity` varchar(100) NOT NULL DEFAULT '0',
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbproductsales`
--

CREATE TABLE `tbproductsales` (
  `productSalesId` int(11) NOT NULL,
  `invoiceNumber` int(50) NOT NULL,
  `productId` int(20) NOT NULL,
  `productPriceRate` varchar(50) NOT NULL,
  `productQtys` varchar(50) NOT NULL,
  `profitAmount` varchar(50) NOT NULL,
  `pdescription` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbproductsales_tmp`
--

CREATE TABLE `tbproductsales_tmp` (
  `tmpid` int(11) NOT NULL,
  `tmpSalesNumber` text NOT NULL,
  `productId` int(11) NOT NULL,
  `productPriceRate` varchar(10) NOT NULL,
  `profitAmount` varchar(20) NOT NULL,
  `productQtys` varchar(50) NOT NULL,
  `pdescription` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbproductunit`
--

CREATE TABLE `tbproductunit` (
  `id` int(11) NOT NULL,
  `unitName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbproductunit`
--

INSERT INTO `tbproductunit` (`id`, `unitName`) VALUES
(2, 'Pc\'s');

-- --------------------------------------------------------

--
-- Table structure for table `tbpuchaseproducts`
--

CREATE TABLE `tbpuchaseproducts` (
  `puchaseproductId` int(11) NOT NULL,
  `puchaseId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `purchaseRate` varchar(50) NOT NULL,
  `purchaseQtys` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbpuchaseproducts_tmp`
--

CREATE TABLE `tbpuchaseproducts_tmp` (
  `purTmpId` int(11) NOT NULL,
  `tmpPuchaseId` text NOT NULL,
  `productId` int(11) NOT NULL,
  `purchaseRate` varchar(50) NOT NULL,
  `purchaseQtys` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbpurchase`
--

CREATE TABLE `tbpurchase` (
  `id` int(11) NOT NULL,
  `supId` int(11) NOT NULL,
  `purDate` date NOT NULL,
  `purTime` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbpurchasepayment`
--

CREATE TABLE `tbpurchasepayment` (
  `id` int(11) NOT NULL,
  `supId` int(11) NOT NULL,
  `debit` varchar(50) NOT NULL DEFAULT '0',
  `credit` varchar(50) NOT NULL DEFAULT '0',
  `paymentDate` date NOT NULL,
  `paymentTime` varchar(50) NOT NULL,
  `purchaseId` int(11) NOT NULL,
  `pdescription` text NOT NULL,
  `paymentTo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbquotation`
--

CREATE TABLE `tbquotation` (
  `quoid` int(20) NOT NULL,
  `quotationNumber` int(20) NOT NULL,
  `customerName` varchar(200) NOT NULL,
  `customerPhone` varchar(50) NOT NULL,
  `customerAddress` varchar(200) NOT NULL,
  `userId` int(20) NOT NULL,
  `quotationDate` date NOT NULL,
  `quotationTime` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbquotationproducts`
--

CREATE TABLE `tbquotationproducts` (
  `quotationproductId` int(11) NOT NULL,
  `quotationId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quotationRate` varchar(50) NOT NULL,
  `quotationQtys` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbquotation_tmp`
--

CREATE TABLE `tbquotation_tmp` (
  `tmpid` int(11) NOT NULL,
  `tmpQuotationNumber` text NOT NULL,
  `productId` int(11) NOT NULL,
  `productPriceRate` varchar(10) NOT NULL,
  `productQtys` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbreturnstock`
--

CREATE TABLE `tbreturnstock` (
  `id` int(11) NOT NULL,
  `productId` varchar(20) NOT NULL,
  `productPriceRate` varchar(20) NOT NULL,
  `productQtys` varchar(20) NOT NULL,
  `invoiceNumber` varchar(20) NOT NULL,
  `exId` int(11) NOT NULL,
  `sellable` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbsubcategory`
--

CREATE TABLE `tbsubcategory` (
  `scid` int(11) NOT NULL,
  `sub_cat_name` varchar(200) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbsupplier`
--

CREATE TABLE `tbsupplier` (
  `id` int(11) NOT NULL,
  `supName` varchar(200) NOT NULL,
  `supType` text NOT NULL,
  `supAddress` text NOT NULL,
  `supPhone` varchar(50) NOT NULL,
  `supEmail` varchar(200) NOT NULL,
  `supEContact` varchar(50) NOT NULL,
  `addingDate` date NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbusers`
--

CREATE TABLE `tbusers` (
  `userid` int(11) NOT NULL,
  `userFullName` varchar(200) NOT NULL,
  `userTypeId` int(1) NOT NULL,
  `userPhone` varchar(50) NOT NULL,
  `userEmail` varchar(200) NOT NULL,
  `userJoiningDate` varchar(50) NOT NULL,
  `userAddress` text NOT NULL,
  `userName` varchar(200) NOT NULL,
  `userPassword` text NOT NULL,
  `userImage` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbusers`
--

INSERT INTO `tbusers` (`userid`, `userFullName`, `userTypeId`, `userPhone`, `userEmail`, `userJoiningDate`, `userAddress`, `userName`, `userPassword`, `userImage`) VALUES
(1, 'Admin', 1, '01515268864', 'samsujjamanbappy@gmail.com', '', '', 'admin', '21232f297a57a5a743894a0e4a801fc3', ''),
(4, 'User', 2, '0175653366', 'hr@feits.co', '01-01-2018', '  House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user_2_0175653366.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbusertype`
--

CREATE TABLE `tbusertype` (
  `userTypeId` int(11) NOT NULL,
  `userType` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbusertype`
--

INSERT INTO `tbusertype` (`userTypeId`, `userType`) VALUES
(1, 'Admin'),
(2, 'Executive');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbactivity_logs`
--
ALTER TABLE `tbactivity_logs`
  ADD PRIMARY KEY (`acl_id`);

--
-- Indexes for table `tbcategory`
--
ALTER TABLE `tbcategory`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbcustomerpayments`
--
ALTER TABLE `tbcustomerpayments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbdestroyproduct`
--
ALTER TABLE `tbdestroyproduct`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbinvoice`
--
ALTER TABLE `tbinvoice`
  ADD PRIMARY KEY (`inid`);

--
-- Indexes for table `tbmsb_inventory`
--
ALTER TABLE `tbmsb_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbproducts`
--
ALTER TABLE `tbproducts`
  ADD PRIMARY KEY (`pid`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `tbproductsales`
--
ALTER TABLE `tbproductsales`
  ADD PRIMARY KEY (`productSalesId`);

--
-- Indexes for table `tbproductsales_tmp`
--
ALTER TABLE `tbproductsales_tmp`
  ADD PRIMARY KEY (`tmpid`);

--
-- Indexes for table `tbproductunit`
--
ALTER TABLE `tbproductunit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpuchaseproducts`
--
ALTER TABLE `tbpuchaseproducts`
  ADD PRIMARY KEY (`puchaseproductId`);

--
-- Indexes for table `tbpuchaseproducts_tmp`
--
ALTER TABLE `tbpuchaseproducts_tmp`
  ADD PRIMARY KEY (`purTmpId`);

--
-- Indexes for table `tbpurchase`
--
ALTER TABLE `tbpurchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpurchasepayment`
--
ALTER TABLE `tbpurchasepayment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbquotation`
--
ALTER TABLE `tbquotation`
  ADD PRIMARY KEY (`quoid`);

--
-- Indexes for table `tbquotationproducts`
--
ALTER TABLE `tbquotationproducts`
  ADD PRIMARY KEY (`quotationproductId`);

--
-- Indexes for table `tbquotation_tmp`
--
ALTER TABLE `tbquotation_tmp`
  ADD PRIMARY KEY (`tmpid`);

--
-- Indexes for table `tbreturnstock`
--
ALTER TABLE `tbreturnstock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbsubcategory`
--
ALTER TABLE `tbsubcategory`
  ADD PRIMARY KEY (`scid`);

--
-- Indexes for table `tbsupplier`
--
ALTER TABLE `tbsupplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbusers`
--
ALTER TABLE `tbusers`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `userEmail` (`userEmail`);

--
-- Indexes for table `tbusertype`
--
ALTER TABLE `tbusertype`
  ADD PRIMARY KEY (`userTypeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbactivity_logs`
--
ALTER TABLE `tbactivity_logs`
  MODIFY `acl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `tbcategory`
--
ALTER TABLE `tbcategory`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tbcustomerpayments`
--
ALTER TABLE `tbcustomerpayments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbdestroyproduct`
--
ALTER TABLE `tbdestroyproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbinvoice`
--
ALTER TABLE `tbinvoice`
  MODIFY `inid` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbmsb_inventory`
--
ALTER TABLE `tbmsb_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbproducts`
--
ALTER TABLE `tbproducts`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `tbproductsales`
--
ALTER TABLE `tbproductsales`
  MODIFY `productSalesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tbproductsales_tmp`
--
ALTER TABLE `tbproductsales_tmp`
  MODIFY `tmpid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbproductunit`
--
ALTER TABLE `tbproductunit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbpuchaseproducts`
--
ALTER TABLE `tbpuchaseproducts`
  MODIFY `puchaseproductId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbpuchaseproducts_tmp`
--
ALTER TABLE `tbpuchaseproducts_tmp`
  MODIFY `purTmpId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbpurchasepayment`
--
ALTER TABLE `tbpurchasepayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbquotation`
--
ALTER TABLE `tbquotation`
  MODIFY `quoid` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbquotationproducts`
--
ALTER TABLE `tbquotationproducts`
  MODIFY `quotationproductId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbquotation_tmp`
--
ALTER TABLE `tbquotation_tmp`
  MODIFY `tmpid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbreturnstock`
--
ALTER TABLE `tbreturnstock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbsubcategory`
--
ALTER TABLE `tbsubcategory`
  MODIFY `scid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `tbsupplier`
--
ALTER TABLE `tbsupplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbusers`
--
ALTER TABLE `tbusers`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbusertype`
--
ALTER TABLE `tbusertype`
  MODIFY `userTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
