-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2018 at 07:54 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbmsb_pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbactivity_logs`
--

CREATE TABLE `tbactivity_logs` (
  `acl_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activity` varchar(20) NOT NULL,
  `adate` date NOT NULL,
  `time` varchar(20) NOT NULL,
  `loginIp` varchar(60) NOT NULL,
  `createdDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbactivity_logs`
--

INSERT INTO `tbactivity_logs` (`acl_id`, `user_id`, `activity`, `adate`, `time`, `loginIp`, `createdDateTime`) VALUES
(37, 13103042, 'Log out', '2018-02-19', '03:19:25pm', '::1', '2018-02-19 15:19:25'),
(38, 1, 'Login', '2018-02-19', '03:19:31pm', '::1', '2018-02-19 15:19:31'),
(39, 1, 'Login', '2018-02-19', '03:26:38pm', '::1', '2018-02-19 15:26:38'),
(40, 1, 'Login', '2018-02-19', '03:34:04pm', '::1', '2018-02-19 15:34:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbcategory`
--

CREATE TABLE `tbcategory` (
  `cid` int(11) NOT NULL,
  `cname` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbcategory`
--

INSERT INTO `tbcategory` (`cid`, `cname`) VALUES
(16, 'Computer'),
(17, 'Laptop'),
(18, 'Computer Accessories'),
(19, 'Software'),
(20, 'Photography'),
(21, 'Projector'),
(22, 'Storage'),
(23, 'Cosmetics');

-- --------------------------------------------------------

--
-- Table structure for table `tbcustomerpayments`
--

CREATE TABLE `tbcustomerpayments` (
  `id` int(11) NOT NULL,
  `customerPhone` varchar(20) NOT NULL,
  `dueAmount` varchar(20) NOT NULL DEFAULT '0',
  `paidAmount` varchar(20) NOT NULL DEFAULT '0',
  `pDate` date NOT NULL,
  `pTime` text NOT NULL,
  `userId` varchar(20) NOT NULL,
  `payDescription` text NOT NULL,
  `invoiceNumber` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbcustomerpayments`
--

INSERT INTO `tbcustomerpayments` (`id`, `customerPhone`, `dueAmount`, `paidAmount`, `pDate`, `pTime`, `userId`, `payDescription`, `invoiceNumber`) VALUES
(1, '01824168966', '49700', '45000', '2018-02-10', '10:56:47am', '4', '', '1'),
(2, '01824168966', '3202.5', '2000', '2018-02-09', '11:11:08am', '4', '', '2'),
(3, '01824168966', '0', '2000', '2018-02-13', '10:30:53am', '4', 'Due amount', ''),
(4, '01582311221', '4567.5', '4000', '2018-02-13', '11:42:47am', '4', '', '3'),
(5, '01582311221', '0', '567.5', '2018-02-13', '12:09:04pm', '4', 'Due Amount', ''),
(6, '01526588965', '71097.5', '65000', '2018-02-13', '02:57:26pm', '4', '', '4'),
(7, '01824169962', '24700', '24000', '2018-02-14', '12:18:16pm', '4', '', '5'),
(8, '01824169962', '33712.5', '20000', '2018-02-14', '12:19:45pm', '4', '', '6'),
(9, '01824169962', '0', '4000', '2018-02-14', '02:14:43pm', '4', 'Due Amount', ''),
(10, '01824169962', '0', '4000', '2018-02-15', '02:14:43pm', '4', 'Due Amount', ''),
(13, '01824168966', '24700', '23000', '2018-02-15', '04:24:56pm', '4', '', '10'),
(14, '01824168966', '3202.5', '3000', '2018-02-15', '04:30:17pm', '4', '', '11'),
(15, '01824168966', '26000', '25000', '2018-02-15', '05:01:58pm', '4', '', '12'),
(16, '01818817354', '24700', '20000', '2018-02-18', '09:50:15am', '4', '', '13'),
(17, '01678123231', '35175', '10000', '2018-02-18', '09:51:08am', '4', '', '14'),
(18, '016781234567', '3500.5', '2000', '2018-02-18', '10:10:47am', '4', '', '15'),
(19, '01672331428', '2205', '2000', '2018-02-19', '11:18:20am', '4', '', '16'),
(20, '1234455678', '135625', '135433', '2018-02-19', '12:31:40pm', '4', '', '17'),
(21, '01975268864', '85517.5', '50000', '2018-02-19', '02:18:58pm', '4', '', '18'),
(22, '01975268864', '0', '35517.5', '2018-02-19', '02:23:45pm', '4', 'Due Amount of Invoice 18', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbdestroyproduct`
--

CREATE TABLE `tbdestroyproduct` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `destroyQty` int(11) NOT NULL,
  `DateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbinvoice`
--

CREATE TABLE `tbinvoice` (
  `inid` int(20) NOT NULL,
  `invoice_Number` int(20) NOT NULL,
  `customerName` varchar(200) NOT NULL,
  `customerPhone` varchar(50) NOT NULL,
  `customerAddress` varchar(200) NOT NULL,
  `userId` int(20) NOT NULL,
  `invoiceDate` date NOT NULL,
  `invoiceTime` varchar(20) NOT NULL,
  `paymentMethodId` int(20) NOT NULL,
  `tenderedAmount` varchar(50) NOT NULL,
  `specialDiscount` varchar(50) NOT NULL DEFAULT '0',
  `tvat` varchar(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbinvoice`
--

INSERT INTO `tbinvoice` (`inid`, `invoice_Number`, `customerName`, `customerPhone`, `customerAddress`, `userId`, `invoiceDate`, `invoiceTime`, `paymentMethodId`, `tenderedAmount`, `specialDiscount`, `tvat`) VALUES
(1, 1, 'Mahabubur Rahman', '01824168966', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-12', '10:56:47am', 1, '45000', '0', '0'),
(2, 2, 'Mahabubur Rahman', '01824168968', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-12', '11:11:08am', 1, '2000', '0', '0'),
(3, 3, 'ACCESSORIES', '01582311221', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-13', '11:42:47am', 1, '4000', '0', '0'),
(4, 4, 'Razia Sultana Rimi', '01526588965', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-13', '02:57:26pm', 1, '65000', '0', '0'),
(5, 5, 'ACCESSORIES', '01824169962', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-14', '12:18:16pm', 1, '24000', '0', '0'),
(6, 6, 'Mahabubur Rahman', '01824169962', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-14', '12:19:45pm', 1, '20000', '0', '0'),
(7, 7, '', '01824168966', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-15', '03:18:15pm', 1, '2200', '0', '0'),
(8, 8, 'Mahabubur Rahman', '01824168966', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-15', '03:38:19pm', 1, '250000', '0', '0'),
(9, 9, 'Mahabubur Rahman', '01824168966', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-15', '03:41:26pm', 1, '20000', '0', '0'),
(10, 10, 'Mahabubur Rahman', '01824168966', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-15', '03:53:23pm', 1, '22000', '0', '0'),
(11, 11, 'Mahabubur Rahman', '01824168966', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-15', '04:30:09pm', 1, '3000', '0', '0'),
(12, 12, 'Mahabubur Rahman', '01824168966', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-15', '04:55:17pm', 1, '2500', '0', '0'),
(13, 13, 'Mizan Vai', '01818817354', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-18', '09:50:15am', 1, '20000', '0', '0'),
(14, 14, 'Bandenawaz Bagwan', '01678123231', 'Sec11, Uttara,Dhaka', 4, '2018-02-18', '09:51:08am', 1, '10000', '1', '5'),
(15, 15, 'Razzak', '016781234567', 'Dhaka', 4, '2018-02-18', '10:10:00am', 1, '2000', '1', '2'),
(16, 16, 'Bappy', '01672331428', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-19', '11:18:20am', 1, '2000', '0', '0'),
(17, 17, 'Nawaz', '1234455678', 'asdv fj hlartbrty', 4, '2018-02-19', '12:31:40pm', 1, '135433', '5000', '12.5'),
(18, 18, 'Fardin Ahmed Rubel', '01975268864', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-19', '02:18:29pm', 1, '50000', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbmsb_inventory`
--

CREATE TABLE `tbmsb_inventory` (
  `id` int(11) NOT NULL,
  `truckId` varchar(250) NOT NULL,
  `sdate` varchar(250) NOT NULL,
  `act` varchar(200) NOT NULL,
  `days` varchar(250) NOT NULL,
  `edays` varchar(250) NOT NULL,
  `appId` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbmsb_inventory`
--

INSERT INTO `tbmsb_inventory` (`id`, `truckId`, `sdate`, `act`, `days`, `edays`, `appId`) VALUES
(1, 'RmVpdHNBY3RL', 'MjAxOC0wMi0xOA==', 'OTA=', 'OTA=', 'MjAxOC0wNS0xOQ==', 'c2hvcDE='),
(3, 'MjAxNjA5', '2018-01-01', 'bad8cdbc2b7501e653ab96bacd679b4e', '644fba8ac63e23a73fa075b6d811a3ec', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbproducts`
--

CREATE TABLE `tbproducts` (
  `pid` int(11) NOT NULL,
  `pname` text NOT NULL,
  `code` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `sdescription` varchar(70) NOT NULL,
  `unitid` int(11) NOT NULL,
  `originalPrice` varchar(50) NOT NULL,
  `sellingPrice` varchar(50) NOT NULL,
  `vat` varchar(20) NOT NULL DEFAULT '0',
  `discount` varchar(20) NOT NULL DEFAULT '0',
  `subCategoryId` int(11) NOT NULL,
  `quantity` varchar(100) NOT NULL DEFAULT '0',
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbproducts`
--

INSERT INTO `tbproducts` (`pid`, `pname`, `code`, `description`, `sdescription`, `unitid`, `originalPrice`, `sellingPrice`, `vat`, `discount`, `subCategoryId`, `quantity`, `image`) VALUES
(19, 'Acer Aspire ES1-131 Intel Celeron Quad Core N3150, Black', '33.002.665', 'Processor: Intel Celeron Quad Core N3150 <br>\r\nClock speed: 1.60-2.08GHz <br >\r\nDisplay size: 11.6\"  <br>\r\nRAM: 4GB <br >\r\nStorage: 500GB HDD <br> \r\nBackup time: Up to 7 Hrs. <br />\r\nWarranty: 1 Year', 'Intel Celeron Quad Core N3150 \r\nDisplay: 11.6\" \r\nRAM: 4GB 500GB HDD', 2, '22000', '23600', '5', '80', 16, '0', 'default.jpg'),
(20, 'Acer Aspire ES1-131 Intel Celeron Quad Core N3150, Red', '33.002.666', 'Processor: Intel Celeron Quad Core N3150 <br >\r\nClock speed: 1.60-2.08GHz <br >\r\nDisplay size: 11.6\" <br >\r\nRAM: 4GB <br >\r\nStorage: 500GB HDD <br >\r\nBackup time: Up to 7 <br >\r\nWarranty: 1 year <br >', ' Intel Celeron Quad Core N3150 , 4GB RAM, 500GB HDD, 1 year Warranty', 2, '22200', '23600', '5', '0', 16, '5.5', 'p_6_33.002.666.jpg'),
(21, 'Acer Aspire ES1-131 Intel Pentium Quad Core N3700, Black', '33.002.626', 'Processor: Intel Pentium Quad Core N3700 <br >\r\nClock speed: 1.60-2.40GHz <br >\r\nDisplay size: 11.6\" <br >\r\nRAM: 4GB <br >\r\nStorage: 500GB HDD <br >\r\nBackup time: Up to 7 Hrs. <br >\r\nWarranty: 1 Year ', 'Intel Pentium Quad Core N3700, RAM: 4GB, 500GB HDD, Warranty: 1 Year', 2, '24500', '25100', '6', '300', 16, '44', 'p_7_33.002.626.png'),
(22, 'Acer Aspire ES1-131 Intel Pentium Quad Core N3700, Red', '33.002.664', 'Processor: Intel Pentium Quad Core N3700 <br >\r\nClock speed: 1.60-2.40GHz <br >\r\nDisplay size: 11.6\" <br >\r\nRAM: 4GB <br >\r\nStorage: 500GB HDD <br >\r\nBackup time: Up to 7 Hrs. <br >\r\nWarranty: 1 Year ', 'Intel Pentium Quad Core N3700, RAM: 4GB, 500GB HDD, Warranty: 1 Year', 2, '24500', '25100', '6', '50', 16, '48', 'p_8_33.002.664.png'),
(23, 'Acer Aspire ES1-431 Intel Pentium Quad Core N3700, Red', '33.002.661', 'Processor: Intel Pentium Quad Core N3700<br >\r\nClock speed: 1.60-2.40GHz<br >\r\nDisplay size: 14\"<br >\r\nRAM: 4GB<br >\r\nStorage: 500GB HDD<br >\r\nBackup time: Up to 5 hrs<br >\r\nWarranty: 1 Year ', 'Intel Pentium Quad Core N3700, RAM: 4GB, 500GB HDD, Warranty: 1 Year ', 2, '24800', '26100', '6', '300', 16, '45', 'p_9_33.002.661.jpg'),
(24, 'A Data HV620 1TB USB 3.0 HDD', '102.055.05', 'Model: HV620\r\nStorage: 1TB\r\nType: Ext HDD\r\nWarranty: 3 Year ', 'Model: HV620,\r\nStorage: 1TB,\r\nType: Ext HDD,\r\nWarranty: 3 Year ', 2, '4600', '5000', '0', '0', 59, '219', 'p_10_102.055.05.jpg'),
(25, 'Acer Aspire ES1-431 Intel Pentium Quad Core N3700, Black', '33.002.577', 'Processor: Intel Pentium Quad Core N3700<br >\r\nClock speed: 1.60-2.40GHz<br >\r\nDisplay size: 14\"<br >\r\nRAM: 4GB<br >\r\nStorage: 500GB HDD<br >\r\nBackup time: Up to 5 hrs<br >\r\nWarranty: 1 Year ', 'Intel Pentium Quad Core N3700, RAM: 4GB, 500GB HDD, Warranty: 1 Year ', 2, '24900', '26100', '0', '0', 16, '42', 'p_11_33.002.577.jpg'),
(27, 'AMD Sempron 2650 1.45GHz 2-Core 1MB cache 25W AM1 Processor', '01.004.34', 'Model - AMD Sempron 2650, Clock Speed (GHz) - 1.45GHz, Core - Dual-core (2 Core), Thread - 2, L2 Cache (MB) - 1MB, Sockets Supported - Socket AM1, Memory Type - DDR3-1333, Memory Max. (GB) - 16GB, Memory Chanel - 2, Lithography (nm) - 28nm, Integ. Graphics - AMD Radeon HD 8240, Warranty - 3 year', 'AMD Sempron 2650 1.45GHz 2-Core 1MB cache 25W AM1 Processor, Warranty ', 2, '2950', '3100', '5', '0', 23, '200', 'p_7_01.004.34.jpg'),
(28, 'A DATA 2GB DDR3 1600', '03.055.09', 'Model - A DATA, Capacity(MB) - 2GB, Type - DDR 3, Bus Speed(MHz) - 1600, Number of Pin - 240, Warranty - Product Lifetime', 'Model - A DATA, Capacity(MB) - 2GB, Type - DDR 3, Bus Speed(MHz) - 160', 2, '1250', '1350', '5', '0', 23, '200', 'p_8_03.055.09.png'),
(29, 'A DATA 4GB DDR3 1600 BUS Desktop RAM', '03.055.08', 'Model - A DATA, Capacity(MB) - 4GB, Type - DDR 3, Bus Speed(MHz) - 1600, Number of Pin - 240, Warranty - Product Lifetime', 'Model - A DATA, Capacity(MB) - 4GB, Type - DDR 3, Bus Speed(MHz) - 160', 2, '2100', '2250', '5', '0', 23, '199', 'p_9_03.055.08.png'),
(30, 'A DATA 4GB DDR4 2133 Bus Desktop RAM', '03.055.14', 'Model - A DATA 4GB, Capacity(MB) - 4GB, Type - DDR4, Bus Speed(MHz) - 2133/2400MHz, CAS Latency - CL15-15-15, Voltage - 1.2V', 'Model - A DATA 4GB, Capacity(MB) - 4GB, Type - DDR4, Bus Speed(MHz) - ', 2, '2350', '2499', '5', '0', 23, '245', 'p_10_03.055.14.jpg'),
(31, 'Apacer 2GB DDR3 1600 Desktop RAM', '03.121.13', 'Model - Apacer, Capacity(MB) - 2GB, Type - DDR3, Bus Speed(MHz) - 1600 MHz, Number of Pin - 240 Pin, CAS Latency - CL11, Voltage - 1.5V, Warranty - product Lifetime', 'Model - Apacer, Capacity(MB) - 2GB, Type - DDR3, Bus Speed(MHz) - 1600', 2, '1000', '1100', '5', '0', 23, '219', 'p_11_03.121.13.png'),
(32, 'A DATA 8GB DDR3 1600 Bus Server Ram', '03.055.10', 'Model - A DATA, Capacity(MB) - 8GB, Type - DDR3 Server RAM, Bus Speed(MHz) - 1600, Number of Pin - 240 pin, CAS Latency - CL11, Voltage - 1.35V, Warranty - Product Life Time, Others - Registered, Clock Rate 800MHz', 'Capacity(MB) - 8GB, Type - DDR3 Server RAM, Bus Speed(MHz) - 1600, Num', 2, '11000', '11450', '5', '0', 23, '217', 'p_12_03.055.10.jpg'),
(33, 'Apacer 4GB DDR3 1600 Desktop RAM', '03.121.08', 'Model - Apacer, Capacity(MB) - 4GB, Type - Desktop RAM, Bus Speed(MHz) - 1600, Number of Pin - 240, Warranty - Product lifetime', 'Capacity(MB) - 4GB, Type - Desktop RAM, Bus Speed(MHz) - 1600, Number ', 2, '1800', '1950', '5', '0', 23, '250', 'p_13_03.121.08.png'),
(34, 'Apacer 4GB DDR4 2400 BUS Panther-Golden Desktop RAM (Heatsink)', '03.121.16', 'Model - Apacer 4GB Panther-Golden (Heatsink), Capacity(MB) - 4GB, Type - DDR4, Bus Speed(MHz) - 2400MHz, Number of Pin - 260 pin, Voltage - 1.2V, Others - Memory Architecture: x8 FBGA DRAM chip, Warranty - Product Lifetime\r\n', 'Capacity(MB) - 4GB, DDR4, 2400MHz,Warranty - Product Lifetime', 2, '2200', '2400', '5', '0', 23, '230', 'default.jpg'),
(35, 'Apacer 4GB DDR4 2400 BUS Desktop RAM', '03.121.14', 'Model - Apacer 4GB, Capacity(MB) - 4GB, Type - DDR4, Bus Speed(MHz) - 2400MHz, Number of Pin - 288pin, Warranty - Product lifetime', 'Capacity(MB) - 4GB, Type - DDR4, Bus Speed(MHz) - 2400MHz, Number of P', 2, '2400', '2550', '5', '0', 23, '250', 'p_15_03.121.14.jpg'),
(36, 'Intel PDC G3250 3.20 GHz 4th Gen. Processor', '01.024.112', 'Model - Intel PDC G3250, Clock Speed (GHz) - 3.20GHz, Core - 2, Thread - 2, Smart Cache (MB) - 3MB, Sockets Supported - FCLGA1150, Bus Ratio - 1333/1600, Memory Type - DDR3 / DDR3L, Memory Max. (GB) - 32GB, Memory Chanel - 2, Lithography (nm) - 22nm, Integ. Graphics - Intel HD Graphics, Warranty - 3 year', ' \r\n', 2, '4500', '4800', '6', '0', 22, '240', 'p_16_01.024.112.jpg'),
(37, 'Intel Core i3', '01.024.129', 'Model - Intel Core i3 4170, Clock Speed (GHz) - 3.70GHz, Core - 2, Thread - 4, L2 Cache (MB) - 3MB, Sockets Supported - LGA1150, Lithography (nm) - 22nm, Integ. Graphics - Intel HD 4400, Warranty - 3 Year', ' Intel Core i3 4170 3.70GHz 3MB Cache LGA1150 4th Gen.Processor', 2, '8850', '9000', '5', '0', 22, '232', 'default.jpg'),
(38, 'Intel Skylake Core i5 6500 3.20GHz 6th Gen. Processor', '01.024.116', 'Model - Intel Skylake Core i5 6500, Clock Speed (GHz) - 3.20GHz, Core - 4, Thread - 4, Smart Cache (MB) - 6MB, Sockets Supported - LGA1151, Lithography (nm) - 14nm, Integ. Graphics - Intel HD 530, Others - DDR4-1866/2133, DDR3L-1333/1600 1.35V, Specialty - Skylake, Warranty - 3 year', ' ', 2, '15650', '16000', '5', '0', 22, '200', 'p_18_01.024.116.jpg'),
(39, 'Intel Skylake Core i7 6700K 4.0GHz 6th Gen. Processor', '01.024.115', 'Model - Intel Skylake Core i7 6700K, Clock Speed (GHz) - 4.0GHz, Core - 4, Thread - 8, Smart Cache (MB) - 8MB, Sockets Supported - LGA1151, Lithography (nm) - 14nm, Integ. Graphics - Intel HD 530, Specialty - Skylake, Warranty - 3 year', ' ', 2, '27750', '28100', '5', '0', 22, '220', 'p_19_01.024.115.jpg'),
(40, 'AMD Bulldozer FX-6300 3.5GHz 6-Core 14MB Cache 95W AM3+ Processor', '01.004.33', 'Model - AMD Bulldozer FX6300, Clock Speed (GHz) - 3.50GHz, Core - 6 Core, Smart Cache (MB) - 14MB, L3 Cache (MB) - 8MB Shared, L2 Cache (MB) - 6MB, Sockets Supported - AM3+, Lithography (nm) - 32nm, Warranty - 3 year', ' ', 2, '9900', '10100', '5', '0', 22, '250', 'p_20_01.004.33.jpg'),
(41, 'AMD APU A10 7700K 3.4GHz 4-Core 4MB cache 95W FM2+ Processor', '01.004.38', 'Model - AMD APU A10 7700K, Clock Speed (GHz) - 3.4GHz - 3.8GHz, Core - 4, L2 Cache (MB) - 2 x 2MB, Sockets Supported - FM2, Integ. Graphics - Radeon R7 graphics, Others - GPU Clock Speed: 720MHz, Manufacturing Tech: 28nm, DDR3 Speed: 2133MHz, Warranty - 1 year', ' ', 2, '14150', '14600', '5', '0', 22, '254', 'p_21_01.004.38.jpg'),
(42, 'Asus EN210 SILENT/DI/1GD3 1GB DDR3 Graphics Card', '14.006.116', 'Chipset - NVIDIA GeForce 210, Model - Asus EN210 SILENT/DI/1GD3, Interface - PCI Express 2.0, GPU Clock (MHz) - 589MHz, Memory Type - DDR3, Memory - 1GB, Memory Bus (bit) - 64-bit, Memory Clock (MHz) - 1200MHz, Resolution Max. (Pixel) - 2560 x 1600, VGA Port - 1 x D-Sub, DVI Port - 1 x (DVI-I), HDMI Port - 1, Power Supply Req. (Watt) - up to 75W, no additional PCIe power required, Warranty - 2 Year', ' ', 2, '2850', '3050', '5', '0', 64, '293', 'default.jpg'),
(43, 'Asus R7 240 2GB DDR3', '14.006.51', 'Chipset - AMD Radeon R7 240, Model - Asus R7 240, Interface - PCI Express 3.0, GPU Clock (MHz) - 730 MHz, Memory (MB) - 2GB, Memory Type - DDR3, Memory Bus (bit) - 128 bit, Memory Clock (MHz) - 900 MHz, Resolution Max. (Pixel) - 1920 x 1200, VGA Port - 1, DVI Port - 1 (DVI-D), HDMI Port - 1, Power Supply Req. (Watt) - 75W (No additional PCIe power required), Warranty - 2 year', ' ', 2, '5950', '6100', '5', '0', 64, '299', 'p_23_14.006.51.png'),
(44, 'Asus ROG STRIX-RX480-O8G-GAMING 8GB GDDR5 ', '14.006.113', 'Chipset - AMD Radeon RX 480, Model - Asus ROG STRIX-RX480-O8G-GAMING, Interface - PCI Express 3.0, GPU Clock (MHz) - 1330 MHz (OC Mode), Memory Type - GDDR5, Memory - 8GB, Memory Bus (bit) - 256-bit, Memory Clock (MHz) - 8000MHz, Resolution Max. (Pixel) - 7680 x 4320, Open GL Support - 4.5, DVI Port - 1 x (Native) (DVI-D), HDMI Port - 2 x (Native) (HDMI 2.0), Others - Display Port : Yes x 2 (Native) (Regular DP), HDCP Support : Yes, Warranty - 2 Year', ' ', 2, '30600', '31150', '5', '0', 64, '298', 'default.jpg'),
(45, 'GIGABYTE AMD A68 GA-F2A68-DS2', '02.018.163', 'Model - AMD A68 GA-F2A68-DS2, Form Factor - Micro ATX Form, Sockets - FM2+, Supported CPU - AMD A series / AMD Athlon series processors, Chipset - AMD A68H, RAM Type - DDR3, RAM Bus (MHz) - 2400(OC)/2133/1866/1600/1333MHz, RAM Max. (GB) - 64GB, RAM Slot - 2, PCI Express x16 Slot - 1 x PCI Express x16 slot, running at x16, SATA Port - 4 x SATA 6Gb/s, Audio Chipset - Realtek ALC887 codec, Audio Channel - 2/4/5.1/7.1-channel, LAN Chipset - Realtek GbE LAN chip, LAN Speed (Mbps) - 10/100/1000Mbit, USB Interface - USB3.0/2.0, USB2.0/1.1, USB Port - 2 x USB3.0/2.0, 8 x USB2.0/1.1, VGA Port - 1, DVI Port - 1 x DVI-D, Warranty - 3 year', ' ', 2, '4300', '4450', '5', '0', 21, '415', 'p_25_02.018.163.jpg'),
(46, 'Gigabyte GA-H81M-S2PV 4TH Gen', '02.018.69', 'Model - GA-H81M-S2PV, Form Factor - Micro ATX, Sockets - LGA1150, Supported CPU - Intel Core i7/ i5/ i3/ Pentium/ Celeron Processors, Chipset - Intel H81, RAM Type - DDR3 DIMM, RAM Bus (MHz) - 1600/1333 MHz, RAM Max. (GB) - 16GB, RAM Slot - 2, PCI Express x16 Slot - 1, SATA Port - 4, Audio Chipset - Realtek ALC887 codec, High Definition Audio, Audio Channel - 2/4/5.1/7.1 Channel, LAN Chipset - Realtek GbE LAN chip, LAN Speed (Mbps) - 10/100/1000 Mbit, USB Interface - USB 3.0/2.0, USB Port - 8, DVI Port - 1, Warranty - 3 year, Others - Supported OS- Windows 8/7', ' ', 2, '4650', '4900', '5', '0', 21, '200', 'p_26_02.018.69.png'),
(47, 'Gigabyte GA-E2100N', '02.018.108', 'Model - Gigabyte GA-E2100N, Form Factor - Mini-ITX, Sockets - Built-in Processor, Supported CPU - Built-in AMD E1-2100 dual-core APU, RAM Type - DDR3 DIMM, RAM Bus (MHz) - 1333 MHz, RAM Max. (GB) - 32GB, RAM Slot - 2, PCI Express x16 Slot - 1, SATA Port - 2, Audio Chipset - Realtek ALC887 codec, Audio Channel - 2/4/5.1/7.1 Channel, Video Chipset - Radeon HD 8210 SoC, LAN Chipset - Realtek GbE LAN c, LAN Speed (Mbps) - 10/100/1000 Mbit, USB Interface - USB 3.0/2.0, USB Port - 10, HDMI Port - 1, Warranty - 3 year', ' ', 2, '5480', '5700', '5', '0', 21, '200', 'p_27_02.018.108.jpg'),
(48, 'Samsung S19F350 18.5 Inch LED Monitor', '08.039.92', 'Model - Samsung S19F350, Display Size (Inch) - 18.5 Inch, Shape - Wide Screen, Display Type - LED Display, Display Resolution - 1366 x 768, Brightness (cd/m2) - 250 cd/m2, Contrast Ratio (TCR/DCR) - 1,000:1, Response Time (ms) - 14ms, TV Card (Built-in) - No, VGA Port - 1 x D-sub, Remote - No, Others - Viewing Angle:178 degree - 178 degree, Wall Mount: Yes (75 x 75), Warranty - 3 year', ' ', 2, '6100', '6300', '5', '0', 25, '150', 'p_28_08.039.92.jpg'),
(49, 'Samsung LS22F350FHW Full HD LED Monitor', '08.039.95', 'Model - Samsung LS22F350FHW, Display Size (Inch) - 21.5\", Shape - Widescreen, Display Type - FHD LED Display, Display Resolution - 1920 x 1080, Brightness (cd/m2) - 200cd/m2, Contrast Ratio (TCR/DCR) - 1000:1, Response Time (ms) - 5ms, Refresh Rate (Hz) - 60Hz, VGA Port - 1 x D-Sub, HDMI Port - 1, Others - Viewing Angle (H/V): 170degree (H) / 160degree (V), Warranty - 3 year', ' ', 2, '10150', '10300', '5', '0', 25, '149', 'p_29_08.039.95.jpg'),
(50, 'Samsung S24F350FHW 23.5 Inch Full HD LED Monitor', '08.039.99', 'Model - Samsung S24F350FHW, Display Size (Inch) - 23.5\", Shape - Wide Screen, Display Type - FHD LED Display, Display Resolution - 1920 x 1080, Brightness (cd/m2) - 250 cd/m2, Contrast Ratio (TCR/DCR) - 1000:1, Response Time (ms) - 4ms (GTG), Refresh Rate (Hz) - 60Hz, VGA Port - 1 x D-Sub, HDMI Port - 1, Others - Viewing Angle (H/V): 178degree - 178degree, Warranty - 3 year', ' ', 2, '15500', '16000', '5', '0', 25, '150', 'default.jpg'),
(51, 'Dell E1715S 17 Inch Square LED Monitor', '08.013.47', 'Model - Dell E1715S, Display Size (Inch) - 17\'\', Shape - Square Screen, Display Type - LED Display, Display Resolution - 1280 x 1024, Brightness (cd/m2) - 250cd/m2, Contrast Ratio (TCR/DCR) - 1000:1, Response Time (ms) - 5ms, Refresh Rate (Hz) - 60Hz, Warranty - 3 year, Part No - E1715S', ' ', 2, '8200', '8500', '5', '0', 25, '137', 'p_31_08.013.47.jpg'),
(52, 'Viewsonic 19.5 Inch VA2055SA TFT LCD Monitor', '08.050.48', 'Model - Viewsonic VA2055SA, Display Size (Inch) - 19.5\", Shape - Wide Screen, Display Type - TFT LCD Display, Display Resolution - 1920 x 1080, Brightness (cd/m2) - 250cd/m2, Contrast Ratio (TCR/DCR) - 3,000:1, Response Time (ms) - 16ms, VGA Port - VGA x 1, Others - Viewing Angles : 178degree - 178degree, WALL MOUNT: 100x100mm, Consumption : 24 W,, Warranty - 3 year', ' ', 2, '7000', '7250', '5', '0', 25, '148', 'default.jpg'),
(53, 'Power Logic Futura NEO 500XV ATX Casing (PSU)', '07.277.05', 'Model - Futura NEO 500XV, Supported Mainboard Type - ATX, Power Supply - 600WT, USB port - 2 x USB port, Audio Ports - 1 x Audio port, 1 x Microphone port, 5.25 - 1 X 5.25\" internal & external drive bay, 3.5 - 1 X 3.5\" internal drive bay, Cooling Fan - 1 X 80mm rear silent fan, Warranty - no', ' ', 2, '2000', '2100', '5', '0', 26, '192', 'default.jpg'),
(54, 'Power Logic Xenon Graphite X5 ATX Casing (PSU)', '07.277.08', 'Model - Power Logic Xenon Graphite X5, Supported Mainboard Type - ATX, Power Supply - 450 watt, USB port - 2 x USB2.0, Audio Ports - 1 x HD-audio port, 1 x Microphone, 5.25 - 2 x 5.25\" external drive / 2 x 5.25\" internal drive, 3.5 - 4 x 3.5\" internal drive, Cooling Fan - 1 x 80mm rear silent fan, Warranty - no', ' ', 2, '3000', '3100', '5', '0', 26, '199', 'p_34_07.277.08.jpg'),
(55, 'Black Cat 500W PSOL Power Supply', '55.193.01', 'Model - Black Cat 500W, Maximum Power (WT) - 500W, Warranty - 0', ' ', 2, '700', '800', '5', '0', 27, '199', 'p_35_55.193.01.jpg'),
(56, 'PC Power 1200VA', '11.492.03', 'Model - PC Power 1200VA, Type - Offline, Input Voltage (V) - 230V, Frequency (Hz) - 50/60Hz, Warranty - 1 year', '  PC Power 1200VA, Input Voltage (V) - 230V, Frequency (Hz) - 50/60Hz,', 2, '4600', '5000', '5', '0', 29, '248', 'p_36_11.492.03.jpg'),
(57, 'PC Power 650VA', '11.492.01', 'Model - PC Power 650VA, Type - Offline, Input Voltage (V) - 100-270, Frequency (Hz) - 50/60, Warranty (Year) - 1', 'Model - PC Power 650VA, Type - Offline, Input Voltage (V) - 100-270, F', 2, '2600', '2700', '5', '0', 29, '289', 'p_37_11.492.01.jpg'),
(58, 'Apollo 650VA UPS', '11.057.01', 'Model - Apollo 650VA, Type - Offline, Input Voltage (V) - 240, Frequency (Hz) - 60, Transfer Rate (ms) - 2, Stabilizer - Built-in, Warranty (Year) - 1 Year', ' ', 2, '2700', '2800', '5', '0', 29, '296', 'p_38_11.057.01.jpg'),
(59, 'Apollo 1200VA UPS', '11.057.03', 'Model - Apollo 1200VA, Type - Offline, Input Voltage (V) - 240, Output Voltage (V) - 120, Frequency (Hz) - 60, Transfer Rate (ms) - 2, Stabilizer - Built-in, Warranty (Year) - 1', ' ', 2, '4900', '5100', '5', '0', 29, '299', 'p_39_11.057.03.jpg'),
(60, 'Sonic Gear MORRO 2 2.1 Bluetooth Speaker', '10.176.12', 'Model - Sonic Gear MORRO 2, Type - Bluetooth Speaker, Channel - 2.1, RMS/Channel (Watt) - 3Watt x 2, RMS/Subwoofer (Watt) - 19Watt, Frequency (Hz - KHz) - 180Hz - 20KHz (Satellite) / 40Hz - 200Hz (Subwoofer), Feature - Side-access power button and volume control panel, USB 5V port for charging small media devices; compatible with Bluetooth-enabling dongle for playing music remotely from Bluetooth media device, Impedance : 4 Ohms (Satellite) / 8 Ohms (Subwoofer), Warranty - 1 year', ' ', 2, '2000', '2100', '5', '0', 30, '67', 'p_40_10.176.12.gif'),
(61, 'F&D T60X Bluetooth 2:1 Tower Speaker(with optical port)', '10.107.32', 'Model - F&D T60X, Type - Bluetooth Tower Speaker, Channel - 2:1, RMS/Channel (Watt) - 55Watt x 2, Signal to Noise Ratio (dB) - 70dB, Frequency (Hz - KHz) - 20 - 20 KHz, Remote Control - Yes, Feature - Separation: 45dB, can use SD card, can use, FM radio, Specialty - Bluetooth with optical port, Warranty - 1 year', ' ', 2, '11500', '12000', '5', '0', 30, '49', 'p_41_10.107.32.gif'),
(62, 'Gadmei Black TV3860E External TV Card', '17.067.11', 'Model - Gadmei TV3860E, Interface - External, Resolution (pixel) - 1440x900, Channel Preview - Yes, Recording - No, Speaker - Built in, Remote - Yes, Warranty - 1 year', '  ', 2, '1150', '1200', '5', '0', 36, '100', 'p_42_17.067.11.jpg'),
(63, 'AVerTV Volar HD 830 USB TV Card', '17.008.09', 'Model - AVerTV Volar HD 830, Interface - USB2.0, Resolution (pixel) - 1080i / 720p, Recording - Schedule Recording, Remote - Yes, Specialty - Os: Windows 8 / 7 / Vista / XP, Warranty - 3 year', ' ', 2, '4400', '4600', '5', '0', 36, '199', 'p_43_17.008.09.jpg'),
(64, 'Netgear PTV3000 Wireless Push 2 TV', '44.034.01', 'Brand - Netgear, Model - Netgear PTV3000 Adapters, Type - Wireless Display, Features - 802.11n 300 Mbps WiFi (b/g/n compatible) with WEP, WPA and WPA2 support,Full HD video Output,Requires a laptop, smartphone, or tablet with Intel? WiDi or Miracast wireless display, Warranty - 1 year', ' ', 2, '5500', '5800', '5', '0', 36, '60', 'p_44_44.034.01.jpg'),
(65, 'Value Top VT390 External', '17.130.01', 'Model - Value Top VT390, Interface - External, Resolution (pixel) - 800x600,1024x768,1280x1024, 1440x900,1680x1050, 1920x1080,1920x1200, Recording - No, Speaker - Yes, Warranty - 1 year, Part No - VT390', ' ', 2, '1800', '2000', '5', '0', 36, '1000', 'default.jpg'),
(66, 'Asus O!Play Mini HD Media Player', '97.006.01', 'Asus O!Play Mini HD Media Player', ' ', 2, '6300', '6500', '5', '0', 33, '60', 'p_46_97.006.01.jpg'),
(67, 'Cenix VR-P2340 4GB', '120.403.01', 'Brand - Cenix, Model - Cenix VR-P2340, Type - Voice Recorder, Memory - 4GB', '  Brand - Cenix, Model - Cenix VR-P2340, Type - Voice Recorder, Memory', 2, '4800', '5000', '5', '0', 35, '97', 'p_47_120.403.01.png'),
(68, 'Sony ICD-440 4GB Voice Recorder', '120.045.04', 'Brand - Sony, Model - Sony ICD-440, Type - Voice Recorder, Memory - 4GB, USB - Yes, Audio Formats - MP3, PC Conectivity - Yes, Battery Type - 2 AAA NiMH batteries, Head phone Jack - Yes, Radio Recording - Yes, Digital Noise Canceling - Yes, Track Mark - Yes, Others - 128kbps Recording Time: 67 Hrs 05 Min, Battery life time: 57 Hrs, Warranty - No', ' ', 2, '5100', '5300', '5', '0', 35, '120', 'p_48_120.045.04.jpg'),
(69, 'Microlab K310 Single Port Black Head Phone', '32.031.10', 'Model - Microlab K310, Type - Single Port Black Head Phone, Connectivity - Single Port, Driver Unit (mm) - 40 mm diameter, Impedance (ohm) - 32 Ohm, Sensitivity (dB) - 108 dB + 3 dB, Frequency Response (Hz - kHz) - 15 Hz - 20 kHz, Microphone - No, Cable Length (ft) - 3.5 mm stereo, Others - Ear cups size: 70 mm (W) x 100 mm (L), Output Power: 50mW', ' ', 2, '850', '900', '5', '0', 32, '98', 'p_49_32.031.10.jpg'),
(70, 'Microlab K380 Single Port Black Head Phone', '32.031.13', 'Model - Microlab K380, Type - Single Port White Head Phone, Connectivity - Single Port, Impedance (ohm) - 32 Ohm, Sensitivity (dB) - 108 dB + 3 dB, Frequency Response (Hz - kHz) - 15 Hz - 20 kHz, Microphone - Yes, Cable Length (ft) - 3.5 mm stereo, Warranty - 1 year, Others - Ear cups size: 60 mm (W) x 78 mm (L), Output Power: 50mW', '  ', 2, '1400', '1600', '5', '0', 32, '100', 'p_50_32.031.13.jpg'),
(71, 'Microsoft Windows 10 Home', '54.191.51', 'Model - Microsoft Windows 10, Software Type - Operating Microsoft Windows 10, System Req (Processor) - 1GHz, System Req (RAM) - 1 gigabyte (GB) for 32-bit or 2 GB for 64-bit, System Req (Disk Space) - 16 GB for 32-bit OS 20 GB for 64-bit OS, Display - 800 x 600, Others - Graphics card: DirectX 9 or later with WDDM 1.0 driver', ' ', 2, '8600', '9000', '5', '0', 43, '246', 'p_51_54.191.51.jpg'),
(72, 'Microsoft Windows 10 Professional', '54.191.50', 'Model - Windows 10, Software Type - Microsoft Windows 10 Professional, System Req (Processor) - 1GHz, System Req (RAM) - 1 gigabyte (GB) for 32-bit or 2 GB for 64-bit, System Req (Disk Space) - 16 GB for 32-bit OS 20 GB for 64-bit OS, Display - 800 x 600, Others - Graphics card: DirectX 9 or later with WDDM 1.0 driver', ' ', 2, '10600', '11200', '5', '0', 43, '245', 'p_52_54.191.50.jpg'),
(73, 'Microsoft Office Home & Business 2016', '54.191.52', 'Model - Microsoft Office Home & Business 2016, Software Type - Microsoft Office, Supporting OS - Windows 7 or later, Windows Server 2008 R2, or Windows Server 2012, User Limit - Singel User, System Req (Processor) - 1GHz, System Req (RAM) - 1GB (32 bit), 2GB (64 bit), System Req (Disk Space) - 3GB, System Req (DVD ROM) - ENGLISH APAC EM DVD, Display - 1280 x 800, Others - Graphics : DirectX 10 graphics card', ' ', 2, '16200', '16500', '5', '0', 42, '98', 'p_53_54.191.52.jpg'),
(74, 'Microsoft Office 365 Personal 32-bit/x64 English', '54.191.48', 'Model - Microsoft Office 365, Software Type - Application, Supporting OS - Windows7 or higher, User Limit - 1 user, Product Includes - All MS Office application, Others - 1 Year Subscription', ' ', 2, '3300', '3700', '5', '0', 42, '99', 'p_54_54.191.48.jpg'),
(75, 'Bijoy Bayanno Bangla Software 2016', '54.319.04', 'Model - Bijoy Bayanno Bangla Software 2016, Software Type - Bangla Software, Supporting OS - Windows Platform, User Limit - 1 User', ' ', 2, '280', '320', '5', '0', 44, '100', 'default.jpg'),
(76, 'Nikon D5200 Digital SLR Camera only Body', '91.035.15', 'Model - Nikon D5200, Effective pixels - 24.1 Mega Pixel, Lens Mount - No, Sensor Type - CMOS, Display - 3\" LCD Dispaly, View Finder Type - Pentamirror (95% Coverage), Shutter Speed - 1/4000 to 30, Face Detection - 1 to 19 EV (ISO 100, 68F/20C), Red-Eye Reduction - Yes, Image - 6016 x 4000, Video - 1920 x 1080, Memory Type - SD, SDHC, SDXC, USB - Hi-Speed 2.0, Battery - Lithium-ion Battery EN-EL14, Body Dimensions - 129 x 98 x 78mm, Weight - 455gm, Warranty - 1 year, Specialty - Full HD 1080p30 video, Internal Memory (MB): Yes', '  ', 2, '30500', '33500', '5', '0', 45, '52', 'p_56_91.035.15.jpg'),
(77, 'Canon EOS 7D Mark-II Digital SLR Camera only Body', '91.010.26', 'Model - Canon EOS 7D Mark-II, Effective pixels - 20.2 Mega Pixel, Lens Mount - No, Sensor Type - CMOS, Display - 3\" LCD Display, Playback zoom - 1.5x - 10x, View Finder Type - Pentaprism (100% Coverage), Shutter Speed - 1/8000 to 30, Face Detection - Yes, Red-Eye Reduction - Yes, Image - 5472 x 3648, Video - 1920 x 1080, Memory Type - Compact Flash, SD, SDHC, SDXC, USB - USB 3.0 (5 GBit/sec), Battery - LP-E6N Lithium-ion, Body Dimensions - 148.6 x 112.4 x 78.2 mm, Weight - 910gm, Specialty - Shoot continuous bursts of images at up to 10 frames per second.,Shoot at high frame rates and enjoy superb colour reproduction,Each scene is dissected and analysed for consistent accurate exposure metering,Focus on subjects wherever they are, with 65 cross-type AF points spread across the frame.,Capture movies at up to 1080p resolution and 60fps frame rate. Mic and headphone sockets for full audio control.,Dual memory cards slots enable automatic back up as you shoot, or extended storage capacity.,Produce great quality photos that are packed with fine detail.,Enjoy smooth continuous focusing when shooting movies and photos in Live View mode.,Geotag each photo and movie you shoot with your current location ? great for travel photographers., Warranty - 1 year', ' 20.2 Mega Pixel, Shutter Speed - 1/8000 ,CMOS, 3\" LCD Display', 2, '98000', '106000', '5', '0', 46, '99', 'p_57_91.010.26.jpg'),
(78, 'Twinmos X3 16GB USB 3.0', '18.048.28', 'Model - Twinmos X3, Interface - USB 3.0, Capacity (GB) - 16GB, Access Time Read (MBps) - 54 MBps, Access Time Write (MBps) - 40 MBps, OS Supported - Windows 2000/ XP/ Vista/ 7-Mac OS 10.4 or later -Linux Kernel 2.4 or later, Warranty - 1 year', ' ', 2, '550', '600', '5', '0', 65, '250', 'p_58_18.048.28.jpg'),
(79, 'Twinmos X3 32GB USB-3.0', '18.048.29', 'Model - Twinmos X3 32GB, Interface - USB-3.0, Capacity (GB) - 32 GB, Access Time Read (MBps) - 54, Access Time Write (MBps) - 40, OS Supported - Windows 2000, XP, Vista, 7 - Mac OS 10.4 or later - Linux Kernel 2.4 or later, Weight (gm) - 11, Warranty (Year) - Product Life Time', '  ', 2, '890', '980', '5', '0', 65, '250', 'p_59_18.048.29.jpg'),
(80, 'HP PAVILION X360 13-U130TU 7th Intel Core i5 7200U', '33.020.763', 'Model - HP PAVILION X360 13-U130TU, Processor - 7th Gen. Intel Core i5 7200U, Processor Clock Speed - 2.50-3.10GHz, CPU Cache - 3MB, Display Size - 13.3\", Display Type - Touch LED Display, RAM - 4GB, RAM Type - DDR4, HDD - 1TB HDD, Graphics Chipset - Intel HD 620, Graphics Memory - Shared, Optical Device - No, Networking - LAN, WiFi, Bluetooth, Card Reader, Webcam, Display Port - HDMI, Audio Port - Combo, USB Port - 2 x USB3.1, 1 x USB2.1, Battery - 3 Cell Li-ion, Backup Time - Up to 4 Hrs., Operating System - Windows 10, Weight - 1.66Kg, Color - Golden, Specialty - Touch, Convertible, Full-size island-style backlit keyboard, Part No - Z4K35PA, Warranty - 2 year (Battery, Adapter 1 year)', ' Color - Golden, Warranty - 2 year (Battery, Adapter 1 year)', 2, '58100', '60600', '5', '0', 18, '49', 'p_60_33.020.763.gif'),
(81, 'HP ENVY 15-AS105TU Intel Core i7 7th Gen. 7500U, Black', '33.020.745', 'Model - HP ENVY 15-AS105TU, Processor - 7th Gen. Intel Core i7 7500U, Processor Clock Speed - 2.70-3.50GHz, CPU Cache - 4MB, Display Size - 15.6\", Display Type - LED Display, RAM - 8GB, RAM Type - DDR4, HDD - 1TB HDD + 128GB SSD, Graphics Chipset - Intel HD 620, Graphics Memory - Shared, Optical Device - No, Networking - LAN, WiFi, Bluetooth, Card reader, HD Webcam, Display Port - HDMI, Audio Port - Combo, USB Port - 3 x USB3.1, 1 x USB3.1 Gen 1 Type-C, Battery - 3 Cell Li-ion, Backup Time - Up to 4 Hrs., Operating System - Windows 10, Weight - 1.94Kg, Color - Black, Part No - Y4G01PA, Warranty - 2 year (Battery, Adapter 1 year)', ' ', 2, '81000', '84500', '5', '0', 18, '50', 'p_61_33.020.745.gif'),
(82, 'HP SPECTRE X2 12-A032TU Intel Core M7-6Y75, Silver', '33.020.720', 'Model - HP SPECTRE X2 12-A032TU, Processor - Intel Core M7-6Y75, Processor Clock Speed - 1.20-3.10GHz, CPU Cache - 4 MB SmartCache, Display Size - 12\", Display Type - WUXGA + IPS WLED Touch Display, RAM - 8GB, RAM Type - DDR3L, HDD - 512GB SSD, Graphics Chipset - Intel HD 515, Graphics Memory - Shared, Optical Device - No, Networking - LAN, WiFi, Bluetooth, Card Reader, VGA Webcam, Display Port - No, Audio Port - Combo, USB Port - 1 x USB3.0 (Type-C), Battery - 3 Cell Li-ion polymer, Backup Time - Up to 4.5 Hrs., Operating System - Windows 10, Weight - 1.19Kg, Color - Black, Others - Touch, Part No - V5D62PA, Warranty - 2 year (Battery, Adapter 1 year)', ' HP SPECTRE X2 12-A032TU Intel Core M7-6Y75 (1.20-3.10GHz, 8GB, 512GB ', 2, '112000', '123000', '5', '0', 18, '48', 'p_62_33.020.720.gif'),
(83, 'Dell INSPIRON 15-5567 7th Gen INTEL Core i5 7200U', '33.013.829', 'Model - Dell INSPIRON 15-5567, Processor - th Gen. INTEL Core i5 7200U, Processor Clock Speed - 2.50GHz, CPU Cache - 3MB, Display Size - 15.6\", Display Type - FHD LED Display, RAM - 4GB, RAM Type - DDR4, HDD - 1TB HDD, Graphics Chipset - AMD RADEON R7 M445, Graphics Memory - 2GB, Optical Device - DVD RW, Networking - LAN, WiFi, Bluetooth, Card Reader, Webcam, Display Port - HDMI, Audio Port - Combo, USB Port - 2 x USB3.0, 1 x USB2.0, Battery - 3 Cell Prismatic, Backup Time - Up to 4 Hrs., Operating System - Windows 10, Weight - 2.30Kg, Color - FOG-GRAY, Warranty - 2 year (Battery, Adapter 1 year)', ' Color - FOG-GRAY, Warranty - 2 year (Battery, Adapter 1 year)', 2, '54000', '56700', '5', '0', 17, '47', 'p_63_33.013.829.gif'),
(84, 'Dell INSPIRON 15-5567 7th Gen INTEL Core i5 7200U, Black', '33.013.831', 'Model - Dell INSPIRON 15-5567, Processor - 7th Gen. INTEL Core i5 7200U, Processor Clock Speed - 2.50GHz, CPU Cache - 3MB, Display Size - 15.6\", Display Type - HD LED Display, RAM - 8GB, RAM Type - DDR4, HDD - 1TB HDD, Graphics Chipset - AMD RADEON R7 M445, Graphics Memory - 2GB, Optical Device - DVD RW, Networking - LAN, WiFi, Bluetooth, Card Reader, Webcam, Display Port - HDMI, Audio Port - Combo, USB Port - 2 x USB3.0, 1 x USB2.0, Battery - 3 Cell Prismatic, Backup Time - Up to 4 Hrs., Operating System - Windows 10, Weight - 2.30Kg, Color - Black, Warranty - 2 year (Battery, Adapter 1 year)', '', 2, '55000', '58500', '5', '0', 17, '9', 'p_64_33.013.831.gif'),
(85, 'DELL XPS 9350 6th Gen. INTEL CORE i5 6200U, Silver', '33.013.722', 'Model - DELL XPS 9350, Processor - 6th Gen. Intel CORE i5 6200U, Processor Clock Speed - 2.30-2.80GHz, CPU Cache - 4MB, Display Size - 13.3\", Display Type - QHD TOUCH LED Display, RAM - 8GB, Display Resolution - 3200 x 1800, RAM Type - DDR3L, HDD - 256 SSD, Graphics Chipset - Intel HD 520, Graphics Memory - Shared, Optical Device - No, Networking - LAN, WiFi, Bluetooth, Card Reader, HD Webcam, Display Port - HDMI, VGA, Audio Port - Combo, USB Port - 2 x USB3.0, Battery - 4 Cell lithium-ion, Backup Time - Up to 4 Hrs., Operating System - WIN 10 64Bit HOME, Weight - 1.30kg, Color - Silver, Warranty - 2 year (battery, adapter 1 yr)', 'Intel CORE i5 6200U, Warranty - 2 year (battery, adapter 1 yr) ', 2, '140000', '145600', '5', '0', 17, '49', 'p_65_33.013.722.jpg'),
(86, 'HP Gaming Edition', 'HP 13103042', '', 'RAM:4GB, HDD:1 TB', 2, '22500', '25000', '5', '500', 18, '12', 'default.jpg'),
(87, 'Redken', '03.055.20', '', 'For Men Grip Tight Holding Gel.', 2, '1212', '1200', '2', '1', 68, '0', 'default.jpg'),
(88, 'Spiker', '03.055.21', '', 'Joice Ice Spiker Styling Glue, Brands', 0, '1300', '1150', '50', '100', 68, '0', 'default.jpg'),
(89, 'SSD1', 'SSD@1234_New', '', '', 2, '2.5', '5.5', '10.5', '5', 63, '0', 'default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbproductsales`
--

CREATE TABLE `tbproductsales` (
  `productSalesId` int(11) NOT NULL,
  `invoiceNumber` int(50) NOT NULL,
  `productId` int(20) NOT NULL,
  `productPriceRate` varchar(50) NOT NULL,
  `productQtys` varchar(50) NOT NULL,
  `profitAmount` varchar(50) NOT NULL,
  `pdescription` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbproductsales`
--

INSERT INTO `tbproductsales` (`productSalesId`, `invoiceNumber`, `productId`, `productPriceRate`, `productQtys`, `profitAmount`, `pdescription`) VALUES
(1, 1, 19, '24700', '1', '2700', ''),
(2, 1, 25, '25000', '1', '100', 'SN: NM22H3G65L65'),
(3, 2, 42, '3202.5', '1', '352.5', ''),
(4, 3, 53, '2205', '1', '205', 'SN: NM22H3365L47'),
(5, 3, 29, '2362.5', '1', '262.5', 'SN: NH3365L47GGH'),
(6, 4, 45, '4672.5', '1', '372.5', 'SN: NM22H3365L47'),
(7, 4, 53, '2205', '1', '205', 'N/A'),
(8, 4, 42, '3202.5', '1', '352.5', 'SN: NMKHP3365L47'),
(9, 4, 49, '10815', '1', '665', 'SN: B6BHGA4HG'),
(10, 4, 37, '9450', '1', '600', 'SN: NJ5N3365L47'),
(11, 4, 32, '12022.5', '1', '1022.5', 'SN: JHBFGDKGV58'),
(12, 4, 69, '945', '1', '95', 'N/A'),
(13, 4, 24, '5000', '1', '400', 'SN: NM2TYR365L47'),
(14, 4, 74, '3885', '1', '585', 'N/A'),
(15, 4, 71, '9450', '2', '850', 'N/A'),
(16, 5, 19, '24700', '1', '2700', ''),
(17, 6, 25, '26100', '1', '1200', ''),
(18, 6, 52, '7612.5', '1', '612.5', 'SN: NM22H3365L47'),
(19, 7, 53, '2180', '1', '180', ''),
(20, 8, 19, '24500', '12', '2700', 'SN: NM22H3365L47'),
(21, 9, 51, '9000', '12', '2300', 'SN: NM22H3365L47'),
(22, 10, 19, '24700', '1', '2700', ''),
(23, 11, 42, '3202.5', '1', '352.5', ''),
(24, 12, 25, '26000', '1', '1200', ''),
(25, 13, 19, '24700', '1', '2700', ''),
(26, 14, 76, '35175', '1', '4675', '1'),
(27, 15, 42, '3500.5', '1', '352.5', ''),
(28, 16, 53, '2205', '1', '205', ''),
(29, 17, 24, '5000', '25', '400', ''),
(30, 18, 53, '2205', '1', '205', ''),
(31, 18, 43, '6405', '1', '455', 'SN: NM22H3365L47'),
(32, 18, 51, '8925', '1', '725', 'SN: NM22H3365L47'),
(33, 18, 45, '4672.5', '1', '372.5', ''),
(34, 18, 55, '840', '1', '140', ''),
(35, 18, 37, '9450', '1', '600', ''),
(36, 18, 32, '12022.5', '2', '1022.5', ''),
(37, 18, 61, '12600', '1', '1100', ''),
(38, 18, 69, '945', '1', '95', ''),
(39, 18, 63, '4830', '1', '430', ''),
(40, 18, 67, '5250', '1', '450', ''),
(41, 18, 59, '5350', '1', '455', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbproductsales_tmp`
--

CREATE TABLE `tbproductsales_tmp` (
  `tmpid` int(11) NOT NULL,
  `tmpSalesNumber` text NOT NULL,
  `productId` int(11) NOT NULL,
  `productPriceRate` varchar(10) NOT NULL,
  `profitAmount` varchar(20) NOT NULL,
  `productQtys` varchar(50) NOT NULL,
  `pdescription` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbproductunit`
--

CREATE TABLE `tbproductunit` (
  `id` int(11) NOT NULL,
  `unitName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbproductunit`
--

INSERT INTO `tbproductunit` (`id`, `unitName`) VALUES
(2, 'Pc\'s'),
(3, 'Pcs'),
(4, 'KG');

-- --------------------------------------------------------

--
-- Table structure for table `tbpuchaseproducts`
--

CREATE TABLE `tbpuchaseproducts` (
  `puchaseproductId` int(11) NOT NULL,
  `puchaseId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `purchaseRate` varchar(50) NOT NULL,
  `purchaseQtys` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbpuchaseproducts`
--

INSERT INTO `tbpuchaseproducts` (`puchaseproductId`, `puchaseId`, `productId`, `purchaseRate`, `purchaseQtys`) VALUES
(4, 1, 45, '4100', '1'),
(5, 2, 41, '5200', '5'),
(6, 3, 45, '4100', '10'),
(7, 3, 19, '35000', '20'),
(8, 3, 60, '6200', '20'),
(9, 3, 76, '39500', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tbpuchaseproducts_tmp`
--

CREATE TABLE `tbpuchaseproducts_tmp` (
  `purTmpId` int(11) NOT NULL,
  `tmpPuchaseId` text NOT NULL,
  `productId` int(11) NOT NULL,
  `purchaseRate` varchar(50) NOT NULL,
  `purchaseQtys` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbpuchaseproducts_tmp`
--

INSERT INTO `tbpuchaseproducts_tmp` (`purTmpId`, `tmpPuchaseId`, `productId`, `purchaseRate`, `purchaseQtys`) VALUES
(1, 'df226a62b248fef37e1498b9d77798f8', 87, '1212', '1'),
(2, '14e3496fc5f6ede9617ef708c399d192', 88, '1300', '1'),
(3, 'bf6eb9beb73dcd2480a8c4673824ba04', 89, '2.5', '35');

-- --------------------------------------------------------

--
-- Table structure for table `tbpurchase`
--

CREATE TABLE `tbpurchase` (
  `id` int(11) NOT NULL,
  `supId` int(11) NOT NULL,
  `purDate` date NOT NULL,
  `purTime` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbpurchase`
--

INSERT INTO `tbpurchase` (`id`, `supId`, `purDate`, `purTime`) VALUES
(1, 2, '2018-01-16', '04:25:59pm'),
(2, 2, '2018-01-16', '04:28:53pm'),
(3, 2, '2018-01-16', '04:34:55pm');

-- --------------------------------------------------------

--
-- Table structure for table `tbpurchasepayment`
--

CREATE TABLE `tbpurchasepayment` (
  `id` int(11) NOT NULL,
  `supId` int(11) NOT NULL,
  `debit` varchar(50) NOT NULL DEFAULT '0',
  `credit` varchar(50) NOT NULL DEFAULT '0',
  `paymentDate` date NOT NULL,
  `paymentTime` varchar(50) NOT NULL,
  `purchaseId` int(11) NOT NULL,
  `pdescription` text NOT NULL,
  `paymentTo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbpurchasepayment`
--

INSERT INTO `tbpurchasepayment` (`id`, `supId`, `debit`, `credit`, `paymentDate`, `paymentTime`, `purchaseId`, `pdescription`, `paymentTo`) VALUES
(14, 2, '3500', '4100', '2018-01-16', '04:25:59pm', 1, '', ''),
(15, 2, '20000', '26000', '2018-01-16', '04:28:53pm', 2, '', ''),
(16, 2, '500000', '983500', '2018-01-16', '04:34:55pm', 3, '', ''),
(17, 2, '50000', '0', '2018-01-16', '04:39:55pm', 0, '', ''),
(18, 2, '100', '0', '2018-01-16', '04:44:59pm', 0, '', ''),
(19, 2, '250', '0', '2018-01-16', '05:10:12pm', 0, 'Due Amount', ''),
(20, 2, '39750', '0', '2018-01-16', '05:16:43pm', 0, 'Due Amount', ''),
(21, 2, '120', '0', '2018-01-17', '09:06:53am', 0, 'When you receive a payment from a customer, especially if the payment is in cash, you may be asked to provide a receipt. A receipt could be just a statement written out by hand or torn out of a receipt book that you purchase from your office supply company. But, you can also create and print your own customized receipt ..', ''),
(22, 2, '15000', '0', '2018-01-17', '10:39:56am', 0, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', ''),
(23, 2, '380000', '0', '2018-01-17', '04:30:01pm', 0, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', ''),
(24, 2, '20', '0', '2018-01-18', '11:16:57am', 0, '', ''),
(25, 2, '4860', '0', '2018-02-11', '10:32:02am', 0, 'Due Amount', ''),
(26, 2, '10.25', '0', '2018-02-19', '10:46:30am', 0, 'sfgrtjyukgwef', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbquotation`
--

CREATE TABLE `tbquotation` (
  `quoid` int(20) NOT NULL,
  `quotationNumber` int(20) NOT NULL,
  `customerName` varchar(200) NOT NULL,
  `customerPhone` varchar(50) NOT NULL,
  `customerAddress` varchar(200) NOT NULL,
  `userId` int(20) NOT NULL,
  `quotationDate` date NOT NULL,
  `quotationTime` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbquotation`
--

INSERT INTO `tbquotation` (`quoid`, `quotationNumber`, `customerName`, `customerPhone`, `customerAddress`, `userId`, `quotationDate`, `quotationTime`) VALUES
(3, 1, 'Feits_Own_Special', '01824168966', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-01-11', '01:16:08pm'),
(4, 2, 'Mahabubur Rahman', '01824168966', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-01-11', '02:06:02pm'),
(5, 3, 'Mahabubur Rahman', '01824168966', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 4, '2018-02-14', '12:33:02pm');

-- --------------------------------------------------------

--
-- Table structure for table `tbquotationproducts`
--

CREATE TABLE `tbquotationproducts` (
  `quotationproductId` int(11) NOT NULL,
  `quotationId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quotationRate` varchar(50) NOT NULL,
  `quotationQtys` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbquotationproducts`
--

INSERT INTO `tbquotationproducts` (`quotationproductId`, `quotationId`, `productId`, `quotationRate`, `quotationQtys`) VALUES
(1, 1, 19, '24700', '1'),
(2, 2, 53, '2205', '1'),
(3, 2, 42, '3202.5', '1'),
(4, 2, 49, '10800', '1'),
(5, 2, 45, '4672.5', '1'),
(6, 2, 55, '840', '1'),
(7, 2, 37, '9450', '1'),
(8, 2, 34, '2520', '1'),
(9, 3, 53, '2205', '1'),
(10, 3, 19, '24700', '1'),
(11, 3, 42, '3202.5', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbquotation_tmp`
--

CREATE TABLE `tbquotation_tmp` (
  `tmpid` int(11) NOT NULL,
  `tmpQuotationNumber` text NOT NULL,
  `productId` int(11) NOT NULL,
  `productPriceRate` varchar(10) NOT NULL,
  `productQtys` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbquotation_tmp`
--

INSERT INTO `tbquotation_tmp` (`tmpid`, `tmpQuotationNumber`, `productId`, `productPriceRate`, `productQtys`) VALUES
(1, '7939a1a27accf7f9e056afdadcc9b1cc', 19, '24700', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbreturnstock`
--

CREATE TABLE `tbreturnstock` (
  `id` int(11) NOT NULL,
  `productId` varchar(20) NOT NULL,
  `productPriceRate` varchar(20) NOT NULL,
  `productQtys` varchar(20) NOT NULL,
  `invoiceNumber` varchar(20) NOT NULL,
  `exId` int(11) NOT NULL,
  `sellable` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbreturnstock`
--

INSERT INTO `tbreturnstock` (`id`, `productId`, `productPriceRate`, `productQtys`, `invoiceNumber`, `exId`, `sellable`) VALUES
(1, '70', '1600', '4', '2', 4, '1'),
(2, '75', '320', '2', '9', 4, '1'),
(3, '71', '9000', '2', '9', 4, '0'),
(4, '73', '16500', '2', '9', 4, '0'),
(5, '42', '3050', '1', '8', 4, '1'),
(6, '54', '3100', '1', '8', 4, '1'),
(7, '52', '7250', '1', '8', 4, '0'),
(8, '45', '4450', '1', '8', 4, '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbsubcategory`
--

CREATE TABLE `tbsubcategory` (
  `scid` int(11) NOT NULL,
  `sub_cat_name` varchar(200) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbsubcategory`
--

INSERT INTO `tbsubcategory` (`scid`, `sub_cat_name`, `category_id`) VALUES
(16, 'Acer', 17),
(17, 'DELL', 17),
(18, 'HP', 17),
(19, 'ASUS', 17),
(20, 'SAMSUNG', 17),
(21, 'Motherboard', 16),
(22, 'Processor', 16),
(23, 'RAM', 16),
(25, 'Monitor', 16),
(26, 'Casing', 16),
(27, 'Power Supply', 16),
(28, 'DVD Writer', 16),
(29, 'UPS ', 18),
(30, 'Bluetooth Speaker', 18),
(31, 'Bluetooth Music Receiver', 18),
(32, 'Headphone ', 18),
(33, 'Media Player', 18),
(34, 'Microphone', 18),
(35, 'Voice Recorder', 18),
(36, 'TV Card', 18),
(37, 'Webcam', 18),
(38, 'MP3/MP4 Player', 18),
(39, 'Audio/Video Cable', 18),
(40, 'Cable & Converter', 18),
(41, 'Anti Virus', 19),
(42, 'MS Office', 19),
(43, 'Windows', 19),
(44, 'Bijoy', 19),
(45, 'DSLR Camera', 20),
(46, 'DSLR Camera (Body Only)', 20),
(47, 'Compact Camera', 20),
(48, 'Video Camera', 20),
(49, 'Camera Accessories', 20),
(50, 'Lens', 20),
(51, 'Camera Bags', 20),
(52, 'Asus', 21),
(53, 'Benq', 21),
(54, 'Casio', 21),
(55, 'Hitachi', 21),
(56, 'Viewsonic', 21),
(57, 'Epson', 21),
(58, 'Toshiba', 21),
(59, 'External HDD', 22),
(61, 'Desktop HDD', 22),
(62, 'Notebook HDD', 22),
(63, 'SSD', 22),
(64, 'Graphics Card', 16),
(65, 'Pendrive', 22),
(66, 'Shampoo', 23),
(67, 'Lotion', 23),
(68, 'Hair Gel', 23),
(69, 'Lipstics', 23),
(70, 'Nail Polish', 23),
(71, 'Facial Cream', 23);

-- --------------------------------------------------------

--
-- Table structure for table `tbsupplier`
--

CREATE TABLE `tbsupplier` (
  `id` int(11) NOT NULL,
  `supName` varchar(200) NOT NULL,
  `supType` text NOT NULL,
  `supAddress` text NOT NULL,
  `supPhone` varchar(50) NOT NULL,
  `supEmail` varchar(200) NOT NULL,
  `supEContact` varchar(50) NOT NULL,
  `addingDate` date NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbsupplier`
--

INSERT INTO `tbsupplier` (`id`, `supName`, `supType`, `supAddress`, `supPhone`, `supEmail`, `supEContact`, `addingDate`, `description`) VALUES
(2, 'Samsujjaman', 'Normal', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230,House #51,', '12345678900', 'sam@gm.com', '01824168996', '2016-08-02', 'NAi'),
(3, 'ABC', 'Normal', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', '12345678900', 'Bandanewaz@gm.com', '01824168996', '2018-02-11', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbusers`
--

CREATE TABLE `tbusers` (
  `userid` int(11) NOT NULL,
  `userFullName` varchar(200) NOT NULL,
  `userTypeId` int(1) NOT NULL,
  `userPhone` varchar(50) NOT NULL,
  `userEmail` varchar(200) NOT NULL,
  `userJoiningDate` varchar(50) NOT NULL,
  `userAddress` text NOT NULL,
  `userName` varchar(200) NOT NULL,
  `userPassword` text NOT NULL,
  `userImage` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbusers`
--

INSERT INTO `tbusers` (`userid`, `userFullName`, `userTypeId`, `userPhone`, `userEmail`, `userJoiningDate`, `userAddress`, `userName`, `userPassword`, `userImage`) VALUES
(1, 'Admin', 1, '01515268864', 'samsujjamanbappy@gmail.com', '', '', 'admin', '21232f297a57a5a743894a0e4a801fc3', ''),
(4, 'User', 2, '0175653366', 'hr@feits.co', '01-01-2018', '  House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user_2_0175653366.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbusertype`
--

CREATE TABLE `tbusertype` (
  `userTypeId` int(11) NOT NULL,
  `userType` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbusertype`
--

INSERT INTO `tbusertype` (`userTypeId`, `userType`) VALUES
(1, 'Admin'),
(2, 'Executive');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbactivity_logs`
--
ALTER TABLE `tbactivity_logs`
  ADD PRIMARY KEY (`acl_id`);

--
-- Indexes for table `tbcategory`
--
ALTER TABLE `tbcategory`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbcustomerpayments`
--
ALTER TABLE `tbcustomerpayments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbdestroyproduct`
--
ALTER TABLE `tbdestroyproduct`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbinvoice`
--
ALTER TABLE `tbinvoice`
  ADD PRIMARY KEY (`inid`);

--
-- Indexes for table `tbmsb_inventory`
--
ALTER TABLE `tbmsb_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbproducts`
--
ALTER TABLE `tbproducts`
  ADD PRIMARY KEY (`pid`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `tbproductsales`
--
ALTER TABLE `tbproductsales`
  ADD PRIMARY KEY (`productSalesId`);

--
-- Indexes for table `tbproductsales_tmp`
--
ALTER TABLE `tbproductsales_tmp`
  ADD PRIMARY KEY (`tmpid`);

--
-- Indexes for table `tbproductunit`
--
ALTER TABLE `tbproductunit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpuchaseproducts`
--
ALTER TABLE `tbpuchaseproducts`
  ADD PRIMARY KEY (`puchaseproductId`);

--
-- Indexes for table `tbpuchaseproducts_tmp`
--
ALTER TABLE `tbpuchaseproducts_tmp`
  ADD PRIMARY KEY (`purTmpId`);

--
-- Indexes for table `tbpurchase`
--
ALTER TABLE `tbpurchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpurchasepayment`
--
ALTER TABLE `tbpurchasepayment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbquotation`
--
ALTER TABLE `tbquotation`
  ADD PRIMARY KEY (`quoid`);

--
-- Indexes for table `tbquotationproducts`
--
ALTER TABLE `tbquotationproducts`
  ADD PRIMARY KEY (`quotationproductId`);

--
-- Indexes for table `tbquotation_tmp`
--
ALTER TABLE `tbquotation_tmp`
  ADD PRIMARY KEY (`tmpid`);

--
-- Indexes for table `tbreturnstock`
--
ALTER TABLE `tbreturnstock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbsubcategory`
--
ALTER TABLE `tbsubcategory`
  ADD PRIMARY KEY (`scid`);

--
-- Indexes for table `tbsupplier`
--
ALTER TABLE `tbsupplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbusers`
--
ALTER TABLE `tbusers`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `userEmail` (`userEmail`);

--
-- Indexes for table `tbusertype`
--
ALTER TABLE `tbusertype`
  ADD PRIMARY KEY (`userTypeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbactivity_logs`
--
ALTER TABLE `tbactivity_logs`
  MODIFY `acl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `tbcategory`
--
ALTER TABLE `tbcategory`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tbcustomerpayments`
--
ALTER TABLE `tbcustomerpayments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbdestroyproduct`
--
ALTER TABLE `tbdestroyproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbinvoice`
--
ALTER TABLE `tbinvoice`
  MODIFY `inid` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbmsb_inventory`
--
ALTER TABLE `tbmsb_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbproducts`
--
ALTER TABLE `tbproducts`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `tbproductsales`
--
ALTER TABLE `tbproductsales`
  MODIFY `productSalesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tbproductsales_tmp`
--
ALTER TABLE `tbproductsales_tmp`
  MODIFY `tmpid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbproductunit`
--
ALTER TABLE `tbproductunit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbpuchaseproducts`
--
ALTER TABLE `tbpuchaseproducts`
  MODIFY `puchaseproductId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbpuchaseproducts_tmp`
--
ALTER TABLE `tbpuchaseproducts_tmp`
  MODIFY `purTmpId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbpurchasepayment`
--
ALTER TABLE `tbpurchasepayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tbquotation`
--
ALTER TABLE `tbquotation`
  MODIFY `quoid` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbquotationproducts`
--
ALTER TABLE `tbquotationproducts`
  MODIFY `quotationproductId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbquotation_tmp`
--
ALTER TABLE `tbquotation_tmp`
  MODIFY `tmpid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbreturnstock`
--
ALTER TABLE `tbreturnstock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbsubcategory`
--
ALTER TABLE `tbsubcategory`
  MODIFY `scid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `tbsupplier`
--
ALTER TABLE `tbsupplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbusers`
--
ALTER TABLE `tbusers`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbusertype`
--
ALTER TABLE `tbusertype`
  MODIFY `userTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
