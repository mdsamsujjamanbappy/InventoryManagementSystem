<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Inventory Management System</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="assets/css/bootstrap-progressbar.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
        <center>
          <?php if(isset($_GET['activation'])&&$_GET['activation']=="expired"){ ?>
            <h3  style="color:red;font-weight:bold;padding-top:300px;"> Activation has been expired. Please contact <a target="_BLANK" title="Far-East IT Solution Ltd." href="https://feits.co"> <strong>Far-East IT Solution Ltd.</strong></a> for renewal.</h3>
            <hr>
            <h4>Contact Number: +880 1763 346334</h4>
          <?php }else{ ?>
            <h3  style="color:red;font-weight:bold;padding-top:300px;"> Error! </h3>
            <?php } ?>
        </center>
    </div>
  </body>
</html>
