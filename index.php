<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Inventory Management System</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="assets/css/bootstrap-progressbar.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">


    <?php 
      require_once("assets/tools/login.php"); 
      $db_handle = new myLoginFunctions();                          

      if(isset($_POST['uname'])&&isset($_POST['upass'])){
           $uname = ($_POST['uname']);
           $upass = ($_POST['upass']);

           $r = $db_handle->checkMSBLoginDetails($uname,$upass);

           if($r){ 
           if($db_handle->b76aab3551fbb04f7cdaa8c7716bb206()){ 
              $results = $db_handle->usersDetailsAtLogin($uname,$upass);
              foreach($results as $user) {
                  $id=htmlentities($user["userid"]); 
                  $userFullName=htmlentities($user["userFullName"]); 
                  $type=htmlentities($user["userTypeId"]);
                  $image=htmlentities($user["userImage"]);
              }

              $_SESSION['ActLeft'] = $db_handle->b76aab3551fbb04f7cdaa8c7716bb207();
              $_SESSION['VmFsaWVk']="Valied";
              
              if($type==1){
                  $_SESSION['adAccess']="Permit";
                  $_SESSION['adId']=$id;
                  $_SESSION['fname']=$userFullName; 
                  $_SESSION['adImage'] = $image;
                  date_default_timezone_set('Asia/Dhaka');
                  $date = date('Y-m-d');
                  $time = date("h:i:sa");
                  $results = $db_handle->insertActivityLog($id,"Login", $date, $time);
                   echo "<script> document.location.href='admin/dashboard.php';</script>";
                  }

              if($type==2){
                  $_SESSION['exAccess']="Permit";
                  $_SESSION['exId']=$id;
                  $_SESSION['fexname']=$userFullName; 
                  $_SESSION['exImage'] = $image;
                  date_default_timezone_set('Asia/Dhaka');
                  $date = date('Y-m-d');
                  $time = date("h:i:sa");
                  $results = $db_handle->insertActivityLog($id,"Login", $date, $time);

                   echo "<script> document.location.href='executive/dashboard.php';</script>";
                  }
             
           }else{ 
             echo "<script> document.location.href='error.php?activation=expired';</script>";
            }
           }else{  echo "<h4 style='color:red;'><center>Invalid Login.</center></h4>";  ?>
             
         <?php }

          }
          ?>

        <div class="animate form login_form">
          <section class="login_content">
            <form action="" method="POST">
              <h1>Login Area</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" name="uname" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="upass" required="" />
              </div>
              <div>
                <center><input type="submit" class="btn btn-default" value="Log in" /></center>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <!-- <a class="signup"  href="#signup" >Lost your password?</a> -->

               <!--  <br />
                <br /> -->
                <br />

                <div>
                    Developed by <a target="_BLANK" title="Far-East IT Solution Ltd." href="https://feits.co"> <strong>Far-East IT Solution Ltd.</strong></a>

                </div>
                <br>
                <div>
                    Version 3.0
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form action="#" method="POST">
              <center>
              <h1>Recover Account</h1>
              <div>
                <input type="text" required class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" required class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="submit" class="btn btn-default" value="Submit" />
              </div>
              </center>
              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  Developed by <a target="_BLANK" title="Far-East IT Solution Ltd." href="https://feits.co"> <strong>Far-East IT Solution Ltd.</strong></a>
                
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
