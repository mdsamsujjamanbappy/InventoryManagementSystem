<?php
	function invoiceCompanyTitle(){
		return "Far-East IT Solutions Limited";
	}

	function invoiceCompanyaddress(){
		return "House #51, Road #18 Sector #11 Uttara, Dhaka-1230";
	}

	function invoiceCompanyPhone(){
		return "Phone: +8801763346334 ";
	}

	function invoiceCompanyEmail(){
		return "Email: info@feits.co";
	}

	function invoiceCompanySloganHeader(){
		return "[Slogan Text will be Appeared here]";
	}

	function footerText1(){
		return "To enjoy the glow of good health, you must exercise.";
	}

	function footerText2(){
		return "Thank you for business with us.";
	}

	function mainLogo(){
		$url = "../fpdf/img/logo1.jpeg";
		return $url;
	}

	function watermarkImages(){
		$url = "../fpdf/img/logo1.jpeg";
		return $url;
	}

	function signature(){
		$txt = "Signature & Date                    ";
		return $txt;
	}


	function dueTextArea(){
		return "[Due Text will be Appeared here]";
	}
	

?>