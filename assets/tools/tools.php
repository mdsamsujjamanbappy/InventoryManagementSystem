<?php
class MyTools{

	function adminMenu(){
	?>
	<ul class="nav side-menu">
	  <li><a href="dashboard.php"><i class="fa fa-home"></i> Dashboard <span class="fa fa-chevron-right"></span></a></li>

	  <li><a><i class="fa fa-windows"></i> Manage Product <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="all_products_list.php">View All Products List</a></li>
	      <li><a href="add_new_product.php">Add New Product </a></li>
	      <li><a href="product_stock.php">Product Stock</a></li>
	      <li><a href="purchase_product.php">Purchase Product</a></li>
	    </ul>
	  </li>
	  <li><a href="users_list.php"><i class="fa fa-users"></i> Manage User <span class="fa fa-chevron-right"></span></a></li>
	  
	  <li><a><i class="fa fa-ship"></i> Manage Supplier  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="supplier_list.php">Supplier List</a></li>
	      <li><a href="supplier_payments.php">Supplier Payments</a></li>
	      <li><a href="supplier_payments_details.php">Supplier Payments Details</a></li>
	    </ul>
	  </li>

	  <li><a><i class="fa fa-list"></i> Sales Report  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="search_invoice.php">Search Invoice</a></li>
	      <li><a href="date_wise_report.php">Date Wise Report</a></li>
	      <li><a href="executive_wise_report.php">Executive Wise Report</a></li>
	      <li><a href="customer_wise_report.php">Customer Wise Report</a></li>
	      <li><a href="product_wise_report.php">Product Wise Report</a></li>
	      <li><a href="due_amount_list.php">All Due Amount List</a></li>
	      <li><a href="search_customer_payment_history.php">Customer Payment History </a></li>
	    </ul>
	  </li>
	  
	 <li><a><i class="fa fa-th"></i> Purchase Report  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="search_voucher.php">Search Purchase No.</a></li>
	      <!-- <li><a href="date_wise_voucher_report">Date Wise Report</a></li> -->
	    </ul>
	  </li>
	  <li><a href="view_return_product_list.php"><i class="fa fa-list"></i> Returned Product List<span class="fa fa-chevron-right"></span></a></li>
	  <li><a><i class="fa fa-cogs" ></i> Settings  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="category_list.php">Manage Category</a></li>
	      <li><a href="subcategory_list.php">Manage Sub-Category</a></li>
	      <li><a href="unit_list.php">Manage Unit Type</a></li>
	    </ul>
	  </li>
	  <li><a href="activity_logs.php"><i class="fa fa-codepen"></i>All Activity Logs<span class="fa fa-chevron-right"></span></a></li> 
	  <li><a href="far-east.php"><i class="fa fa-certificate"></i> Developers Info<span class="fa fa-chevron-right"></span></a></li> 
	</ul>
	<?php 
	}

	function executiveMenu(){
	?>
	<ul class="nav side-menu">
	  <li><a href="dashboard.php"><i class="fa fa-home"></i> Dashboard <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="sales.php"><i class="fa fa-cart-plus"></i> Sale Product<span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="product_list.php"><i class="fa fa-list"></i> All Product List <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="search_invoice.php"><i class="fa fa-search"></i> Search Invoice <span class="fa fa-chevron-right"></span></a></li>
	  <li><a><i class="fa fa-money" ></i> Customer Payments  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="receive_payment.php">Receive Due Amount </a></li>
	      <li><a href="search_customer_payment_history.php">Search Payment History</a></li>
	    </ul>
	  </li>
	  <li><a href="quotation.php"><i class="fa fa-book"></i> Make Price Quotation <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="search_quotation.php"><i class="fa fa-search"></i> Search Price Quotation <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="return_product.php"><i class="fa fa-cart-arrow-down"></i> Return Product <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="view_return_product_list.php"><i class="fa fa-list"></i> Return Product List<span class="fa fa-chevron-right"></span></a></li>
	  
	</ul>
	<?php 
	}

	function developer(){

		echo "<p align='center'> Developed by <b>Far-East IT Solutions Limited</b></p>";
		
	}
	 
	function fullname(){
		echo $_SESSION['fname'];	
	}

	function exfullname(){
		echo $_SESSION['fexname'];	
	}

	function profileLink(){ ?>
	 <ul class="dropdown-menu dropdown-usermenu pull-right">
		 <!-- 
	        <li><a href="myprofile"> Profile</a></li>
	        <li><a href="help">Help</a></li> 
        -->
        <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
      </ul>
	<?php 
	 }
	 
	 function userImage(){
	 	return "images/img.jpg";
	 }

	 function exuserImage(){
	 	return "../user_image/".$_SESSION['exImage'];
	 }

	function invoiceHeader(){

		echo "<h3>Far-East IT Solutions Limited.</h3>";
		echo "<p style='margin-top:-14px;margin-bottom:-14px;'>
			House #51, Road #18 Sector #11 Uttara, Dhaka-1230.<br>
			Phone: +8801763346334<br>
			Email: info@feits.co</p>";
	}

	function invoiceHeaderExport(){

		return "<center>
	<p><span style='font-size:25px;'><b>Far-East IT Solutions Limited.</b></span><br />
	House #51, Road #18 Sector #11 Uttara, Dhaka-1230
	<br />
	Phone: +8801763346334
	<br />
	Email: info@feits.co
	</p> </center>";
	}

	function myFooter(){ ?>
        <footer>
          <div class="pull-left">
            Copyright &copy; <?php echo date('Y');?> | <strong>Demo Shop</strong> 
          </div>
          <div class="pull-right">
            Developed by <a target="_BLANK" title="Far-East IT Solution Ltd." href="https://feits.co"> <strong>Far-East IT Solution Ltd.</strong></a>
          </div>
          <div class="clearfix"></div>
        </footer>
	<?php
	}

	function sidebar_footer(){ ?>
		 
              <a data-toggle="tooltip" data-placement="top" title="">
                <span class="glyphicon" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="System Settings"  href="#">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a> 
              
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="logout.php">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="">
                <span class="glyphicon" aria-hidden="true"></span>
              </a>
	<?php
	}

	 
	function title(){
		echo "Shop Inventory Management System";	
	}

} 
?>