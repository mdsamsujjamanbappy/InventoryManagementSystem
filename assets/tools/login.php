<?php
require_once("controller.php");

class myLoginFunctions extends MSBController{

	// Function to get the client IP address
	function get_client_ip() {
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	function checkMSBLoginDetails($uname,$upass){
		$uname=$this->makeSecure($uname);
		$upass=md5($this->makeSecure($upass));
		$query="SELECT * FROM tbusers WHERE userName='$uname'&& userPassword='$upass'";
  		return $this->getDataWithValue($query);
	}
	
	function b76aab3551fbb04f7cdaa8c7716bb206(){
		date_default_timezone_set('Asia/Dhaka');
        $today = date("Y-m-d");
		$query="SELECT * FROM tbmsb_inventory WHERE truckId='RmVpdHNBY3RL'";
  		$r= $this->getData($query);
  		if(count($r)){
          foreach($r as $dataArr) {
          	$edays = base64_decode($dataArr['edays']);
          }
          $today_time = strtotime($today);
		  $expire_time = strtotime($edays);

          if($expire_time < $today_time){
  			return false;
          }
  		 	
  		}
  		return true;
	}
	
	function b76aab3551fbb04f7cdaa8c7716bb207(){
		date_default_timezone_set('Asia/Dhaka');
        $today = date("Y-m-d");
		$query="SELECT * FROM tbmsb_inventory WHERE truckId='RmVpdHNBY3RL'";
	  	$r= $this->getData($query);
  		if(count($r)){
          foreach($r as $dataArr) {
          	$edays = base64_decode($dataArr['edays']);
          }
          $str = (strtotime($edays) - strtotime($today));
		  $days =  floor($str/3600/24);
          return $days;          
  		 	
  		}
	}

	function usersDetailsAtLogin($uname,$upass){
		$uname=$this->makeSecure($uname);
		$upass=md5($this->makeSecure($upass));
		$query="SELECT * FROM tbusers WHERE userName='$uname'&&userPassword='$upass'";
	  	return $this->getData($query);
	}

	function insertActivityLog($id,$activity, $date, $time){
		$id=$this->makeSecure($id);
		$activity=$this->makeSecure($activity);
		$date=$this->makeSecure($date);
		$time=$this->makeSecure($time);
		$ip=$this->get_client_ip();

		$query="INSERT INTO tbactivity_logs (user_id, activity, adate, time,loginIp) VALUES('$id','$activity','$date','$time','$ip')";
	  	return $this->insertData($query);
	}


}
?>
