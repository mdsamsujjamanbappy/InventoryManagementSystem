<?php
require_once("controller.php");

class myFunctions extends MSBController{
	
	/* All Global Function */

	function getTotalRowNumber($tbname){
		$tbname = $this->makeSecure($tbname);
  		$query ="SELECT * FROM $tbname ";
  		return $this->getRowNumber($query);
	}

	// Function for checkDependency data to delete purpose
	function checkDependency($tbname,$colname,$id){
		$tbname = $this->makeSecure($tbname);
		$colname = $this->makeSecure($colname);
		$id = $this->makeSecure($id);

	  	$query ="SELECT * FROM $tbname WHERE $colname ='$id'";
	  		return $this->getRowNumber($query);
	}

	// Function to get the client IP address
	function get_client_ip() {
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	/* Product Function Area */

	function updateStock($pid,$pquantity){
		$pid = $this->makeSecure($pid);
		$pquantity = $this->makeSecure($pquantity);
  		$query ="UPDATE tbproducts SET quantity = '$pquantity' WHERE pid= '$pid'";
  		return $this->updateData($query);
	}

	/* Get all data form store */
	function getAllProducts(){
  		$query ="SELECT * FROM tbproducts  LEFT JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id  LEFT JOIN tbsubcategory ON tbproducts.subCategoryId=tbsubcategory.scid LEFT JOIN tbcategory ON tbsubcategory.category_id=tbcategory.cid";
  			return $this->getData($query);
	}

	function getProductDetails($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbproducts LEFT JOIN tbsubcategory ON tbproducts.subCategoryId=tbsubcategory.scid LEFT JOIN tbcategory ON tbsubcategory.category_id=tbcategory.cid LEFT JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id   WHERE pid = '$id'";
  		return $this->getData($query);
	}


	function updateProductInfo($id,$pname,$pcode,$subcat_id,$quantity,$description,$sdescription,$originalPrice,$sellingPrice,$vat,$discount){
		
		$id = $this->makeSecure($id);
		$pname = $this->makeSecure($pname);
		$pcode = $this->makeSecure($pcode);
		$subcat_id = $this->makeSecure($subcat_id);
		$quantity = $this->makeSecure($quantity);
		$description = $this->makeSecure($description);
		$sdescription = $this->makeSecure($sdescription);
		$originalPrice = $this->makeSecure($originalPrice);
		$sellingPrice = $this->makeSecure($sellingPrice);
		$vat = $this->makeSecure($vat);
		$discount = $this->makeSecure($discount);

  		$query ="UPDATE tbproducts SET pname='$pname', code='$pcode',subCategoryId='$subcat_id',quantity='$quantity',description='$description',sdescription='$sdescription',originalPrice='$originalPrice',sellingPrice='$sellingPrice',vat='$vat',discount='$discount' WHERE pid = '$id'";
  		return $this->UpdateData($query);
	}

	function insertNewProductInfo($pname,$pcode,$subcat_id,$quantity,$description,$sdescription,$unitid,$originalPrice,$sellingPrice,$vat,$discount,$image){

		$pname = $this->makeSecure($pname);
		$pcode = $this->makeSecure($pcode);
		$subcat_id = $this->makeSecure($subcat_id);
		$quantity = $this->makeSecure($quantity);
		$description = $this->makeSecure($description);
		$sdescription = $this->makeSecure($sdescription);
		$unitid = $this->makeSecure($unitid);
		$originalPrice = $this->makeSecure($originalPrice);
		$sellingPrice = $this->makeSecure($sellingPrice);
		$vat = $this->makeSecure($vat);
		$discount = $this->makeSecure($discount);
		$image = $this->makeSecure($image);

  		$query ="INSERT INTO tbproducts (pname,code,subCategoryId,quantity,description,sdescription,unitid,originalPrice,sellingPrice,vat,discount,image) VALUES ('$pname','$pcode','$subcat_id','$quantity','$description','$sdescription','$unitid','$originalPrice','$sellingPrice','$vat','$discount','$image')";
  		return $this->insertData($query);
	}

	function deleteProduct($pid){
		$pid = $this->makeSecure($pid);
  		$query ="DELETE FROM tbproducts WHERE pid= '$pid'";
  		return $this->deleteData($query);
	}

	/* Category Area*/

	function getCategory(){
  		$query ="SELECT * FROM tbcategory order by cname ASC";
  		return $this->getData($query);
	}

	function saveCategory($name){
		$name = $this->makeSecure($name);
  		$query ="INSERT INTO  tbcategory(cname) VALUES ('$name')";
  		return $this->insertData($query);
	}

	function deleteCategory($cid){
		$cid = $this->makeSecure($cid);
  		$query ="DELETE FROM tbcategory WHERE cid= '$cid'";
  		return $this->deleteData($query);
	}


	/* Sub-category Area */

	function getSubCategoryByCatId($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbsubcategory WHERE category_id='$id'";
  		return $this->getData($query);
	}

	function getSubCategory(){
  		$query ="SELECT tbsubcategory.scid,tbsubcategory.sub_cat_name,tbcategory.cname FROM tbsubcategory LEFT JOIN tbcategory ON tbcategory.cid=tbsubcategory.category_id";
  		return $this->getData($query);
	}

	function saveSubCategory($name,$category_id){
		$name = $this->makeSecure($name);
		$category_id = $this->makeSecure($category_id);
  		$query ="INSERT INTO  tbsubcategory(sub_cat_name,category_id) VALUES ('$name','$category_id')";
  		return $this->insertData($query);
	}

	function deleteSubCategory($cid){
		$cid = $this->makeSecure($cid);
  		$query ="DELETE FROM tbsubcategory WHERE scid= '$cid'";
  		return $this->deleteData($query);
	}

	/* Product Unit */

	function getUnit(){
  		$query ="SELECT * FROM tbproductunit";
  		return $this->getData($query);
	}

	function saveUnit($name){
		$name = $this->makeSecure($name);
  		$query ="INSERT INTO  tbproductunit(unitName) VALUES ('$name')";
  		return $this->insertData($query);
	}

	function deleteUnit($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbproductunit WHERE id= '$id'";
  		return $this->deleteData($query);
	}

	/* Users Area */

	function getUsers(){

  		$query ="SELECT * FROM tbusers LEFT JOIN tbusertype ON tbusers.userTypeId=tbusertype.userTypeId";
  		return $this->getData($query);
	}

	function getUserType(){

  		$query ="SELECT * FROM tbusertype WHERE userTypeId!=1";
  		return $this->getData($query);
	}

	function insertNewUserInfo($userfname,$usertype,$uemail,$uphone,$ujoiningDate,$uaddress,$username,$upassword,$image){
			$upassword = md5($upassword);

		$query ="INSERT INTO tbusers (userFullName,userTypeId,userEmail,userPhone,userJoiningDate,userAddress,userName,userPassword,userImage) VALUES ('$userfname','$usertype','$uemail','$uphone','$ujoiningDate','$uaddress','$username','$upassword','$image')";
  		return $this->insertData($query);
	}

	function getUserDetails($id){
  		$query ="SELECT * FROM tbusers LEFT JOIN tbusertype ON tbusers.userTypeId=tbusertype.userTypeId WHERE userid='$id'";
  		return $this->getData($query);
	}

	function updateUserInfo($id,$userfname,$usertype,$uemail,$uphone,$ujoiningDate,$uaddress,$username,$image){
  		$query ="UPDATE tbusers SET userFullName = '$userfname',userTypeId='$usertype',userEmail='$uemail',userPhone ='$uphone',userJoiningDate='$ujoiningDate',userAddress='$uaddress',userName='$username',userImage='$image' WHERE userid = '$id'";

  		return $this->UpdateData($query);
	}

	function deleteUser($uid){
		$uid = $this->makeSecure($uid);
  		$query ="DELETE FROM tbusers WHERE userid= '$uid'";
  		return $this->deleteData($query);
	}

	function getUsersList(){
  		$query ="SELECT * FROM tbusers WHERE userTypeId=2";
  		return $this->getData($query);
	}

	function resetPassword($userid,$password){
		$userid = $this->makeSecure($userid);
		$password = $this->makeSecure($password);
  		$query ="UPDATE tbusers SET userPassword = '$password' WHERE userid= '$userid'";
  		return $this->deleteData($query);
	}

	/* Sales Report Area */

	function getDateWiseReport($fromdate,$todate){
		$fromdate = $this->makeSecure($fromdate);
		$todate = $this->makeSecure($todate);

  		$query ="SELECT * FROM tbinvoice LEFT JOIN tbusers ON tbinvoice.userId=tbusers.userid  WHERE  invoiceDate BETWEEN '$fromdate' AND '$todate' order by invoice_Number ASC";
  		return $this->getData($query);
	}


	function getExecutiveWiseReport($fromdate,$todate,$exId){
		$fromdate = $this->makeSecure($fromdate);
		$todate = $this->makeSecure($todate);
		$exId = $this->makeSecure($exId);

  		$query ="SELECT * FROM tbinvoice  LEFT JOIN tbusers ON tbinvoice.userId=tbusers.userid  WHERE  tbinvoice.userid='$exId' && invoiceDate BETWEEN '$fromdate' AND '$todate' order by invoice_Number ASC";
  		return $this->getData($query);
	}

	/* Supplier Area */

	function getSupplier(){
  		$query ="SELECT * FROM tbsupplier ";
  		return $this->getData($query);
	}

	function insertNewSupplierInfo($suppname,$sphone,$semail,$saddress,$sephone,$supType,$saddingDate,$sdescription){
		$suppname = $this->makeSecure($suppname);
		$sphone = $this->makeSecure($sphone);
		$semail = $this->makeSecure($semail);
		$saddress = $this->makeSecure($saddress);
		$sephone = $this->makeSecure($sephone);
		$supType = $this->makeSecure($supType);
		$saddingDate = $this->makeSecure($saddingDate);
		$sdescription = $this->makeSecure($sdescription);

		$query ="INSERT INTO tbsupplier (supName,supType, supAddress, supPhone, supEmail, supEContact, addingDate, description) VALUES ('$suppname','$supType','$saddress','$sphone','$semail','$sephone','$saddingDate','$sdescription')";
  		return $this->insertData($query);
	}

	function getSupplierDetails($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbsupplier WHERE id='$id'";
  		return $this->getData($query);
	}

	function updateSupplierInfo($id,$suppname,$sphone,$semail,$saddress,$sephone,$supType,$saddingDate,$sdescription){

		$id = $this->makeSecure($id);
		$suppname = $this->makeSecure($suppname);
		$sphone = $this->makeSecure($sphone);
		$semail = $this->makeSecure($semail);
		$saddress = $this->makeSecure($saddress);
		$sephone = $this->makeSecure($sephone);
		$supType = $this->makeSecure($supType);
		$saddingDate = $this->makeSecure($saddingDate);
		$sdescription = $this->makeSecure($sdescription);

  		$query ="UPDATE tbsupplier SET supName = '$suppname',supPhone='$sphone',supEmail='$semail',supAddress ='$saddress',supEContact ='$sephone',supType='$supType',addingDate='$saddingDate',description='$sdescription' WHERE id = '$id'";

  		return $this->UpdateData($query);
	}

	function deleteSupplier($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbsupplier WHERE id = '$id'";
  		return $this->deleteData($query);
	}

	function getTmpPurchaseProducts($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT tpst.*,tp.pname,tp.sdescription,tpu.unitName FROM tbpuchaseproducts_tmp tpst LEFT JOIN tbproducts tp ON tpst.productId=tp.pid  LEFT JOIN tbproductunit tpu ON tp.unitid=tpu.id  WHERE tpst.tmpPuchaseId='$id' order by purTmpId ASC";
  		return $this->getData($query);
	}

	function getProductListBySubCatId($sid){
		$sid = $this->makeSecure($sid);
  		$query ="SELECT * FROM tbproducts WHERE subCategoryId = '$sid' ORDER BY pname ASC";
  		return $this->getData($query);
	}

	/* Purchase Product Area */

	function insertProductToTmpPuchaseList($tmpPuchaseId,$productId,$purchaseRate,$purchaseQtys){
		$tmpPuchaseId = $this->makeSecure($tmpPuchaseId);
		$productId = $this->makeSecure($productId);
		$purchaseRate = $this->makeSecure($purchaseRate);
		$purchaseQtys = $this->makeSecure($purchaseQtys);
  		$query ="INSERT INTO  tbpuchaseproducts_tmp(tmpPuchaseId, productId, purchaseRate, purchaseQtys) VALUES ('$tmpPuchaseId','$productId','$purchaseRate','$purchaseQtys')";
  		return $this->insertData($query);
	}

	function insertNewPurchaseProductInfo($pid,$pname,$pcode,$subcat_id,$sdescription,$unitid,$originalPrice,$sellingPrice,$vat,$discount){

		$pid = $this->makeSecure($pid);
		$pname = $this->makeSecure($pname);
		$pcode = $this->makeSecure($pcode);
		$subcat_id = $this->makeSecure($subcat_id);
		$sdescription = $this->makeSecure($sdescription);
		$unitid = $this->makeSecure($unitid);
		$originalPrice = $this->makeSecure($originalPrice);
		$sellingPrice = $this->makeSecure($sellingPrice);
		$vat = $this->makeSecure($vat);
		$discount = $this->makeSecure($discount);

  		$query ="INSERT INTO tbproducts (pid,pname,code,subCategoryId,sdescription,unitid,originalPrice,sellingPrice,vat,discount,image) VALUES ('$pid','$pname','$pcode','$subcat_id','$sdescription','$unitid','$originalPrice','$sellingPrice','$vat','$discount','default.jpg')";
  		return $this->insertData($query);
	}

	function deleteProductFromPurchaseList($tmpId){
		$tmpId = $this->makeSecure($tmpId);
  		$query ="DELETE FROM tbpuchaseproducts_tmp WHERE purTmpId= '$tmpId'";
  		return $this->deleteData($query);
	}
	
	function getSupplierPayableAmountById($id){
		$id = $this->makeSecure($id);
		$query ="SELECT * FROM tbpurchasepayment WHERE supId='$id' ";
  		return $this->getData($query);
	}

	function getNewVoucherNumber(){
		$query = "SELECT MAX(id) as maximum FROM tbpurchase";
  		return $this->getData($query);
	}

	function updateIncreaseProductQuantity($pid,$quantity){
		$pid = $this->makeSecure($pid);
		$quantity = $this->makeSecure($quantity);
  		$query ="UPDATE tbproducts SET quantity = quantity + '$quantity' WHERE pid= '$pid'";
  		return $this->deleteData($query);
	}

	function tmpToPurchase($voucherNumber,$productId,$purchaseRate,$purchaseQtys){
		$voucherNumber = $this->makeSecure($voucherNumber);
		$productId = $this->makeSecure($productId);
		$purchaseRate = $this->makeSecure($purchaseRate);
		$purchaseQtys = $this->makeSecure($purchaseQtys);

		$query ="INSERT INTO tbpuchaseproducts(puchaseId,productId,purchaseRate,purchaseQtys) VALUES ('$voucherNumber','$productId','$purchaseRate','$purchaseQtys')";
  		return $this->insertData($query);
	}

	function insertPurchaseInfo($voucherNumber,$supplierId,$purchaseDate,$purchaseTime){
		$voucherNumber = $this->makeSecure($voucherNumber);
		$supplierId = $this->makeSecure($supplierId);
		$purchaseDate = $this->makeSecure($purchaseDate);
		$purchaseTime = $this->makeSecure($purchaseTime);

		$query ="INSERT INTO tbpurchase(id,supId,purDate,purTime) VALUES ('$voucherNumber','$supplierId','$purchaseDate','$purchaseTime')";
  		return $this->insertData($query);
	}

	function insertPaymentInfo($supplierId,$tenderedAmount,$total_amount,$purchaseDate,$purchaseTime,$voucherNumber){
		$supplierId = $this->makeSecure($supplierId);
		$tenderedAmount = $this->makeSecure($tenderedAmount);
		$total_amount = $this->makeSecure($total_amount);
		$purchaseDate = $this->makeSecure($purchaseDate);
		$purchaseTime = $this->makeSecure($purchaseTime);
		$voucherNumber = $this->makeSecure($voucherNumber);

		$query ="INSERT INTO tbpurchasepayment(supId, debit, credit, paymentDate, paymentTime, purchaseId) VALUES ('$supplierId','$tenderedAmount','$total_amount','$purchaseDate','$purchaseTime','$voucherNumber')";
  		return $this->insertData($query);
	}

	/* Supplier Payments Area */

	function getNewSupplierPaymentNumber(){
		$query = "SELECT MAX(id) as maximum FROM tbpurchasepayment";
  		return $this->getData($query);
	}

	function getSupplierAllPayableAmountById($id){
		$id = $this->makeSecure($id);
		$query ="SELECT * FROM tbpurchasepayment tpp INNER JOIN tbsupplier ts ON tpp.supId=ts.id WHERE tpp.supId='$id' ";
  		return $this->getData($query);
	}

	function saveSupplierPayments($SupplierPaymentsId,$supplierId,$tenderedAmount,$paymentDate,$paymentTime,$description){
	    $SupplierPaymentsId = $this->makeSecure($SupplierPaymentsId);
	    $supplierId = $this->makeSecure($supplierId);
		$tenderedAmount = $this->makeSecure($tenderedAmount);
		$paymentDate = $this->makeSecure($paymentDate);
		$paymentTime = $this->makeSecure($paymentTime);
		$description = $this->makeSecure($description);

		$query ="INSERT INTO tbpurchasepayment(id,supId, debit, paymentDate, paymentTime,pdescription) VALUES ('$SupplierPaymentsId','$supplierId','$tenderedAmount','$paymentDate','$paymentTime','$description')";
  		return $this->insertData($query);
	}

	/* Customer wise Report*/

	function searchCustomer($search){
	    $search = $this->makeSecure($search);
  		$query ="SELECT distinct customerName,customerPhone FROM tbinvoice WHERE customerName like '%$search%' OR customerPhone like '%$search%' ";
  		return $this->getData($query);
	}

	function searchCustomerShoppingDate($search){
	    $search = $this->makeSecure($search);
  		$query ="SELECT distinct invoiceDate FROM tbinvoice WHERE customerPhone = '$search' ";
  		return $this->getData($query);
	}

	function getCustomerInvoiceByDate($date,$cphone){
	    $date = $this->makeSecure($date);
	    $cphone = $this->makeSecure($cphone);
  		$query ="SELECT * FROM tbinvoice WHERE customerPhone='$cphone' &&  invoiceDate = '$date' order by invoice_Number ASC ";
  		return $this->getData($query);
	}

	/* */

	function getProductInventory($search,$num_rows,$condition){
	    $search = $this->makeSecure($search);
	    $num_rows = $this->makeSecure($num_rows);
	    $condition = $this->makeSecure($condition);

  		$query ="SELECT * FROM tbproducts  INNER JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id  INNER JOIN tbsubcategory ON tbproducts.subCategoryId=tbsubcategory.scid INNER JOIN tbcategory ON tbsubcategory.category_id=tbcategory.cid WHERE $condition && (tbproducts.pname  like '%$search%' OR tbproducts.code  like '%$search%' OR tbsubcategory.sub_cat_name like '%$search%' OR tbcategory.cname like '%$search%' ) limit 0,$num_rows";
  		return $this->getData($query);
	}


	function getProductId(){
		$query = "SELECT MAX(pid) as maximum FROM tbproducts";
  		return $this->getData($query);
	}





	function getNotifications($search,$num_rows){

  		$query ="SELECT * FROM tbproducts  INNER JOIN tbsubcategory ON tbproducts.subCategoryId=tbsubcategory.scid INNER JOIN tbcategory ON tbsubcategory.category_id=tbcategory.cid WHERE quantity <5 AND (tbproducts.pname  like '%$search%' OR tbproducts.code  like '%$search%' OR tbsubcategory.sub_cat_name like '%$search%' OR tbcategory.cname like '%$search%') order by quantity ASC limit 0,$num_rows";
  		return $this->getData($query);
	}


	function getNotificationsNum(){

  		$query ="SELECT * FROM tbproducts WHERE quantity <5 ";
  		return $this->getRowNumber($query);
	}

	function addProductQuantity($id,$quantity){
  		$query ="UPDATE tbproducts SET quantity = quantity+'$quantity' WHERE pid = '$id'";

  		return $this->UpdateData($query);
	}

	function getProductSalesListByInvoiceNumber($invoiceNumber){
	  		$query ="SELECT * FROM tbproductsales  WHERE invoiceNumber = '$invoiceNumber'";
	  		return $this->getData($query);
	}

	function getExecutivesWiseReport($userid,$fromdate,$todate){
  		$query ="SELECT * FROM tbinvoice WHERE userId='$userid' &&  invoiceDate BETWEEN '$fromdate' AND '$todate' order by invoice_Number ASC ";
  		return $this->getData($query);
	}

	function getSalesProducts($id){
  		$query ="SELECT * FROM tbproductsales  INNER JOIN tbproducts ON tbproductsales.productId=tbproducts.pid INNER JOIN tbinvoice ON tbinvoice.invoice_Number=tbproductsales.invoiceNumber  INNER JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id  WHERE invoiceNumber='$id' order by productSalesId ASC";
  		return $this->getData($query);
	}

	function getInvoiceDetails($id){
  		$query ="SELECT * FROM tbinvoice WHERE invoice_Number='$id'";
  		return $this->getData($query);
	}

	function getProductsIdByCode($pcode){
  		$query ="SELECT * FROM tbproducts WHERE code='$pcode'";
  		return $this->getData($query);
	}


	function getProductsSaleDate(){
  		$query ="SELECT distinct invoiceDate FROM tbinvoice ";
  		return $this->getData($query);
	}

	function getProductsSaleHistory($date){
	    $date = $this->makeSecure($date);
  		$query ="SELECT distinct tbproductsales.productId,tbinvoice.invoiceDate FROM tbinvoice  INNER JOIN tbproductsales ON tbinvoice.invoice_Number=tbproductsales.invoiceNumber WHERE tbinvoice.invoiceDate = '$date' order by tbproductsales.productId ASC";
  		return $this->getData($query);
  	}

	
	function getProductsSaleHistoryTotalQuantity($pid){
	    $pid = $this->makeSecure($pid);
  		$query ="SELECT tbproductunit.unitName, tbproducts.pname, tbproductsales.productQtys FROM tbproductsales  INNER JOIN tbproducts ON tbproductsales.productId=tbproducts.pid   INNER JOIN tbproductunit ON tbproductunit.id=tbproducts.unitid WHERE productId='$pid'";
  		return $this->getData($query);
  	}

	function getActivityLogs($ex_id,$activity,$rows){

  		$query ="SELECT * FROM tbactivity_logs  INNER JOIN tbusers ON tbusers.userid=tbactivity_logs.user_id  WHERE user_id='$ex_id' AND activity like '%$activity%' order by acl_id desc limit 0,$rows ";
  		return $this->getData($query);
	}

	/* Purchase Report */

	function getVoucherDetails($id){
	    $id = $this->makeSecure($id);
  		$query ="SELECT tp.id purchaseId, tp.purTime purchaseTime, tp.purDate purchaseDate,ts.* FROM tbpurchase tp LEFT JOIN tbsupplier ts ON ts.id=tp.supId WHERE tp.id='$id'";
  		return $this->getData($query);
	}

	function getVoucherPaymentDetails($id){
	    $id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbpurchasepayment WHERE purchaseId='$id'";
  		return $this->getData($query);
	}

	function getPurchaseProducts($id){
	    $id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbpuchaseproducts INNER JOIN tbproducts ON tbpuchaseproducts.productId=tbproducts.pid INNER JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id WHERE tbpuchaseproducts.puchaseId='$id' order by tbpuchaseproducts.puchaseproductId ASC";
  		return $this->getData($query);
	}


	function getPaymentDetails($id){
	    $id = $this->makeSecure($id);
  		$query ="SELECT tpp.*, ts.supName supName, ts.supPhone supPhone, ts.supAddress supAddress FROM tbpurchasepayment tpp LEFT JOIN tbsupplier ts ON ts.id=tpp.supId WHERE tpp.id='$id'";
  		return $this->getData($query);
	}

	function getProductsSaleDetails($id){
	    $id = $this->makeSecure($id);
  		$query ="SELECT tps.*, tp.*,tiv.invoiceDate invoiceDate,tpu.unitName unitName FROM tbproductsales tps LEFT JOIN tbproducts tp ON tps.productId=tp.pid LEFT JOIN tbinvoice tiv ON tiv.invoice_Number=tps.invoiceNumber LEFT JOIN tbproductunit tpu ON tp.unitid=tpu.id WHERE tps.productId='$id'";
  		return $this->getData($query);
	}

	function getReturnProductList(){
  		$query ="SELECT trs.*,tp.pname pname,tp.code pcode,tpu.unitName unitName,tu.userFullName userFullName FROM tbreturnstock trs LEFT JOIN tbproducts tp ON tp.pid=trs.productId LEFT JOIN tbproductunit tpu ON tp.unitid=tpu.id  LEFT JOIN tbusers tu ON trs.exId=tu.userid";
  		return $this->getData($query);
	}

	function getDistinctCustomerPhone(){
  		$query ="SELECT DISTINCT customerPhone FROM  tbinvoice ORDER BY inid ASC";
  		return $this->getData($query);
	}

	function getCustomerPaymentByPhone($customerPhone){
	    $customerPhone = $this->makeSecure($customerPhone);
  		$query ="SELECT tbcp.*,tu.userFullName FROM tbcustomerpayments tbcp LEFT JOIN tbusers tu ON tu.userid=tbcp.userId WHERE tbcp.customerPhone='$customerPhone'";
  		return $this->getData($query);
	}
	
	function getCustomerDetailsByPhone($customerPhone){
	    $customerPhone = $this->makeSecure($customerPhone);
  		$query ="SELECT * FROM tbinvoice WHERE customerPhone='$customerPhone' LIMIT 0,1";
  		return $this->getData($query);
	}

	function getProductsSaleAmountHistory($date){
	    $date = $this->makeSecure($date);
  		$query ="SELECT * FROM tbcustomerpayments WHERE pDate='$date'";
  		return $this->getData($query);
	}
	
	function getInvoiceList(){
  		$query ="SELECT * FROM tbinvoice";
  		return $this->getData($query);
	}
	
	function getActivityLogsList(){
  		$query ="SELECT tbal.*,tu.userFullName,tu.userPhone FROM tbactivity_logs tbal LEFT JOIN tbusers tu ON tu.userid=tbal.user_id ORDER BY tbal.acl_id DESC";
  		return $this->getData($query);
	}
	
	function insertLogoutActivityLog($id,$activity, $date, $time){
		$id=$this->makeSecure($id);
		$activity=$this->makeSecure($activity);
		$date=$this->makeSecure($date);
		$time=$this->makeSecure($time);
		$ip=$this->get_client_ip();

		$query="INSERT INTO tbactivity_logs (user_id, activity, adate, time,loginIp) VALUES('$id','$activity','$date','$time','$ip')";
	  	return $this->insertData($query);
	}
}
?>
