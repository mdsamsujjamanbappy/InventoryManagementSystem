<?php
require_once("controller.php");

class myFunctions extends MSBController{

	// Function to get the client IP address
	function get_client_ip() {
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	/* All Global Function */

	function getTotalRowNumber($tbname){
		$tbname = $this->makeSecure($tbname);
  		$query ="SELECT * FROM $tbname ";
  		return $this->getRowNumber($query);
	}

	function checkDependency($tbname,$colname,$id){
		$tbname = $this->makeSecure($tbname);
		$colname = $this->makeSecure($colname);
		$id = $this->makeSecure($id);

	  	$query ="SELECT * FROM $tbname WHERE $colname ='$id'";
	  		return $this->getRowNumber($query);
	}

	/* Product Function Area */

	function getProductDetails($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbproducts INNER JOIN tbsubcategory ON tbproducts.subCategoryId=tbsubcategory.scid INNER JOIN tbcategory ON tbsubcategory.category_id=tbcategory.cid INNER JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id   WHERE pid = '$id'";
  		return $this->getData($query);
	}
	

	/* Category Area*/

	function getCategory(){
  		$query ="SELECT * FROM tbcategory order by cname ASC";
  		return $this->getData($query);
	}

	
	/* Sub-category Area */

	function getSubCategoryByCatId($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbsubcategory WHERE category_id='$id' ORDER BY 	sub_cat_name ASC";
  		return $this->getData($query);
	}	

	/* Product Area */

	function getAllProducts(){
  		$query ="SELECT * FROM tbproducts  LEFT JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id  LEFT JOIN tbsubcategory ON tbproducts.subCategoryId=tbsubcategory.scid LEFT JOIN tbcategory ON tbsubcategory.category_id=tbcategory.cid";
  			return $this->getData($query);
	}

	function getProductDetailsId($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbproducts  LEFT JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id WHERE pid ='$id'";
  		return $this->getData($query);
	}

	function getProductListBySubCatId($sid){
		$sid = $this->makeSecure($sid);
  		$query ="SELECT * FROM tbproducts WHERE subCategoryId = '$sid' ORDER BY pname ASC";
  		return $this->getData($query);
	}

	/* Sales product */

	function insertProductToChart($tmpSalesNumber,$productId,$price,$profit,$quantity,$description){
		$tmpSalesNumber = $this->makeSecure($tmpSalesNumber);
		$productId = $this->makeSecure($productId);
		$price = $this->makeSecure($price);
		$profit = $this->makeSecure($profit);
		$quantity = $this->makeSecure($quantity);
		$description = $this->makeSecure($description);

		$query ="INSERT INTO tbproductsales_tmp (tmpSalesNumber,productId,productPriceRate,profitAmount,productQtys,pdescription) VALUES ('$tmpSalesNumber','$productId','$price','$profit','$quantity','$description')";
  		return $this->insertData($query);
	}

	function insertPaymentInfo($invoiceNumber,$customerPhone,$userId,$invoiceDate,$invoiceTime,$tenderedAmount,$payableAmount){
		$invoiceNumber = $this->makeSecure($invoiceNumber);
		$customerPhone = $this->makeSecure($customerPhone);
		$userId = $this->makeSecure($userId);
		$invoiceDate = $this->makeSecure($invoiceDate);
		$invoiceTime = $this->makeSecure($invoiceTime);
		$tenderedAmount = $this->makeSecure($tenderedAmount);
		$payableAmount = $this->makeSecure($payableAmount);

		$query ="INSERT INTO tbcustomerpayments (customerPhone, dueAmount, paidAmount, pDate, pTime, userId, invoiceNumber) VALUES ('$customerPhone','$payableAmount','$tenderedAmount','$invoiceDate','$invoiceTime','$userId','$invoiceNumber')";
  		return $this->insertData($query);
	}

	function insertReceivePaymentInfo($cpi,$customerPhone,$userId,$pDate,$pTime,$paymentAmount,$description){
		$cpi = $this->makeSecure($cpi);
		$customerPhone = $this->makeSecure($customerPhone);
		$userId = $this->makeSecure($userId);
		$pDate = $this->makeSecure($pDate);
		$pTime = $this->makeSecure($pTime);
		$paymentAmount = $this->makeSecure($paymentAmount);
		$description = $this->makeSecure($description);

		$query ="INSERT INTO tbcustomerpayments (id,customerPhone, dueAmount, paidAmount, pDate, pTime, userId, payDescription) VALUES ('$cpi','$customerPhone','0','$paymentAmount','$pDate','$pTime','$userId','$description')";
  		return $this->insertData($query);
	}

	function getTmpProducts($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT tpst.*,tp.pname,tp.sdescription,tpu.unitName FROM tbproductsales_tmp tpst LEFT JOIN tbproducts tp ON tpst.productId=tp.pid  LEFT JOIN tbproductunit tpu ON tp.unitid=tpu.id  WHERE tpst.tmpSalesNumber='$id' order by tmpid ASC";
  		return $this->getData($query);
	}
	
	function getProductDetailsbyCode($code){
		$code = $this->makeSecure($code);
  		$query ="SELECT * FROM tbproducts  LEFT JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id  LEFT JOIN tbsubcategory ON tbproducts.subCategoryId=tbsubcategory.scid LEFT JOIN tbcategory ON tbsubcategory.category_id=tbcategory.cid WHERE code = '$code'";
  		return $this->getData($query);
	}

	function getNewInvoiceNumber(){
		$query = "SELECT MAX(invoice_Number) as maximum FROM tbinvoice";
  		return $this->getData($query);
	}

	function getNewCustomerPaymentId(){
		$query = "SELECT MAX(id) as maximum FROM tbcustomerpayments";
  		return $this->getData($query);
	}

	function getTmpAllProducts($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbproductsales_tmp LEFT JOIN tbproducts ON tbproductsales_tmp.productId=tbproducts.pid   LEFT JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id  WHERE tmpSalesNumber='$id' order by tmpid ASC";
  		return $this->getData($query);
	}

	function tmpToSales($invoice_id,$productId,$productPriceRate,$quantity,$profitAmount,$pdescription){
		$invoice_id = $this->makeSecure($invoice_id);
		$productId = $this->makeSecure($productId);
		$productPriceRate = $this->makeSecure($productPriceRate);
		$quantity = $this->makeSecure($quantity);
		$profitAmount = $this->makeSecure($profitAmount);
		$pdescription = $this->makeSecure($pdescription);

		$query ="INSERT INTO tbproductsales(invoiceNumber,productId,productPriceRate,productQtys,profitAmount,pdescription) VALUES ('$invoice_id','$productId','$productPriceRate','$quantity','$profitAmount','$pdescription')";
  		return $this->insertData($query);
	}

	function deleteProductFromChart($tmpId){
		$tmpId = $this->makeSecure($tmpId);
  		$query ="DELETE FROM tbproductsales_tmp WHERE tmpid= '$tmpId'";
  		return $this->deleteData($query);
	}

	function insertInvoiceInfo($invoiceNumber,$customerName,$customerPhone,$customerAddress,$userId,$invoiceDate,$invoiceTime,$paymentMethodId,$tenderedAmount,$specialDiscount,$vat){

		$invoiceNumber = $this->makeSecure($invoiceNumber);
		$customerName = $this->makeSecure($customerName);
		$customerPhone = $this->makeSecure($customerPhone);
		$customerAddress = $this->makeSecure($customerAddress);
		$invoiceDate = $this->makeSecure($invoiceDate);
		$userId = $this->makeSecure($userId);
		$invoiceTime = $this->makeSecure($invoiceTime);
		$paymentMethodId = $this->makeSecure($paymentMethodId);
		$tenderedAmount = $this->makeSecure($tenderedAmount);
		$specialDiscount = $this->makeSecure($specialDiscount);
		$vat = $this->makeSecure($vat);

		$query ="INSERT INTO tbinvoice(invoice_Number,customerName,customerPhone,customerAddress,userId,invoiceDate,invoiceTime,paymentMethodId,tenderedAmount,specialDiscount,tvat) VALUES ('$invoiceNumber','$customerName','$customerPhone','$customerAddress','$userId','$invoiceDate','$invoiceTime','$paymentMethodId','$tenderedAmount','$specialDiscount','$vat')";
  		return $this->insertData($query);
	}

	function updateDecreaseProductQuantity($pid,$pquantity){
		$pid = $this->makeSecure($pid);
		$pquantity = $this->makeSecure($pquantity);
  		$query ="UPDATE tbproducts SET quantity = quantity - '$pquantity' WHERE pid= '$pid'";
  		return $this->updateData($query);
	}

	function updateStock($pid,$pquantity){
		$pid = $this->makeSecure($pid);
		$pquantity = $this->makeSecure($pquantity);
  		$query ="UPDATE tbproducts SET quantity = '$pquantity' WHERE pid= '$pid'";
  		return $this->updateData($query);
	}

	function getInvoiceDetails($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbinvoice WHERE invoice_Number='$id'";
  		return $this->getData($query);
	}
	
	function getSalesProducts($id){
  		$query ="SELECT * FROM tbproductsales  INNER JOIN tbproducts ON tbproductsales.productId=tbproducts.pid INNER JOIN tbinvoice ON tbinvoice.invoice_Number=tbproductsales.invoiceNumber  INNER JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id  WHERE invoiceNumber='$id' order by productSalesId ASC";
  		return $this->getData($query);
	}

	
	function getCustomerPDetails($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT tcp.* FROM  tbcustomerpayments tcp WHERE tcp.customerPhone='$id'";
  		return $this->getData($query);
	}
	
	function getCheckCustomerPDetails($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbinvoice WHERE customerPhone='$id' LIMIT 0,1";
  		return $this->getData($query);
	}
	


	/* Quotation Area */


	function getQuotationTmpProducts($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT tqt.*,tp.pid pid,tp.pname,tp.sdescription,tpu.unitName FROM tbquotation_tmp tqt LEFT JOIN tbproducts tp ON tqt.productId=tp.pid  LEFT JOIN tbproductunit tpu ON tp.unitid=tpu.id  WHERE tqt.tmpQuotationNumber='$id' order by tmpid ASC";
  		return $this->getData($query);
	}
	

	function insertProductToQuotation($tmpQuotationNumber,$productId,$price,$quantity){
		$tmpQuotationNumber = $this->makeSecure($tmpQuotationNumber);
		$productId = $this->makeSecure($productId);
		$price = $this->makeSecure($price);
		$quantity = $this->makeSecure($quantity);

		$query ="INSERT INTO tbquotation_tmp (tmpQuotationNumber,productId,productPriceRate,productQtys) VALUES ('$tmpQuotationNumber','$productId','$price','$quantity')";
  		return $this->insertData($query);
	}

	function deleteProductFromQuotation($tmpId){
		$tmpId = $this->makeSecure($tmpId);
  		$query ="DELETE FROM tbquotation_tmp WHERE tmpid= '$tmpId'";
  		return $this->deleteData($query);
	}

	function getNewQuotationNumber(){
		$query = "SELECT MAX(quotationNumber) as maximum FROM tbquotation";
  		return $this->getData($query);
	}

	function getTmpQuotationAllProducts($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbquotationproducts LEFT JOIN tbproducts ON tbquotationproducts.productId=tbproducts.pid   LEFT JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id  WHERE tbquotationproducts.tmpQuotationNumber='$id' order by tmpid ASC";
  		return $this->getData($query);
	}

	function tmpToQuotation($quotationNumber,$pid,$productPriceRate,$quantity){
		$quotationNumber = $this->makeSecure($quotationNumber);
		$productId = $this->makeSecure($pid);
		$productPriceRate = $this->makeSecure($productPriceRate);
		$quantity = $this->makeSecure($quantity);

		$query ="INSERT INTO tbquotationproducts(quotationId,productId,quotationRate,quotationQtys) VALUES ('$quotationNumber','$productId','$productPriceRate','$quantity')";
  		return $this->insertData($query);
	}

	function insertQuotationInfo($quotationNumber,$customerName,$customerPhone,$customerAddress,$userId,$quotationDate,$quotationTime){

		$quotationNumber = $this->makeSecure($quotationNumber);
		$customerName = $this->makeSecure($customerName);
		$customerPhone = $this->makeSecure($customerPhone);
		$customerAddress = $this->makeSecure($customerAddress);
		$userId = $this->makeSecure($userId);
		$quotationDate = $this->makeSecure($quotationDate);
		$quotationTime = $this->makeSecure($quotationTime);

		$query ="INSERT INTO tbquotation(quotationNumber, customerName, customerPhone, customerAddress, userId, quotationDate, quotationTime) VALUES ('$quotationNumber','$customerName','$customerPhone','$customerAddress','$userId','$quotationDate','$quotationTime')";
  		return $this->insertData($query);
	}

	function getQuotationDetails($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbquotation WHERE quotationNumber='$id'";
  		return $this->getData($query);
	}

	function getQuotationProducts($id){
  		$query ="SELECT * FROM tbquotationproducts  INNER JOIN tbproducts ON tbquotationproducts.productId=tbproducts.pid INNER JOIN tbquotation ON tbquotation.quotationNumber=tbquotationproducts.quotationId  INNER JOIN tbproductunit ON tbproducts.unitid=tbproductunit.id  WHERE tbquotationproducts.quotationId='$id' order by quotationproductId ASC";
  		return $this->getData($query);
	}

	function getSaleProductDetails($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT tpst.*,tp.* FROM tbproductsales_tmp tpst LEFT JOIN tbproducts tp ON tpst.productId=tp.pid WHERE tpst.tmpid = '$id'";
  		return $this->getData($query);
	}


	function updatePrice($id,$productPriceRate){
		$id = $this->makeSecure($id);
		$productPriceRate = $this->makeSecure($productPriceRate);
  		$query ="UPDATE tbproductsales_tmp SET productPriceRate = '$productPriceRate' WHERE tmpid= '$id'";
  		return $this->updateData($query);
	}

	function getSaleProductBySaleId($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbproductsales WHERE productSalesId='$id'";
  		return $this->getData($query);
	}
	

	function updateIncreaseProductQuantity($pid,$pquantity){
		$pid = $this->makeSecure($pid);
		$pquantity = $this->makeSecure($pquantity);
  		$query ="UPDATE tbproducts SET quantity = quantity + '$pquantity' WHERE pid= '$pid'";
  		return $this->deleteData($query);
	}

	function insertToReturnStock($productId,$productQtys,$invoiceNumber,$productPriceRate,$exId,$sellable){
		$productId = $this->makeSecure($productId);
		$productQtys = $this->makeSecure($productQtys);
		$invoiceNumber = $this->makeSecure($invoiceNumber);
		$productPriceRate = $this->makeSecure($productPriceRate);
		$sellable = $this->makeSecure($sellable);
		$exId = $this->makeSecure($exId);

		$query ="INSERT INTO tbreturnstock (productId, productPriceRate, productQtys, invoiceNumber,exId, sellable) VALUES ('$productId','$productPriceRate','$productQtys','$invoiceNumber','$exId','$sellable')";
  		return $this->insertData($query);
	}

	function deleteFromSale($id,$invoiceNumber){
		$id = $this->makeSecure($id);
		$invoiceNumber = $this->makeSecure($invoiceNumber);
  		$query ="DELETE FROM tbproductsales WHERE productSalesId= '$id'&&invoiceNumber='$invoiceNumber'";
  		return $this->deleteData($query);
	}

	function getReturnProductList(){
  		$query ="SELECT trs.*,tp.pname pname,tp.code pcode,tpu.unitName unitName,tu.userFullName userFullName FROM tbreturnstock trs LEFT JOIN tbproducts tp ON tp.pid=trs.productId LEFT JOIN tbproductunit tpu ON tp.unitid=tpu.id  LEFT JOIN tbusers tu ON trs.exId=tu.userid";
  		return $this->getData($query);
	}

	function getCustomerPaymentByPhone($customerPhone){
	    $customerPhone = $this->makeSecure($customerPhone);
  		$query ="SELECT * FROM  tbcustomerpayments WHERE customerPhone='$customerPhone'";
  		return $this->getData($query);
	}
	
	function insertLogoutActivityLog($id,$activity, $date, $time){
		$id=$this->makeSecure($id);
		$activity=$this->makeSecure($activity);
		$date=$this->makeSecure($date);
		$time=$this->makeSecure($time);
		$ip=$this->get_client_ip();

		$query="INSERT INTO tbactivity_logs (user_id, activity, adate, time,loginIp) VALUES('$id','$activity','$date','$time','$ip')";
	  	return $this->insertData($query);
	}
}
?>
