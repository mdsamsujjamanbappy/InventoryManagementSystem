<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Invoice</title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Executive Panel</span></a>
            </div>

            <div class="clearfix"></div>
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->executiveMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->exuserImage();?>" alt=""><?php $my_tools->exfullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Invoice</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12">
                 <?php
                 if(!empty($_POST['_MSBtoken'])){
                      $invoiceNumber= $_SESSION['invoiceNumber'];
                      $tmpId  = $_SESSION['tmpMSalesNumber'];
                      date_default_timezone_set('Asia/Dhaka');
                      $invoiceDate = date("Y-m-d");
                      $invoiceTime = date("h:i:sa");
                      $customerPhone = $_SESSION['cPhone '];
                      $tenderedAmount = $_POST['tendered_amount'];
                      $payableAmount = $_POST['payableAmount'];
                      $userId =$_SESSION['exId'];

                      $results = $db_handle->getTmpAllProducts($tmpId);
                       foreach($results as $product) {
                          $tmpid = $product["tmpid"];
                          $pid = $product["pid"];
                          $productPriceRate = $product["productPriceRate"];
                          $profitAmount = $product["profitAmount"];
                          $quantity = $product["productQtys"];
                          $pdescription = $product["pdescription"];
                          
                          $db_handle->tmpToSales($invoiceNumber,$pid,$productPriceRate,$quantity,$profitAmount,$pdescription);

                          $db_handle->updateDecreaseProductQuantity($pid,$quantity);

                          $db_handle->deleteProductFromChart($tmpid);
                       }
                       if($results){
                          $db_handle->insertPaymentInfo($invoiceNumber,$customerPhone,$userId,$invoiceDate,$invoiceTime,$tenderedAmount,$payableAmount);
                        }
                       
                          unset($_SESSION['tmpMSalesNumber']);
                          echo "<script> document.location.href='save_final_invoice.php?iid=".md5($invoiceNumber)."&&invoiceNumber=".base64_encode($invoiceNumber)."';</script>";

                        }

                        if(!empty($_GET['invoiceNumber'])){
                          echo "<h1 align='center' autofocus style='color:green;'>Invoice Successfully Saved</h1>";
                          echo "<center><br /><a target='_BLANK' href='save_invoice_as_pdf.php?id=".md5($_GET['invoiceNumber'])."&&iid=".($_GET['invoiceNumber'])."' class='btn btn-primary'><i class='fa fa-file-pdf-o'></i> Save as PDF</a>";
                          echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                          echo "<a href='sales.php' class='btn btn-primary'><i class='fa fa-undo'></i>  Goto Sales Page</a></center>";
                
                        }          
                    ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
  </body>
</html>