<?php
require_once 'header_link.php';
if(!empty($_POST["product_id"])) {
	$results = $db_handle->getProductDetailsId($_POST["product_id"]);
	foreach($results as $product) {
	$vatamount = ($product["sellingPrice"])*($product['vat']/100);
?>
<div class="form-group has-feedback">
	<input type="number" step="any" required name="productPrice" min="<?php echo $product["originalPrice"];?>" value='<?php echo (($product["sellingPrice"]+$vatamount)-$product["discount"]); ?>' class="form-control">
	<span class="form-control-feedback right" aria-hidden="true">Taka</span>
</div>
<?php
	}
}
?>
