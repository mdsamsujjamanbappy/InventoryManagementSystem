<?php 
require_once 'header_link.php';
?>
<div class="row-fluid" style="color:black;">
    <div class="span12">
        <form action="save_invoice.php" method="POST">
        <table class="table table-striped table-bordered">
            <h3 align="center"><b>Customer's Information</b></h3>
            <br />
                    <tr>
                    <td class="span4" >Customer Name</td>
                    <td class="span8" ><input  placeholder="Customer Name" class="form-control" type="text"  name="name"></td>
                    </tr>
                    
                    <tr>
                    <td class="span2">Customer Phone</td>
                    <td class="span4"><input type="text" required placeholder="Customer Phone"  class="form-control" name="phone"></td>
                    </tr>

                    <tr>
                    <td class="span2">Customer Address</td>
                    <td class="span4"><input type="text"   placeholder="Customer Address" class="form-control" name="address"></td>
                    </tr>

                    <tr>
                    <td class="span2">Total Amount</td>
                    <td  class="span4"><input  onload="pay()" value="<?php echo $_REQUEST['tot'];?>" id="txt2"  onkeyup="pay();"  disabled type="text"  class="form-control" name="total_amount"></td>
                    </tr>

                    <tr>
                    <td class="span2">VAT(%)</td>
                    <td  class="span4"><input id="svat"  onkeyup="pay();" type="number" step="any" value="0"  class="form-control" name="vat"></td>
                    </tr>

                    <tr>
                    <td class="span2">Special Discount</td>
                    <td  class="span4"><input id="sdis"  onkeyup="pay();" type="number" step="any" value="0"  class="form-control" name="specialDiscount"></td>
                    </tr>

                    <tr>
                    <td class="span2">Payable Amount</td>
                    <td  class="span4"><input id="payable"  onkeyup="cal();"  readonly type="text"  class="form-control" name="payableAmount"></td>
                    </tr>

                    <tr>
                    <td class="span2">Paid Amount</td>
                    <td class="span4"><input required placeholder="Paid Amount" type="number"   id="txt1"  onkeyup="cal();"  class="form-control" name="tendered_amount"></td>
                    </tr>

                    <tr>
                    <td class="span2">Due Amount</td>
                    <td class="span4"><input  placeholder="" id="txt3" disabled type="text" class="form-control" name="change_amount"></td>
                    </tr>
                    
    					<tr>
                    <td class="span2"></td>
                    <td  class="span4">
                        <input type="submit" name="invoice_save_print" value="Save & Print" class="btn btn-primary ">
                    </td>
                    </tr>
        </table>
        </form>
    </div>
</div>

<script>
    function cal() {
        var txtFirstNumberValue = document.getElementById('payable').value;
        var txtSecondNumberValue = document.getElementById('txt1').value;
        var result = parseFloat(txtFirstNumberValue)-parseFloat(txtSecondNumberValue);
        if (!isNaN(result)) {
            document.getElementById('txt3').value = result;
        }
    }

    function pay() {
        var txtFirstNumberValue = document.getElementById('txt2').value;
        var txtSecondNumberValue = document.getElementById('sdis').value;
        var svat = document.getElementById('svat').value;
        var vat = txtFirstNumberValue*(svat/100);
        var result = ((parseFloat(txtFirstNumberValue)+vat) - parseFloat(txtSecondNumberValue));
        if (!isNaN(result)) {
            document.getElementById('payable').value = result;
            cal();
        }
    }
    document.getElementById('payable').value = (document.getElementById('txt2').value);

    window.onload = pay;
</script>
