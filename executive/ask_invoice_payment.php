<?php 
require_once 'header_link.php'; 
?>
<div>
    <div class="row animated  " style="color:black;">
        <div class="col-md-10 col-md-offset-1">
            <form action="save_final_invoice.php" method="POST">
              <h3><center><b>Final Payments</b></center></h3><br>
            <table class="table table-striped table-bordered responsive-utilities">

              <tr>
                  <td class="">Payable Amount</td>
                  <td class="">
                  <div>
                    <input hidden value="<?php echo md5("MSB");?>" name="_MSBtoken" >
                    <input readonly class="form-control" value="<?php echo $_GET['pay'];?>"  name="payableAmount" >
                  </div>
                  </td>
              </tr>
              <tr>
                  <td class="">Paid Amount</td>
                  <td class="">
                  <div>
                  <input required class="form-control" autofocus name="tendered_amount" placeholder="Insert paid amount" >
                  </div>
                  </td>
              </tr>

              <tr>
                  <td class=" "></td>
                  <td class=" ">
                      <input onClick="return confirm('Do you want to save final Invoice?');" type="submit" name="update_price" value="Submit" class="btn btn-success ">
                      <input type="reset" name="" value="Reset" class="btn btn-danger ">
                  </td>
              </tr>
            </table>
            </form>
         </div>
    </div>
</div>
