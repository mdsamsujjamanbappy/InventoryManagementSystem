<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $my_tools->title();?></title>
    <?php include("css.php");?>
  </head>


  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Executive Panel</span></a>
            </div>

            <div class="clearfix"></div>
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->executiveMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->exuserImage();?>" alt=""><?php $my_tools->exfullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Modify Sales Info (<span style="font-weight:bold;color:red;">Invoice No-<?php echo $_SESSION['invoiceNumber'];?></span>)</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>                    
                  <div class="x_content">
                    <center>
                         <div class="row">
                        <br>
                            <div class="col-md-12">
                               <?php
                                    $tmp_id = $_SESSION['tmpMSalesNumber'];
                                    $results = $db_handle->getTmpProducts($tmp_id);

                                    
                               ?>
                               <table style="margin-top:-30px"; class="table table-bordered table-stripted table-hover">
                                      <thead style="font-size:15px;color:black;">
                                            <th width="3%">SN</th>
                                            <th width="35%">Product Name <br></th>
                                            <th width="4%">Quantity <br></th>
                                            <th width="12%">Rate <span style="font-size:11px;">(inc. Vat & Discount)</span></th>
                                            <th  width="8%">Sub-Total <br></th>
                                            <th width="4%">Action <br></th>
                                       </thead>
                                        <?php
                                        $i=0;
                                        $gt =0;
                                        $trow=count($results);
                                        if($trow>0){

                                         foreach($results as $product) {
                                        ?>
                                       <tr>
                                            <td><?php echo ++$i; ?></td>
                                            <td><b><?php echo ($product["pname"]); ?></b><br>
                                            <span style="font-size:11px;"><?php echo ($product["sdescription"]); ?></span>

                                            </td>
                                            <td><?php echo ($product["productQtys"])." ".($product["unitName"]); ?> </td>
                                            <td><?php echo (($product["productPriceRate"])); ?> Taka</td>
                                             <td><?php echo (($product["productPriceRate"])*($product["productQtys"])); ?> Taka</td>
                                            <td>
                                                <a rel="facebox" href="update_product.php?id=<?php echo base64_encode($product['tmpid']); ?>" class="btn btn-danger btn-xs" >Update Price</a>
                                              </td>
                                       </tr>

                                       <?php
                                        $gt = $gt + (($product["productPriceRate"])*($product["productQtys"]));

                                        }
                                       }else{
                                          echo "<tr><td colspan='7' > <center>No Products are in Chart</center></td></tr>";
                                        }
                                        ?>
                                        <tfoot>
                                            <th colspan="4" >Total </th>
                                            <th  colspan="" style="font-size:15px;"><?php echo $gt;?> Taka</th>
                                            <th  colspan=""></th>
                                       </tfoot>
                                   </table>
                                   <div class="row">
                                    <div class="col-md-12" align="center">
                                         <a  style="margin-left:10px;" href="ask_invoice_payment.php?pay=<?php echo $gt;?>" class="btn btn-success" rel="facebox"><i class="fa fa-save"></i>  Save Final Invoice</a>
                                    </div>
                                  </div>

                            </div>
                        </div>
                        </center>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>