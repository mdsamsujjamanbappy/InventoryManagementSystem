<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Invoice</title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Executive Panel</span></a>
            </div>

            <div class="clearfix"></div>
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->executiveMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->exuserImage();?>" alt=""><?php $my_tools->exfullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Invoice</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12">
                       <?php
                        $results = $db_handle->getNewInvoiceNumber();
                        foreach($results as $row0) {
                            $invoiceNumber = $row0['maximum'];
                        }
                        
                        $invoiceNumber+=1;
                        
                        date_default_timezone_set('Asia/Dhaka');

                        if(isset($_POST['invoice_save_print'])){

                            $tmpId = $_SESSION['tmpSalesNumber'];
                            $customerName = $_POST['name'];
                            $customerPhone = $_POST['phone'];
                            $customerAddress = $_POST['address'];
                            $specialDiscount = $_POST['specialDiscount'];
                            $vat = $_POST['vat'];
                            $invoiceDate = date("Y-m-d");
                            $invoiceTime = date("h:i:sa");
                            $userId =$_SESSION['exId'];
                            $paymentMethodId = 1;
                            $tenderedAmount = $_POST['tendered_amount'];
                            $payableAmount = $_POST['payableAmount'];

                            $results = $db_handle->getTmpAllProducts($tmpId);
                             foreach($results as $product) {
                                $tmpid = $product["tmpid"];
                                $pid = $product["pid"];
                                $productPriceRate = $product["productPriceRate"];
                                $profitAmount = $product["profitAmount"];
                                $quantity = $product["productQtys"];
                                $pdescription = $product["pdescription"];
                                
                                $db_handle->tmpToSales($invoiceNumber,$pid,$productPriceRate,$quantity,$profitAmount,$pdescription);

                                $db_handle->updateDecreaseProductQuantity($pid,$quantity);

                                $db_handle->deleteProductFromChart($tmpid);
                             }
                             if($results){
                                 $db_handle->insertInvoiceInfo($invoiceNumber,$customerName,$customerPhone,$customerAddress,$userId,$invoiceDate,$invoiceTime,$paymentMethodId,$tenderedAmount,$specialDiscount,$vat);
                                 $db_handle->insertPaymentInfo($invoiceNumber,$customerPhone,$userId,$invoiceDate,$invoiceTime,$tenderedAmount,$payableAmount);
                                }
                                  unset($_SESSION['tmpSalesNumber']);
                                  echo "<script> document.location.href='sale_conf.php?iin=".md5($invoiceNumber)."&&inv=".base64_encode($invoiceNumber)."';</script>";
                                
                                }

                    ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
  </body>
</html>