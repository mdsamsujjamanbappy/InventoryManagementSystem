<?php
ob_start();
if(isset($_GET['id'])){
require_once 'header_link.php';
require_once 'number_to_word.php';
require('../fpdf/fpdf.php');

$invoice_number=base64_decode($_GET['iid']);
$r = $db_handle->getInvoiceDetails($invoice_number);
if(count($r)>0){
class PDF extends FPDF
{
	function Code39($xpos=0, $ypos=0, $code, $baseline=1, $height=9){

	$wide = $baseline;
	$narrow = $baseline / 3 ; 
	$gap = $narrow;

	$barChar['0'] = 'nnnwwnwnn';
	$barChar['1'] = 'wnnwnnnnw';
	$barChar['2'] = 'nnwwnnnnw';
	$barChar['3'] = 'wnwwnnnnn';
	$barChar['4'] = 'nnnwwnnnw';
	$barChar['5'] = 'wnnwwnnnn';
	$barChar['6'] = 'nnwwwnnnn';
	$barChar['7'] = 'nnnwnnwnw';
	$barChar['8'] = 'wnnwnnwnn';
	$barChar['9'] = 'nnwwnnwnn';
	$barChar['A'] = 'wnnnnwnnw';
	$barChar['B'] = 'nnwnnwnnw';
	$barChar['C'] = 'wnwnnwnnn';
	$barChar['D'] = 'nnnnwwnnw';
	$barChar['E'] = 'wnnnwwnnn';
	$barChar['F'] = 'nnwnwwnnn';
	$barChar['G'] = 'nnnnnwwnw';
	$barChar['H'] = 'wnnnnwwnn';
	$barChar['I'] = 'nnwnnwwnn';
	$barChar['J'] = 'nnnnwwwnn';
	$barChar['K'] = 'wnnnnnnww';
	$barChar['L'] = 'nnwnnnnww';
	$barChar['M'] = 'wnwnnnnwn';
	$barChar['N'] = 'nnnnwnnww';
	$barChar['O'] = 'wnnnwnnwn'; 
	$barChar['P'] = 'nnwnwnnwn';
	$barChar['Q'] = 'nnnnnnwww';
	$barChar['R'] = 'wnnnnnwwn';
	$barChar['S'] = 'nnwnnnwwn';
	$barChar['T'] = 'nnnnwnwwn';
	$barChar['U'] = 'wwnnnnnnw';
	$barChar['V'] = 'nwwnnnnnw';
	$barChar['W'] = 'wwwnnnnnn';
	$barChar['X'] = 'nwnnwnnnw';
	$barChar['Y'] = 'wwnnwnnnn';
	$barChar['Z'] = 'nwwnwnnnn';
	$barChar['-'] = 'nwnnnnwnw';
	$barChar['.'] = 'wwnnnnwnn';
	$barChar[' '] = 'nwwnnnwnn';
	$barChar['*'] = 'nwnnwnwnn';
	$barChar['$'] = 'nwnwnwnnn';
	$barChar['/'] = 'nwnwnnnwn';
	$barChar['+'] = 'nwnnnwnwn';
	$barChar['%'] = 'nnnwnwnwn';

	$this->SetFont('Arial','B',11);
	$this->Text($xpos, $ypos + $height + 4, $code);
	$this->SetFillColor(0);

	$code = '*'.strtoupper($code).'*';
	for($i=0; $i<strlen($code); $i++){
		$char = $code[$i];
		if(!isset($barChar[$char])){
			$this->Error('Invalid character in barcode: '.$char);
		}
		$seq = $barChar[$char];
		for($bar=0; $bar<9; $bar++){
			if($seq[$bar] == 'n'){
				$lineWidth = $narrow;
			}else{
				$lineWidth = $wide;
			}
			if($bar % 2 == 0){
				$this->Rect($xpos, $ypos, $lineWidth, $height, 'F');
			}
			$xpos += $lineWidth;
		}
		$xpos += $gap;
		}
	}
	function Header()
	{
		$this ->SetFont('Times','B',18);;
		$this->Cell(200,10,invoiceCompanyTitle(),0,0,'C');
		$this->Ln(6);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyAddress(),0,0,'C');
		$this->Ln(5);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyPhone(),0,0,'C');
		$this->Ln(5);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyEmail(),0,0,'C');
		$this->Ln(9);
	}

	function Footer()
	{
	    $this->SetY(-19);
	    $this->SetFont('Times','',10);
	    $this->Cell(0,10,"...................................................................",0,0,'R');

	    $this->SetY(-15);
	    $this->SetFont('Times','I',10);
	    $this->Cell(0,10,"Signature & Date                    ",0,0,'R');

	    $this->SetY(-17);
	    $this->SetFont('Times','I',11);
	    $this->Cell(0,10,"Thank you for shopping with us.",0,0,'C');

	     $this->SetY(-15);
	    $this->SetFont('Times','I',8);
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'L');
	}
}

foreach($r as $invoiceDetails) {
    $customerName = $invoiceDetails['customerName'];
    $customerPhone = $invoiceDetails['customerPhone'];
    $customerAddress = $invoiceDetails['customerAddress'];
    $invoiceDate = $invoiceDetails['invoiceDate'];
    $invoiceTime = $invoiceDetails['invoiceTime'];
    $tenderedAmount = $invoiceDetails['tenderedAmount'];
	$specialDiscount = $invoiceDetails["specialDiscount"];
	$tvat = $invoiceDetails["tvat"];

}

$pdf = new PDF();
$pdf->AddPage();
$pdf->AliasNbPages();

$pdf->SetFont('Times','B',12);
$pdf->Cell(34,12,"Invoice Number: ",0,0,'R');
// $pdf->Cell(80,8,$pdf->Code39(95,39,str_pad("123",5, '0', STR_PAD_LEFT)),0,0,'C');
$pdf->Cell(80,8,$pdf->Code39(44,36,str_pad($invoice_number,5, '0', STR_PAD_LEFT)),0,0,'C');
$pdf->Ln(14);

$pdf->SetFont('Times','',11);
$pdf->Cell(27,10,"Customer Name: ",0,0);
$pdf->SetFont('Times','B',12);
$pdf->Cell(53,10,$customerName,0,0);
$pdf->Cell(35,10,"",0,0);
$pdf->SetFont('Times','',11);
$pdf->Cell(28,10,"Customer Phone: ",0,0);
$pdf->SetFont('Times','B',11);
$pdf->Cell(55,10,$customerPhone,0,0);
$pdf->Ln(6);

$pdf->SetFont('Times','',11);
$pdf->Cell(31,10,"Customer Address: ",0,0);
$pdf->SetFont('Times','B',9);
$pdf->Cell(50,10,$customerAddress,0,0);
$pdf->Cell(34,10,"",0,0);
$pdf->SetFont('Times','',11);
$pdf->Cell(22,10,"Date & Time: ",0,0);
$pdf->SetFont('Times','B',11);
$pdf->Cell(50,10,date("d-M-Y", strtotime($invoiceDate))."   ".$invoiceTime,0,0);
$pdf->Ln(12);

$pdf->SetFont('Times','B',11);
$pdf->Cell(13,10,"Serial",1);
$pdf->Cell(107,10,"Product Name",1);
$pdf->Cell(24,10,"Quantity",1,0,'C');
$pdf->Cell(21,10,"Rate",1,0,'C');
$pdf->Cell(30,10,"Sub-Total",1,0,'C');
$pdf->Ln();

$results = $db_handle->getSalesProducts($invoice_number);
$i=0;
$gt =0;
$tdiscount=0;
$trow=count($results);
if($trow>0){
	foreach($results as $product){
			$rate =$product["productPriceRate"];
			$pdf->SetFont('Times','',10);
			$pdf->Cell(13,7,++$i,'LT',0,'C');
			$pdf->SetFont('Times','B',10);
			$pdf->Cell(107,7,$product["pname"],'LT',0,'L');
			$pdf->SetFont('Times','',10);
			$pdf->Cell(24,7,$product["productQtys"]." ".$product["unitName"],'LT',0,'C');
			$pdf->Cell(21,7,$rate." TK",'LT',0,'C');
			$pdf->Cell(30,7,$product["productQtys"]*($product["productPriceRate"])." TK",'LTR',0,'C');
			$pdf->Ln(5);

			$pdf->SetFont('Times','',8);
			$pdf->Cell(13,5,"",'LB',0,'L');
			$pdf->Cell(107,5,$product["sdescription"],'LB',0);
			$pdf->Cell(24,5,"",'LB',0);
			$pdf->Cell(21,5,"",'LB',0);
			$pdf->Cell(30,5,"",'LBR',0);
			$pdf->Ln();
			$gt+=$product["productQtys"]*($product["productPriceRate"]);

		if ($i==14) {
			$pdf->Ln(10);
		}
		}
		$totVat = $gt*($tvat/100);
		$pdf->SetFont('Times','B',11);
		$pdf->Cell(135,10,"",'L',0,'C');
		$pdf->SetFont('Times','B',10);
		$pdf->Cell(30,7,"Total",1,0);
		$pdf->Cell(30,7,$gt." TK",1,0,'C');
		$pdf->Ln();

		$pdf->SetFont('Times','B',12);
		$pdf->Cell(135,7,"Net total amount in word:",'L',0,'C');
		$pdf->SetFont('Times','B',10);
		$pdf->Cell(30,7,"VAT (".$tvat."%)",1,0);
		$pdf->Cell(30,7,$totVat." TK",1,0,'C');
		$pdf->Ln();

		$pdf->SetFont('Times','I',8);
		$pdf->Cell(135,7,strtoupper(convert_number_to_words((($gt+$totVat)-$specialDiscount))." Taka Only"),'L',0,'C');
		$pdf->SetFont('Times','B',10);
		$pdf->Cell(30,7,"Spe. Discount",1,0);
		$pdf->Cell(30,7,$specialDiscount." TK",1,0,'C');
		$pdf->Ln();

		$pdf->SetFont('Times','',9);
		$pdf->Cell(135,7,'','LB',0,0);
		$pdf->SetFont('Times','B',11);
		$pdf->Cell(30,7,"Net Total",1,0);
		$pdf->Cell(30,7,(($gt+$totVat)-$specialDiscount)." TK",1,0,'C');
		$pdf->Ln();
	}
$filename = "Invoice-".$invoice_number.".pdf";

$pdf->Output("",$filename,"false");
}else{
	echo "<span style='color:red;font-size:15px;font-weight:bold;'><center>Invalid Invoice</center></span>";
}
}else{
	echo "<span style='color:red;font-size:15px;font-weight:bold;'><center>Invalid Invoice</center></span>";
}
ob_end_flush();
?>
