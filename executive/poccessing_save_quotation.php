<?php 
require_once 'header_link.php';
?>
<div class="row-fluid" style="color:black;">
    <div class="span12">
        <form action="save_quotation.php" method="POST">
        <table class="table table-striped table-bordered">
            <h3 align="center"><b>Customer's Information</b></h3>
            <br />
            <tr>
                <td class="span4" >Customer Name:</td>
                <td class="span8" ><input required placeholder="Customer Name" class="form-control" type="text"  name="name"></td>
            </tr>
            
            <tr>
                <td class="span2">Customer Phone</td>
                <td class="span4"><input type="text"   placeholder="Customer Phone"  class="form-control" name="phone"></td>
            </tr>

            <tr>
                <td class="span2">Customer Address</td>
                <td class="span4"><input type="text"   placeholder="Customer Address" class="form-control" name="address"></td>
            </tr>

            <tr>
                <td class="span2">Total Amount</td>
                <td  class="span4"><input value="<?php echo $_REQUEST['tot'];?>" id="txt2"  onkeyup="pay();"  disabled type="text"  class="form-control" name="total_amount"></td>
            </tr>

            <tr>
            <td class="span2"></td>
            <td  class="span4">
                <input type="submit" name="quotation_save_print" value="Save & Print" class="btn btn-primary ">
            </td>
            </tr>
        </table>
       </form>
    </div>
</div>
