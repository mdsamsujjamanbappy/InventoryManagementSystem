<?php 
require_once 'header_link.php'; 
require_once 'number_to_word.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Report | View Invoice</title>

    <?php include("css.php");?>

    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">

                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="x_panel">
                            <div class="x_title">
                              <span style="color:black !important;font-weight: bold;font-size: 16px;">Search Invoice</span>
                              <ul class="nav navbar-right panel_toolbox">
                                <li>
                                  <a style="color:red;" target="_BLANK" href="save_invoice_as_pdf.php?id=<?php echo base64_encode($_POST['invoice_number']); ?>"><i class="fa fa-print"></i> Save as PDF</a>
                                </li>
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                              </ul>
                              <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                            <br >
                              <center>
                              <?php 
                                  if(isset($_POST['search'])){
                                  $invoice_number = $_POST['invoice_number'];
                                   $results = $db_handle->getSalesProducts($invoice_number);
                                  if(!isset($results))
                                  {
                                    echo "<h4 style='margin-left: 30px;' >Invoice Number: <b>".$invoice_number."</b></h4>";
                                    echo "<h2 style='color:;margin-top:80px;'>Sorry, Invoice does not found</h2>";
                                  }else{ 
                                    $results1 = $db_handle->getInvoiceDetails($invoice_number);
                                    foreach($results1 as $InvoiceDetails) {
                                ?>
                                
                              <div class="row" >
                                  <div class="col-md-12" align="center" style="margin-top: -20px;" >
                                    <?php $my_tools->invoiceHeader(); ?>
                                  </div>
                              </div>

                                  <div class="row"  style="margin-left: 30px;">
                                      <div class="col-md-7" align="left">
                                            <?php 
                                                  echo "<h5 >Invoice Number: <b>".$invoice_number."</b></h5>";
                                                  echo "Customer Name &nbsp;&nbsp;&nbsp;&nbsp;: <b>".$InvoiceDetails["customerName"]."</b><br/>";
                                                  echo "Customer Phone &nbsp;&nbsp;&nbsp;: <b>".$InvoiceDetails["customerPhone"]."</b><br/>";
                                                   }
                                              ?> 
                                      </div>
                                      <div  class="col-md-5" align="" style="margin-top:35px;" >
                                        <?php 
                                            echo "Customer Address : <b>".$InvoiceDetails["customerAddress"]."</b><br/>";
                                            echo "Date & Time: <b>".$InvoiceDetails["invoiceDate"]." ".$InvoiceDetails["invoiceTime"]."</b>";
                                            $ten_amount = $InvoiceDetails["tenderedAmount"];
                                              
                                        ?>
                                      </div>
                                  </div>

                                  <div class="row"  style="margin-left: 30px;">
                                    <div class="col-md-12">
                                      <table  style="margin-top:15px;"  align="center"class="table table-bordered table-stripted table-hover">
                                        <thead style="font-size:15px;color:dark;">
                                              <th width="3%">SN</th>
                                              <th width="45%">Product Name <br></th>
                                              <th width="4%">Quantity <br></th>
                                              <th width="10%">Rate</th>
                                              <th width="6%">Vat</th>
                                              <th width="8%">Discount</th>
                                              <th  width="10%"><center>Sub-Total <br></center></th>
                                         </thead>
                                          <?php
                                          $i=0;
                                          $tdiscount=0;
                                          $gt =0;
                                          $trow=count($results);
                                          if($trow>0){
                                           
                                           foreach($results as $product) {
                                           $vatamount = ($product["productPriceRate"])*($product['svat']/100);
                                          ?>
                                         <tr>
                                              <td><?php echo ++$i; ?></td>
                                              <td><b><?php echo htmlentities($product["pname"]); ?></b><br>
                                              <span style="font-size:11px;"><?php echo htmlentities($product["sdescription"]); ?></span>

                                              </td>
                                              <td><?php echo htmlentities($product["productQtys"]); ?> pc's</td>
                                              <td><?php echo htmlentities($product["productPriceRate"]); ?> TK</td>
                                              <td><?php echo htmlentities($product["svat"]); ?> %</td>
                                              <td><?php echo htmlentities($product["sdiscount"]); ?> TK</td>
                                               <td><center><?php echo htmlentities((($product["productPriceRate"]+$vatamount)-$product["sdiscount"])*($product["productQtys"])); ?> TK</center></td>
                                              
                                         </tr>

                                         <?php 
                                            $vatamount = ($product["productPriceRate"])*($product['svat']/100);
                                            $gt+=($product["productQtys"]*$product["productPriceRate"])+$vatamount;
                                            $tdiscount += $product["sdiscount"];
                                          
                                          } 
                                         }else{
                                            echo "<tr><td colspan='6' > <center>No Products are in Chart</center></td></tr>";
                                          } 
                                          ?> 
                                          <tr>
                                              <th colspan="4"></th>
                                              <th colspan="2" >Total </th>
                                              <th  colspan="" style="font-size:15px;"><center><?php echo $gt;?> TK</center></th>
                                         </tr>
                                         <tr>
                                              <th colspan="4"></th>
                                              <th colspan="2" >Discount</th>
                                              <th  colspan="" style="font-size:15px;"><center><?php echo $tdiscount;?> TK</center></th>
                                         </tr>
                                         <tr>
                                              <th colspan="4" style="font-size:12px;"><i>In Word: <?php echo strtoupper(convert_number_to_words($gt-$tdiscount)." Taka Only");?></i></th>
                                              <th colspan="2" >Net Total </th>
                                              <th  colspan="" style="font-size:15px;"><center><?php echo $gt-$tdiscount;?> TK</center></th>
                                         </tr>
                                      </table>
                                     <?php 
                                        } 
                                        } 
                                      ?>
                                  </div>         
                                </div>



                              </center>
                            </div>
                          </div>
                        </div>
                        </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

  </body>
</html>