<?php
ob_start();
if(isset($_GET['iid'])){
require_once 'header_link.php';
require_once 'number_to_word.php';
require('../fpdf/fpdf.php');
class PDF extends FPDF
{
	function Header()
	{
		$this ->SetFont('Times','B',18);;
		$this->Cell(200,10,invoiceCompanyTitle(),0,0,'C');
		$this->Ln(6);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyAddress(),0,0,'C');
		$this->Ln(5);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyPhone(),0,0,'C');
		$this->Ln(5);

		$this ->SetFont('Times','',11);;
		$this->Cell(200,10,invoiceCompanyEmail(),0,0,'C');
		$this->Ln(9);
	}

	function Footer()
	{
	    $this->SetY(-18);
	    $this->SetFont('Times','',10);
	    $this->Cell(0,10,"...................................................................",0,0,'R');

	    $this->SetY(-15);
	    $this->SetFont('Times','',10);
	    $this->Cell(0,10,"Signature & Date                    ",0,0,'R');

	     $this->SetY(-15);
	    $this->SetFont('Times','I',8);
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'L');
	}
}

$quotationNumber=base64_decode($_GET['iid']);
$r = $db_handle->getQuotationDetails($quotationNumber);
foreach($r as $QuotationDetails) {
    $customerName = $QuotationDetails['customerName'];
    $customerPhone = $QuotationDetails['customerPhone'];
    $customerAddress = $QuotationDetails['customerAddress'];
    $quotationDate = $QuotationDetails['quotationDate'];
    $quotationTime = $QuotationDetails['quotationTime'];
}

$pdf = new PDF();
$pdf->AddPage();
$pdf->AliasNbPages();

$pdf->Ln(5);
$pdf->SetFont('Times','B',12);
$pdf->Cell(55,8,"",0,0,'C');
$pdf->Cell(80,8,"Quotation Number: ".$quotationNumber,1,0,'C');
$pdf->Cell(55,8,"",0,0,'C');
$pdf->Ln(12);

$pdf->SetFont('Times','',11);
$pdf->Cell(27,10,"Customer Name: ",0,0);
$pdf->SetFont('Times','B',12);
$pdf->Cell(53,10,$customerName,0,0);
$pdf->Cell(35,10,"",0,0);
$pdf->SetFont('Times','',11);
$pdf->Cell(28,10,"Customer Phone: ",0,0);
$pdf->SetFont('Times','B',11);
$pdf->Cell(55,10,$customerPhone,0,0);
$pdf->Ln(6);

$pdf->SetFont('Times','',11);
$pdf->Cell(31,10,"Customer Address: ",0,0);
$pdf->SetFont('Times','B',9);
$pdf->Cell(50,10,$customerAddress,0,0);
$pdf->Cell(34,10,"",0,0);
$pdf->SetFont('Times','',11);
$pdf->Cell(22,10,"Date & Time: ",0,0);
$pdf->SetFont('Times','B',11);
$pdf->Cell(50,10,date("d-M-Y", strtotime($quotationDate))."   ".$quotationTime,0,0);
$pdf->Ln(12);

$pdf->SetFont('Times','B',12);
$pdf->Cell(13,10,"Serial",1);
$pdf->Cell(110,10,"Product Name",1);
$pdf->Cell(21,10,"Quantity",1,0,'C');
$pdf->Cell(20,10,"Rate",1,0,'C');
$pdf->Cell(30,10,"Sub-Total",1,0,'C');
$pdf->Ln();

$results = $db_handle->getQuotationProducts($quotationNumber);
$i=0;
$gt =0;
$tdiscount=0;
$trow=count($results);
if($trow>0){
	foreach($results as $product){
			$rate =$product["quotationRate"];
			$pdf->SetFont('Times','',10);
			$pdf->Cell(13,7,++$i,'LT',0,'C');
			$pdf->SetFont('Times','B',10);
			$pdf->Cell(110,7,$product["pname"],'LT',0,'L');
			$pdf->SetFont('Times','',10);
			$pdf->Cell(21,7,$product["quotationQtys"]." ".$product["unitName"],'LT',0,'C');
			$pdf->Cell(20,7,$rate." TK",'LT',0,'C');
			$pdf->Cell(30,7,$product["quotationQtys"]*($product["quotationRate"])." TK",'LTR',0,'C');
			$pdf->Ln(5);

			$pdf->SetFont('Times','',8);
			$pdf->Cell(13,5,"",'LB',0,'L');
			$pdf->Cell(110,5,$product["sdescription"],'LB',0);
			$pdf->Cell(21,5,"",'LB',0);
			$pdf->Cell(20,5,"",'LB',0);
			$pdf->Cell(30,5,"",'LBR',0);
			$pdf->Ln();
			$gt+=$product["quotationQtys"]*($product["quotationRate"]);

		if ($i==14) {
			$pdf->Ln(10);
		}
		}

		$pdf->SetFont('Times','I',10);
		$pdf->Cell(144,9,strtoupper(convert_number_to_words((($gt)))." Taka Only"),'LB',0,'C');
		$pdf->SetFont('Times','B',10);
		$pdf->Cell(20,9,"Total ",1,0);
		$pdf->Cell(30,9,$gt." TK",1,0,'C');
		$pdf->Ln();

	}
$filename = "Quotation-".$quotationNumber.".pdf";

$pdf->Output("",$filename,"false");
}else{
	echo "<span style='color:red;font-size:15px;font-weight:bold;'><center>Invalid Invoice</center></span>";
}
ob_end_flush();
?>
