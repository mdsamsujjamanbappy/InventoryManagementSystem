<?php 
require_once 'header_link.php'; 
$id = base64_decode($_REQUEST['id']);
$results = $db_handle->getSaleProductDetails($id);
foreach($results as $product) {
  $productPriceRate = $product['productPriceRate'];
  $pname = $product['pname'];
  $pcode = $product['code'];
  }
?>
<div>
    <div class="row animated  " style="color:black;">
        <div class="col-md-10 col-md-offset-1">
            <form action="update_price.php" method="POST">
              <h3><center><b>Update Price</b></center></h3><br>
            <table class="table table-striped table-bordered responsive-utilities">
            
              <tr>
                  <td  class="">Product Code</td>
                  <td class="">
                  <div ><b><?php echo $pcode;?></b></div>
                  </td>
              </tr>  
              <tr>
                  <td  class="">Product Name</td>
                  <td class="">
                  <div ><b><?php echo $pname;?></b></div>
                  </td>
              </tr> 

              <tr>
                  <td class="">Price</td>
                  <td class="">
                  <div>
                    <input hidden value="<?php echo $id;?>" name="_MSBtoken" >
                    <input hidden value="<?php echo $id;?>" name="id" >
                    <input required class="form-control" autofocus value="<?php echo $productPriceRate;?>"  name="productPriceRate" >
                  </div>
                  </td>
              </tr>

              <tr>
                  <td class=" "></td>
                  <td class=" ">
                      <input type="submit" name="update_price" value="Update Price" class="btn btn-success ">
                      <input type="reset" name="" value="Reset" class="btn btn-danger ">
                  </td>
              </tr>
            </table>
            </form>
         </div>
    </div>
</div>
