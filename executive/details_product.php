<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $my_tools->title();?></title>
    <?php include("css.php");?>
  </head>


  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Executive Panel</span></a>
            </div>

            <div class="clearfix"></div>
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->executiveMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->exuserImage();?>" alt=""><?php $my_tools->exfullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Product Details</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="product_list.php" style="color:white;" class="btn btn-primary btn-xs"><i class="fa fa-th"></i> All Products</a>
                      </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>                    
                  <div class="x_content">
                    <?php
                      $id = base64_decode($_REQUEST['id']);
                      $results = $db_handle->getProductDetails($id);
                      foreach($results as $product) {
                      ?>
                      <div class="row animated" style="color:black;">
                        <div class="col-md-12"  style="margin-bottom:-4px;">
                        <table class="table table-bordered">
                          <tr><td width="25%">Product Code:</td><td><b><?php echo htmlentities($product["code"]); ?></b></td></tr>
                          <tr><td>Product Name:</td><td><b><?php echo htmlentities($product["pname"]); ?></b></td></tr>
                          <tr><td>Sub-Category:</td><td><?php echo htmlentities($product["sub_cat_name"]); ?></td></tr>
                          <tr><td>Category:</td><td><?php echo htmlentities($product["cname"]); ?></td></tr>
                          <tr><td>Description:</td><td><?php echo ($product["description"]); ?></td></tr>
                          <tr><td>Short <br >Description:</td><td><?php echo ($product["sdescription"]); ?></td></tr>
                          <tr><td>Quantity:</td><td><b><?php echo htmlentities($product["quantity"])." ".htmlentities($product["unitName"]); ?></b></td></tr>
                          <tr><td>Original Price:</td><td><?php echo htmlentities($product["originalPrice"]); ?></td></tr>
                          <tr><td>Selling Price:</td><td><?php echo htmlentities($product["sellingPrice"]); ?></td></tr>
                          <tr><td>Vat:</td><td><?php echo htmlentities($product["vat"])." %"; ?></td></tr>
                          <tr><td>Discount:</td><td><?php echo htmlentities($product["discount"]); ?></td></tr>
                          <tr><td>Image:</td><td><img width="150px" height="150px" src="../product_image/<?php echo htmlentities($product["image"]); ?>" ></td></tr>
                        </table>
                       </div>
                      </div>
                      <?php } ?>




                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>