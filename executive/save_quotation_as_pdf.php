<?php
ob_start();
if(isset($_GET['iid'])){
require_once 'header_link.php';
require_once 'number_to_word.php';
require('../fpdf/fpdf.php');
$quotationNumber=base64_decode($_GET['iid']);
$r = $db_handle->getQuotationDetails($quotationNumber);
if(count($r)>0){
foreach($r as $QuotationDetails) {
    $customerName = $QuotationDetails['customerName'];
    $customerPhone = $QuotationDetails['customerPhone'];
    $customerAddress = $QuotationDetails['customerAddress'];
    $quotationDate = $QuotationDetails['quotationDate'];
    $quotationTime = $QuotationDetails['quotationTime'];
}

function getInvoiceNumber(){
	return base64_decode($_GET['iid']);
}

class PDF extends FPDF
{	
	function Code39($xpos=0, $ypos=0, $code, $baseline=1, $height=9){

	$wide = $baseline;
	$narrow = $baseline / 3 ; 
	$gap = $narrow;

	$barChar['0'] = 'nnnwwnwnn';
	$barChar['1'] = 'wnnwnnnnw';
	$barChar['2'] = 'nnwwnnnnw';
	$barChar['3'] = 'wnwwnnnnn';
	$barChar['4'] = 'nnnwwnnnw';
	$barChar['5'] = 'wnnwwnnnn';
	$barChar['6'] = 'nnwwwnnnn';
	$barChar['7'] = 'nnnwnnwnw';
	$barChar['8'] = 'wnnwnnwnn';
	$barChar['9'] = 'nnwwnnwnn';
	$barChar['A'] = 'wnnnnwnnw';
	$barChar['B'] = 'nnwnnwnnw';
	$barChar['C'] = 'wnwnnwnnn';
	$barChar['D'] = 'nnnnwwnnw';
	$barChar['E'] = 'wnnnwwnnn';
	$barChar['F'] = 'nnwnwwnnn';
	$barChar['G'] = 'nnnnnwwnw';
	$barChar['H'] = 'wnnnnwwnn';
	$barChar['I'] = 'nnwnnwwnn';
	$barChar['J'] = 'nnnnwwwnn';
	$barChar['K'] = 'wnnnnnnww';
	$barChar['L'] = 'nnwnnnnww';
	$barChar['M'] = 'wnwnnnnwn';
	$barChar['N'] = 'nnnnwnnww';
	$barChar['O'] = 'wnnnwnnwn'; 
	$barChar['P'] = 'nnwnwnnwn';
	$barChar['Q'] = 'nnnnnnwww';
	$barChar['R'] = 'wnnnnnwwn';
	$barChar['S'] = 'nnwnnnwwn';
	$barChar['T'] = 'nnnnwnwwn';
	$barChar['U'] = 'wwnnnnnnw';
	$barChar['V'] = 'nwwnnnnnw';
	$barChar['W'] = 'wwwnnnnnn';
	$barChar['X'] = 'nwnnwnnnw';
	$barChar['Y'] = 'wwnnwnnnn';
	$barChar['Z'] = 'nwwnwnnnn';
	$barChar['-'] = 'nwnnnnwnw';
	$barChar['.'] = 'wwnnnnwnn';
	$barChar[' '] = 'nwwnnnwnn';
	$barChar['*'] = 'nwnnwnwnn';
	$barChar['$'] = 'nwnwnwnnn';
	$barChar['/'] = 'nwnwnnnwn';
	$barChar['+'] = 'nwnnnwnwn';
	$barChar['%'] = 'nnnwnwnwn';

	$this->SetFont('Arial','B',11);
	$this->Text($xpos, $ypos + $height + 4, $code);
	$this->SetFillColor(0);

	$code = '*'.strtoupper($code).'*';
	for($i=0; $i<strlen($code); $i++){
		$char = $code[$i];
		if(!isset($barChar[$char])){
			$this->Error('Invalid character in barcode: '.$char);
		}
		$seq = $barChar[$char];
		for($bar=0; $bar<9; $bar++){
			if($seq[$bar] == 'n'){
				$lineWidth = $narrow;
			}else{
				$lineWidth = $wide;
			}
			if($bar % 2 == 0){
				$this->Rect($xpos, $ypos, $lineWidth, $height, 'F');
			}
			$xpos += $lineWidth;
		}
		$xpos += $gap;
		}
	}
	function Header()
	{
		$this->Cell(30,10,"",0,0,'L');
		$this ->SetFont('Arial','B',20);
		$this->SetTextColor(30, 70, 73);
		// $this->SetTextColor(254, 50, 0);
		$this->Cell(113,10,invoiceCompanyTitle(),0,0,'L');
		$this ->SetFont('Arial','B',18);
		$this->SetTextColor(240, 105, 0);
		$this->Cell(50,10,"QUOTATION",0,0,'L');
		$this->SetTextColor(0, 0, 0);
		$this->Ln(8);

		$this ->SetFont('Arial','',10);
		$this->Cell(30,10,'',0,0,'L');
		$this->Code39(155,27,str_pad(getInvoiceNumber(),5, '0', STR_PAD_LEFT));
		$this->Cell(113,10,invoiceCompanyAddress(),0,0,'L');
		$this ->SetFont('Arial','B',10);
		$this->Cell(40,10,"Print Date: ".date('d-m-Y'),0,0,'C');
		$this->Ln(5);

		$this ->SetFont('Arial','',11);
		$this->Cell(30,10,"",0,0,'L');
		$this->Cell(100,10,invoiceCompanyPhone(),0,0,'L');
		$this->Ln(5);

		$this->SetFont('Arial','',11);
		$this->Cell(30,10,"",0,0,'L');
		$this->Cell(100,10,invoiceCompanyEmail(),0,0,'L');
		$this->Ln(5);

		$this->SetFont('Arial','',11);
		$this->Cell(30,10,"",0,0,'L');
		$this->Cell(100,10,invoiceCompanySloganHeader(),0,0,'L');
		$this->Ln(6);

		// Logo Image
		$this->SetAlpha(1);
		$image=mainLogo();
		$this->SetFont("Arial","B",10);
		$this->Image($image,7,6,34,34);

		// Header line
		$this->SetAlpha(1);
		$this->SetLineWidth(1);
		$this->SetDrawColor(30, 86, 73);
		$this->SetFont('Times','',11);
		$this->Line(8,44,202,44);
		$this->SetLineWidth(0.4);
		$this->Line(146,14,146,44);
		$this->Ln(10);

		// Background Watermark Image
		$this->SetAlpha(0.09);
		$image=watermarkImages();
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,35,75,130,130);
		$this->SetAlpha(1);
		
	}

	function Footer()
	{




	    $this->SetY(-18);
	    $this->SetFont('Times','',10);
	    $this->Cell(0,10,"...................................................................",0,0,'R');
	    $this->SetY(-14);
	    $this->SetFont('Times','I',10);
	    $this->Cell(0,10,signature(),0,0,'R');

	    $this->SetY(-16);
	    $this->SetFont('Times','I',12);
	    $this->Cell(0,10,footerText2(),0,0,'C');

	     $this->SetY(-14);
	    $this->SetFont('Times','I',8);
	    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'L');
	}


	var $extgstates = array();

    function SetAlpha($alpha, $bm='Normal')
    {
        // set alpha for stroking (CA) and non-stroking (ca) operations
        $gs = $this->AddExtGState(array('ca'=>$alpha, 'CA'=>$alpha, 'BM'=>'/'.$bm));
        $this->SetExtGState($gs);
    }

    function AddExtGState($parms)
    {
        $n = count($this->extgstates)+1;
        $this->extgstates[$n]['parms'] = $parms;
        return $n;
    }

    function SetExtGState($gs)
    {
        $this->_out(sprintf('/GS%d gs', $gs));
    }

    function _enddoc()
    {
        if(!empty($this->extgstates) && $this->PDFVersion<'1.4')
            $this->PDFVersion='1.4';
        parent::_enddoc();
    }

    function _putextgstates()
    {
        for ($i = 1; $i <= count($this->extgstates); $i++)
        {
            $this->_newobj();
            $this->extgstates[$i]['n'] = $this->n;
            $this->_out('<</Type /ExtGState');
            $parms = $this->extgstates[$i]['parms'];
            $this->_out(sprintf('/ca %.3F', $parms['ca']));
            $this->_out(sprintf('/CA %.3F', $parms['CA']));
            $this->_out('/BM '.$parms['BM']);
            $this->_out('>>');
            $this->_out('endobj');
        }
    }

    function _putresourcedict()
    {
        parent::_putresourcedict();
        $this->_out('/ExtGState <<');
        foreach($this->extgstates as $k=>$extgstate)
            $this->_out('/GS'.$k.' '.$extgstate['n'].' 0 R');
        $this->_out('>>');
    }

    function _putresources()
    {
        $this->_putextgstates();
        parent::_putresources();
    }
}

$pdf = new PDF();
// $pdf->PDF1($invoice_number);
$pdf->AddPage();
$pdf->AliasNbPages();

$pdf->SetFillColor(235,235,235);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Arial','',10);
$pdf->Cell(33,6,"","LTR",0,'L',true);
$pdf->Cell(18,6,"Name: ","TR",0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(140,6,$customerName,"TR",0,'L');
$pdf->Ln(6);

$pdf->SetFont('Arial','B',13);
$pdf->Cell(33,6,"Quotation","LR",0,'C',true);
$pdf->SetFont('Arial','',10);
$pdf->Cell(18,6,"Phone: ","TR",0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(140,6,$customerPhone,"TR",0,'L');
$pdf->Ln(6);

$pdf->SetFont('Arial','B',13);
$pdf->Cell(33,6,"To","LR",0,'C',true);
$pdf->SetFont('Arial','',10);
$pdf->Cell(18,6,"Address: ","TR",0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(140,6,$customerAddress,"TR",0,'L');
$pdf->Ln(6);

$pdf->SetFont('Arial','',10);
$pdf->Cell(33,6,"","LBR",0,'L',true);
$pdf->Cell(18,6,"Date : ","TBR",0,'L');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(140,6,date("d-M-Y", strtotime($quotationDate))." ".$quotationTime,"TBR",0,'L');
$pdf->Ln(12);

$pdf->SetFillColor(30,86,73);
$pdf->SetTextColor(254,254,254);

// $pdf->SetFillColor(235,235,235);
// $pdf->SetTextColor(0,0,0);
$pdf->SetFont('Arial','B',11);
$pdf->Cell(13,10,"Serial",1,0,'L',true);
$pdf->Cell(104,10,"Product Name",1,0,'L',true);
$pdf->Cell(24,10,"Quantity",1,0,'C',true);
$pdf->Cell(21,10,"Rate",1,0,'C',true);
$pdf->Cell(30,10,"Sub-Total",1,0,'C',true);
$pdf->Ln();
$pdf->SetTextColor(0,0,0);

$pdf->SetDrawColor(30,86,73);

$results = $db_handle->getQuotationProducts($quotationNumber);
$i=0;
$gt =0;
$tdiscount=0;
$trow=count($results);
if($trow>0){
	foreach($results as $product){
			$rate =$product["quotationRate"];
			$pdf->SetFont('Times','',10);
			$pdf->Cell(13,7,++$i,'LT',0,'C');
			$pdf->SetFont('Times','B',10);
			$pdf->Cell(104,7,$product["pname"],'LT',0,'L');
			$pdf->SetFont('Times','',10);
			$pdf->Cell(24,7,$product["quotationQtys"]." ".$product["unitName"],'LT',0,'C');
			$pdf->Cell(21,7,$rate." TK",'LT',0,'C');
			$pdf->Cell(30,7,$product["quotationQtys"]*($product["quotationRate"])." TK",'LTR',0,'C');
			$pdf->Ln(5);

			$pdf->SetFont('Times','',8);
			$pdf->Cell(13,5,"",'LB',0,'L');
			$pdf->Cell(104,5,$product["sdescription"],'LB',0);
			$pdf->Cell(24,5,"",'LB',0);
			$pdf->Cell(21,5,"",'LB',0);
			$pdf->Cell(30,5,"",'LBR',0);
			$pdf->Ln();
			$gt+=$product["quotationQtys"]*($product["quotationRate"]);

		if ($i==14) {
			$pdf->Ln(10);
		}
		}

		$pdf->SetFont('Arial','I',8);
		$pdf->Cell(141,9,strtoupper(convert_number_to_words((($gt)))." Taka Only"),'LB',0,'C');
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(21,9,"Total ",1,0);
		$pdf->Cell(30,9,$gt." TK",1,0,'C');
		$pdf->Ln();
		$pdf->SetTextColor(0,0,0);

	}
$filename = "Quotation-".$quotationNumber.".pdf";

$pdf->Output("",$filename,"false");
}else{
	echo "<span style='color:red;font-size:28px;font-weight:bold;'><br><br><br><br><center>Invalid Invoice</center></span>";
}
}else{
	echo "<span style='color:red;font-size:28px;font-weight:bold;'><br><br><br><br><center>Invalid Invoice</center></span>";
}
ob_end_flush();
?>