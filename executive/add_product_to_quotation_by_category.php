<?php 
require_once 'header_link.php'; 
?>
<div>
    <div class="row animated  " style="color:black;">
        <div class="col-md-12">
            <form action="save_to_quotation_tbtmp.php" method="POST" onsubmit="return cal()" >
            <table class="table table-striped table-bordered responsive-utilities">
            <br /><br />
                <script>                              
                    function getSubCat(val) {
                      $.ajax({
                      type: "POST",
                      url: "get_sub_category.php",
                      data:'category_id='+val,
                      success: function(data){
                      $("#subcat_list").html(data);
                      }
                      });
                    } 
                             
                    function getProductList(val) {
                        $.ajax({
                        type: "POST",
                        url: "get_product_list.php",
                        data:'sub_cat_id='+val,
                        success: function(data){
                            $("#product-list").html(data);
                        }
                        });
                    }
                    
                    function getProductPrice(val) {
                        $.ajax({
                        type: "POST",
                        url: "get_product_price.php",
                        data:'product_id='+val,
                        success: function(data){
                            $("#product-price").html(data);
                        }
                        });
                    }

                    function getProductQuantity(val) {
                        $.ajax({
                        type: "POST",
                        url: "get_product_quantity.php",
                        data:'product_id='+val,
                        success: function(data){
                            $("#product-quantity").html(data);
                        }
                        });
                    }
                </script>
                          <tr>
                              <td width="35%">Select Category:</td>
                              <td>
                                  <select class="form-control" name="cat_id" required autofocus onChange="getSubCat(this.value);" >
                                      <option value=""> Select Category</option>
                                        <?php
                                          $results = $db_handle->getCategory();
                                       foreach($results as $category) {
                                        ?>
                                           <option value="<?php echo $category['cid']; ?>"><?php echo $category['cname']; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td class="">Select Sub-Category:</td>
                                <td >
                                 <select id="subcat_list"  class="form-control" onChange="getProductList(this.value);"  required name="subcat_id" required >
                                  </select>
                              </td>
                          </tr>
                        <tr>
                            <td  class="">Select Product</td>
                            <td class=" ">
                                <select id="product-list"  class="form-control" name="product_id"  onclick="getProductQuantity(this.value);" onChange="getProductPrice(this.value);"  required  class="select2me form-control">
                            </select>
                            </td>
                        </tr>
                        
                        <tr>
                             <td  class="">Selling Price
                             <br ><span style="font-size:11px;">(including Discount and VAT)</span></td>
                            <td class="">
                            <div id="product-price"><input required class="form-control"  disabled ></div>
                            </td>
                        </tr>

                        <tr>
                            <td  class="">Available Quantity</td>
                            <td>
                                <select id="product-quantity"  id="avilable_quantity"  class="form-control" name="avail_quan"  onkeyup="cal();" readonly  class="select2me form-control"></select>
                            </td>
                        </tr>

                     
                        <tr>
                            <td class="span2">Quantity</td>
                            <td>
                                <input  class="form-control" required placeholder="Number of Quantity"  onkeyup="cal();" id="txt2" type="text" name="quantity">
                            </td>
                        </tr>
						   
                        <tr>
                            <td class=" "></td>
                            <td class=" ">
                                <input type="submit" name="add_product" value="Add Product" class="btn btn-success ">
                                <input type="reset" name="" value="Reset" class="btn btn-danger ">
                            </td>
                        </tr>
            </table>
                 </form>
                        <span  class=""  type="text" disabled  > </span>
         </div>
    </div>
</div>
