<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $my_tools->title();?></title>
    <?php include("css.php");?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Executive Panel</span></a>
            </div>

            <div class="clearfix"></div>
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->executiveMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->exuserImage();?>" alt=""><?php $my_tools->exfullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Receive Due Payment</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>  
                  <?php if(!isset($_POST['search'])){ ?>

                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12">
                        <form action="" method="POST">
                         
                         <div class="form-group">
                          <div class="col-md-6 col-sm-12 col-xs-12">
                            <b>Customer Phone Number: </b><br><br>
                            <input autofocus class="form-control" name="customerPhone" placeholder="Insert Customer Phone Number Here" required >
                            <br >
                            <button name="search" class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Search Invoice</button>
                            </div>

                          </div>
                        </form> 
                      </div>
                    </div>
                  </div>
                  <?php
                  }else{ ?>
                  <div class="x_content">
                         <?php
                          $customerPhone=($_POST['customerPhone']);
                          $r = $db_handle->getCheckCustomerPDetails($customerPhone);
                          if(count($r)){
                             foreach($r as $dataArr) {
                                $customerName = $dataArr['customerName'];
                                $customerAddress = $dataArr['customerAddress'];
                             }

                              $r1 = $db_handle->getCustomerPDetails($customerPhone);
                              $payableAmount = 0;
                              $paidAmount = 0;
                              $dueAmount = 0;
                             foreach($r1 as $dataArr1) {
                              $payableAmount+=$dataArr1['dueAmount'];
                              $paidAmount+=$dataArr1['paidAmount'];

                             }
                             $dueAmount = $payableAmount-$paidAmount;

                         ?> 
                        <form action="save_receive_payment.php" method="POST">
                         <div class="form-group">
                          <div class="col-md-6 col-sm-12 col-xs-12">
                            <h5>Customer Name: <b><?php echo $customerName;?></b></h5>
                            <h5>Customer Phone: <b><?php echo $customerPhone;?></b></h5>
                            <h5>Customer Address: <b><?php echo $customerAddress;?></b></h5>
                              <hr>
                            <h4 style="color:red;">Due Amount: <b><?php echo $dueAmount;?> Taka</b></h4>
                            
                            <br >
                            <b>Payment Amount: </b><br>
                            <input name="customerPhone" hidden value="<?php echo $customerPhone;?>">
                            <input type="number" autofocus step="any" class="form-control" name="paymentAmount" placeholder="Insert payment amount here" required >
                            <br >
                            
                            <b>Payment Description: </b><br>
                            <textarea class="form-control" name="description" placeholder="Insert description here" ></textarea>
                            <br >
                            <input name="save_payment" class="btn btn-primary" type="submit" value="Save Payment" >
                            </div>
                          </div>
                        </form> 


                         
                        <?php }else{ ?>
                          <h2 style="color:red;"><strong>Sorry, Customer not found.</strong></h2>
                         <?php } ?>
                      </div>
                      <?php } ?>


                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>