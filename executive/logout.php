<?php
require_once 'header_link.php';

date_default_timezone_set('Asia/Dhaka');
$date = date('Y-m-d');
$time = date("h:i:sa");
$results = $db_handle->insertLogoutActivityLog($_SESSION['exId'],"Log out", $date, $time);

unset($_SESSION['exAccess']);
unset($_SESSION['exId']);
unset($_SESSION['fexname']);
unset($_SESSION['exImage']);
echo "<script> document.location.href='../index.php';</script>";
?>