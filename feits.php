<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Far-East IT Solution Ltd.</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="assets/css/bootstrap-progressbar.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">

        <?php if(isset($_POST['authLog'])){ 
           ?>

        <div class="animate form login_form">
          <section class="login_content">
             <?php 
                require_once("assets/tools/auth.php"); 
                $db_handle = new myAuthFunctions();                          

                if(isset($_POST['uname'])&&isset($_POST['upass'])){
                      $uname = ($_POST['uname']);
                      $upass = ($_POST['upass']);
                      $trcId = ($_POST['trcId']);

                     $r = $db_handle->checkMSBAuthDetails($uname,$upass,$trcId);

                     if(count($r)>0){ 
                        $results = $db_handle->MSBAuthDetailsAtLogin();
                        foreach($results as $dataArr) {
                            $sdate=base64_decode(htmlentities($dataArr["sdate"])); 
                            $edays=base64_decode(htmlentities($dataArr["edays"])); 
                            $appId=(htmlentities($dataArr["appId"])); 
                        } ?>
                        <table class="table table-bordered table-striped" style="font-size:14px;font-weight:bold;">
                          <tr>
                            <td>Previous Activated Date: </td>
                            <td><?php echo date("d-M-Y", strtotime($sdate)); ?></td>
                          </tr>
                          <tr>
                            <td>Activation Till: </td>
                            <td><?php echo date("d-M-Y", strtotime($edays)); ?></td>
                          </tr>
                        </table>
                        <div class="separator"></div> 
                       
                        <form action="" method="POST">
                          <div>
                            <input type="number" autofocus min="0" class="form-control" placeholder="Number of Days to Increase" name="days" required="" />
                            <input name="appId" hidden value="<?php echo $appId;?>" >
                          </div>
                          <div >
                            <input type="submit" style="margin-top:20px;" onclick="return confirm('Are you sure to update activation information?')" class="btn btn-primary" name="actNew" value="Re-Activate" />
                          </div>
                          <div class="clearfix"></div>
                          </div>
                        </form>

                     <?php
                     }else{ ?>
                       <div  style="color:red;">
                          <strong>Invalid Login!</strong>
                        </div>
                   <?php }
                    }
                ?>
          </section>
        </div>
        <?php }else if(isset($_POST['actNew'])){

             if(isset($_POST['days'])){
                require_once("assets/tools/auth.php"); 
                $db_handle = new myAuthFunctions();
                date_default_timezone_set('Asia/Dhaka');
                $today = date("Y-m-d");

                $days=$_POST['days'];
                $appId=$_POST['appId'];
                $edays= date('Y-m-d', strtotime($today. ' + '.$days.' days'));
                $act=($days);
                $db_handle->newActivationAction($today,$act,$edays,$days,$appId);

                echo "<center><h5 style='color:green;font-weight:bold;'>New Activation information has been applied.</h5>";
                echo "<hr><a href='index.php' class='btn btn-primary btn-sm'><i class='fa fa-sign-in'></i> Login page</a></center>";
             }

          }else{ ?>

        <div class="animate form login_form">
          <section class="login_content">
            <form action="" method="POST">
              <h1><b>FEITS</b></h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" name="uname" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password"  name="upass" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Access Code" name="trcId" required="" />
              </div>
              <div>
                <center><input type="submit" class="btn btn-danger" name="authLog" value="Access" /></center>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                
                <br />

                <div>
                    Developed by <a target="_BLANK" title="Far-East IT Solution Ltd." href="https://feits.co"> <strong>Far-East IT Solution Ltd.</strong></a>
                </div>
              </div>
            </form>
          </section>
        </div>
        <?php } ?>

      </div>
    </div>
  </body>
</html>
